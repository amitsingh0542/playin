package playin.orgname.com.playin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.model.MyTeamsModel;


public class MyTeamsAdapter extends BaseListAdapter {

	protected static final String TAG = "MyTeamsAdapter";
	private LayoutInflater mInflater;
    private List<MyTeamsModel> listMyTeams;
    private String[] sss;
    public MyTeamsAdapter(Context context, ArrayList<MyTeamsModel> myTeams)
	{
		 mInflater = LayoutInflater.from(context);
	     this.listMyTeams = myTeams;
	}
	@Override
	public int getCount() 
	{
		if (listMyTeams != null)
			return listMyTeams.size();
		else
			return 0;
	}

	@Override
	public Object getItem(int position) 
	{
		return listMyTeams.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		Holder viewHolder = new Holder();
		if(convertView == null)
		{
    		convertView = mInflater.inflate(R.layout.list_item_myteams, null);

			viewHolder.tvEventDate = (TextView) convertView.findViewById(R.id.tv_tournament_date_myteams);
			viewHolder.tvEventMonth=(TextView)convertView.findViewById(R.id.tv_tournament_month_myteams);
			viewHolder.tvEventYear=(TextView)convertView.findViewById(R.id.tv_tournament_year_myteams);

    		viewHolder.tvTournamentName = (TextView) convertView.findViewById(R.id.tv_tournament_name_myteams);
			viewHolder.tvTournamentTime=(TextView)convertView.findViewById(R.id.tv_tournament_time_myteams);
			viewHolder.imgSportsName=(TextView)convertView.findViewById(R.id.tv_sports_name_myteams);
			viewHolder.tvTournamentLocation = (TextView) convertView.findViewById(R.id.tv_location_myteams);
    		convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder = (Holder) convertView.getTag();
    	}

		viewHolder.tvEventDate.setText("15");
		viewHolder.tvEventMonth.setText("Jun");
		viewHolder.tvEventYear.setText("2016");

		viewHolder.tvTournamentName.setText(listMyTeams.get(position).getName());
		viewHolder.tvTournamentTime.setText("9:30 PM");

		viewHolder.imgSportsName.setText("Carrom");
		viewHolder.tvTournamentLocation.setText(listMyTeams.get(position).getSport());
		return convertView;
	}
	
	private class Holder 
	{
		TextView tvEventDate;
		TextView tvEventMonth;
		TextView tvEventYear;

		TextView tvTournamentName;
		TextView tvTournamentTime;
		TextView imgSportsName;
		TextView tvTournamentLocation;
    }
}
