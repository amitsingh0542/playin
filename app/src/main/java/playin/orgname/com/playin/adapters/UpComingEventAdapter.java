package playin.orgname.com.playin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.model.MyEventModel;
import playin.orgname.com.playin.model.UpComingEventModel;


public class UpComingEventAdapter extends BaseListAdapter {

	protected static final String TAG = "MyEventAdapter";
	private LayoutInflater mInflater;
    private List<UpComingEventModel> listUpComingEvent;
    private String[] sss;
    public UpComingEventAdapter(Context context, ArrayList<UpComingEventModel> myEvent)
	{
		 mInflater = LayoutInflater.from(context);
	     this.listUpComingEvent = myEvent;
	}
	@Override
	public int getCount() 
	{
		if (listUpComingEvent != null)
			return listUpComingEvent.size();
		else
			return 0;
	}

	@Override
	public Object getItem(int position) 
	{
		return listUpComingEvent.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		Holder viewHolder = new Holder();
		if(convertView == null)
		{
    		convertView = mInflater.inflate(R.layout.list_item_myevent, null);

			viewHolder.tvEventDate = (TextView) convertView.findViewById(R.id.tv_tournament_date_myevent);
			viewHolder.tvEventMonth=(TextView)convertView.findViewById(R.id.tv_tournament_month_myevent);
			viewHolder.tvEventYear=(TextView)convertView.findViewById(R.id.tv_tournament_year_myevent);

    		viewHolder.tvTournamentName = (TextView) convertView.findViewById(R.id.tv_tournament_name_myevent);
			viewHolder.tvTournamentTime=(TextView)convertView.findViewById(R.id.tv_tournament_time_myevent);
			viewHolder.imgSportsName=(TextView)convertView.findViewById(R.id.tv_sports_name_myevent);
			viewHolder.tvTournamentLocation = (TextView) convertView.findViewById(R.id.tv_location_myevent);
    		convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder = (Holder) convertView.getTag();
    	}

		viewHolder.tvEventDate.setText("15");
		viewHolder.tvEventMonth.setText("Jun");
		viewHolder.tvEventYear.setText("2016");

		//viewHolder.tvTournamentName.setText(listUpComingEvent.get(position).getTitle());
		viewHolder.tvTournamentTime.setText("9:30 PM");

		viewHolder.imgSportsName.setText("Carrom");
		//viewHolder.tvTournamentLocation.setText(listUpComingEvent.get(position).getCity());
		return convertView;
	}
	
	private class Holder 
	{
		TextView tvEventDate;
		TextView tvEventMonth;
		TextView tvEventYear;

		TextView tvTournamentName;
		TextView tvTournamentTime;
		TextView imgSportsName;
		TextView tvTournamentLocation;
    }
}
