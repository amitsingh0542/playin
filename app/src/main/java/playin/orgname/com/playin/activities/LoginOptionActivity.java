package playin.orgname.com.playin.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.RelativeLayout;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.base.BaseActivity;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.preference.PlayInSharedPrefKeys;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;

public class LoginOptionActivity extends BaseActivity implements OnClickListener
{
	public static final String TAG = "LoginOptionActivity";
	private Context mContext;
	private RelativeLayout relativeLayoutIndividualOption,relativeLayoutOrganizationOption;
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		//getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_login_option);
		mContext=getApplicationContext();
		initViews();
		initListener();
	}

	private void initViews() 
	{
		relativeLayoutIndividualOption=(RelativeLayout)findViewById(R.id.rl_individual_option);
		relativeLayoutOrganizationOption=(RelativeLayout)findViewById(R.id.rl_organization_option);
	}
	
	private void initListener() 
	{
		relativeLayoutIndividualOption.setOnClickListener(this);
		relativeLayoutOrganizationOption.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
			case R.id.rl_individual_option:
				Intent intentIndividual = new Intent(LoginOptionActivity.this, LoginActivity.class);
				PlayInSharedPrefUtils.getInstance(mContext).saveStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE, Constants.USER_TYPE_INDIVIDUAL);
				//intentIndividual.putExtra("login_option","Individual");
				startActivity(intentIndividual);
				//finish();
				break;
			case R.id.rl_organization_option:
				Intent intentOrganization = new Intent(LoginOptionActivity.this, LoginActivity.class);
				PlayInSharedPrefUtils.getInstance(mContext).saveStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE, Constants.USER_TYPE_ORGANIZATION);
				//intentOrganization.putExtra("login_option","Organization");
				startActivity(intentOrganization);
				//finish();
				break;
			default:
				break;
		}
	}
	
	private void getLogin()
	{}
	
}
