package playin.orgname.com.playin.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Hashtable;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.adapters.MyEventAdapter;
import playin.orgname.com.playin.adapters.UpComingEventAdapter;
import playin.orgname.com.playin.base.BaseActivity;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.MyEventModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.model.UpComingEventModel;
import playin.orgname.com.playin.preference.PlayInSharedPrefKeys;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;

public class UpComingEventActivity extends BaseActivity implements OnClickListener,DisplableInterface
{
	public static final String TAG = "UpComingEventActivity";

	private Context mContext;
	private ListView lvUpComingEvent;
	private TextView tvHeaderTitle;
	private ImageView imgPrevious;
	private LinearLayout llUpcomingEvent;

	private ArrayList<UpComingEventModel> listUpComingEvent=new ArrayList<UpComingEventModel> ();


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		//getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_upcoming_event);
		mContext=getApplicationContext();
		initViews();
		initData();
		initListener();
	}

	private void initViews()
	{
		imgPrevious=(ImageView)findViewById(R.id.img_previous);
		tvHeaderTitle=(TextView)findViewById(R.id.tv_header_topbar);
		imgPrevious.setVisibility(View.VISIBLE);
		lvUpComingEvent=(ListView)findViewById(R.id.lv_upcoming_event);
		tvHeaderTitle.setText("Upcoming Events");
		llUpcomingEvent=(LinearLayout)findViewById(R.id.ll_upcoming_event);
	}

	private void initListener()
	{
		tvHeaderTitle.setOnClickListener(this);
		imgPrevious.setOnClickListener(this);

		lvUpComingEvent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				/*Intent intent = new Intent(mContext, UpComingEventActivity.class);
				//PlayInSharedPrefUtils.getInstance(mContext).saveStringPrefernce(PlayInSharedPrefKeys.KEY_EVENT_ID, listMyEvent.get(position).getEventId());
				btnDelete.setVisibility(View.VISIBLE);
				startActivity(intent);*/
			}
		});
	}
	private void initData()
	{
		getUpcomingEvent();
	}


	private void getUpcomingEvent()
	{
		MainController controller = new MainController(UpComingEventActivity.this, this, Constants.UPCOMING_EVENT, true);
		String finalUrl= Constants.BASE_URL + Constants.UPCOMING_EVENT_API;
		controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN_TOKEN);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.img_previous:
				finish();
				break;

		}
	}

	@Override
	public void setScreenData(Hashtable obj, byte type, String Responce) {
		switch (type)
		{
			case Constants.UPCOMING_EVENT:
				Hashtable<String, Object> hashtableUpcomingEvent = (Hashtable<String, Object>) obj;
				listUpComingEvent = (ArrayList<UpComingEventModel>) obj.get("data");
				if (listUpComingEvent != null && listUpComingEvent.size() > 0)
				{
					llUpcomingEvent.setVisibility(View.GONE);
					UpComingEventAdapter upComingEventAdapter=new UpComingEventAdapter(mContext,listUpComingEvent);
					lvUpComingEvent.setAdapter(upComingEventAdapter);
				}
				else
				{
					llUpcomingEvent.setVisibility(View.VISIBLE);
				}

				ResponseInfo responseInfoEventForMe = (ResponseInfo) obj.get("responseInfo");
				if (responseInfoEventForMe != null && responseInfoEventForMe.getMessage() != null) {
					if(!TextUtils.isEmpty(responseInfoEventForMe.getMessage()))
					Utility.ShowToast(mContext, responseInfoEventForMe.getMessage());
				}

				break;


		}
	}

	@Override
	public void setScreenData(String obj) {

	}

	@Override
	public void setScreenMessage(String obj, byte type) {

	}

	@Override
	public void setCancelMessage(String obj, byte type) {

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==RESULT_OK)
		{
			switch (requestCode)
			{

			}
		}
	}
}
