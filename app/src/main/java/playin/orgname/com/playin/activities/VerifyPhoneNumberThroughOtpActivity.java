package playin.orgname.com.playin.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Hashtable;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.base.BaseActivity;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.DataModel;
import playin.orgname.com.playin.model.LoginModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.preference.PlayInSharedPrefKeys;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;

public class VerifyPhoneNumberThroughOtpActivity extends BaseActivity implements OnClickListener,DisplableInterface
{
	public static final String TAG = "VerifyPhoneNumberThroughOtpActivity";
	private Context mContext;
	private Button btnConfirmPhoneNumber,btnResendOtp,btnEditPhoneNumber;
	private EditText edtReceivedOtp;


	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		//getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_verify_phone_number_through_otp);
		mContext=getApplicationContext();
		initViews();
		initListener();
	}

	private void initViews() 
	{
		edtReceivedOtp=(EditText)findViewById(R.id.edt_received_otp);
		btnConfirmPhoneNumber=(Button)findViewById(R.id.btn_confirm_phone);
		btnResendOtp=(Button)findViewById(R.id.btn_resend_otp);
		btnEditPhoneNumber=(Button)findViewById(R.id.btn_edit_phone_number);
	}
	
	private void initListener() 
	{
		btnConfirmPhoneNumber.setOnClickListener(this);
		btnResendOtp.setOnClickListener(this);
		btnEditPhoneNumber.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
			case R.id.btn_confirm_phone:
				verifyOTP();
				break;
			case R.id.btn_resend_otp:
				Utility.ShowToast(mContext, Constants.UNDER_DEVELOPMENT);
				break;
			case R.id.btn_edit_phone_number:
				registerPhoneNumberThroughOtp();
				//Utility.ShowToast(mContext, Constants.UNDER_DEVELOPMENT);
				break;
			default:
				break;
		}
	}
	public void verifyOTP()
	{
		int validOTP=1234;
		if(!TextUtils.isEmpty(edtReceivedOtp.getText().toString())) {
			if (Integer.parseInt(edtReceivedOtp.getText().toString())==validOTP)
			{
				//requestVerifyOtp(Integer.parseInt(edtReceivedOtp.getText().toString()));

				if (PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE).equalsIgnoreCase(Constants.USER_TYPE_INDIVIDUAL)) {
					Intent intentProfile = new Intent(mContext, ProfileActivity.class);
					startActivity(intentProfile);
				} else if (PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE).equalsIgnoreCase(Constants.USER_TYPE_ORGANIZATION)) {
					Intent intentVerificationPending = new Intent(mContext, VerificationPendingSignUpAsOrganizationActivity.class);
					startActivity(intentVerificationPending);
				}
				finish();
			}
			else
			{
				Utility.ShowToast(mContext, Constants.PLEASE_ENTER_VALID_OTP);
			}
		}
		else
		{
			Utility.ShowToast(mContext, Constants.PLEASE_ENTER_OTP);
		}
	}

	private void requestVerifyOtp(int otp)
	{
		MainController controller = new MainController(VerifyPhoneNumberThroughOtpActivity.this, this, Constants.VERIFY_OTP, true);
		controller.RequestServiceGet(Constants.BASE_URL + Constants.VERIFY_OTP_API+otp, Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN);
	}

	private void registerPhoneNumberThroughOtp()
	{
		try
		{
			final Dialog dialogRegisterPhoneNumber = new Dialog(VerifyPhoneNumberThroughOtpActivity.this);
			dialogRegisterPhoneNumber.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialogRegisterPhoneNumber.setContentView(R.layout.dialog_register_phone_number);
			//dialogForgetPassword.setTitle("Forgot Password");
			Button btnSendTemporaryPassword=(Button)dialogRegisterPhoneNumber.findViewById(R.id.btn_getotp_mobile_number_registration);

			final EditText edtMobileNumberRegistrsation=(EditText)dialogRegisterPhoneNumber.findViewById(R.id.edt_mobile_number_registrsation);

			edtMobileNumberRegistrsation.addTextChangedListener(new TextWatcher() {

				public void afterTextChanged(Editable s) {

				}

				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if (!TextUtils.isEmpty(s)) {

						if (s.toString().equalsIgnoreCase("0")) {
							Utility.ShowToast(mContext, Constants.RULE_VALID_MOBILE_NUMBER);
							edtMobileNumberRegistrsation.setText("");
							edtMobileNumberRegistrsation.setFocusable(true);
						}
					} else {

					}
				}
			});

			btnSendTemporaryPassword.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v)
				{
					if(!TextUtils.isEmpty(edtMobileNumberRegistrsation.getText()))
					{
						if(edtMobileNumberRegistrsation.getText().length()==10)
						{
							Intent intentIndividual = new Intent(mContext, VerifyPhoneNumberThroughOtpActivity.class);
							startActivity(intentIndividual);
							dialogRegisterPhoneNumber.dismiss();
							finish();
						}
						else
						{
							Utility.ShowToast(mContext, Constants.RULE_VALID_MOBILE_NUMBER);
						}
					}
					else {
						Utility.ShowToast(mContext, Constants.PLEASE_ENTER_MOBILE_NUMBER);
					}
				}
			});
			dialogRegisterPhoneNumber.show();
			//Toast.makeText(mContext, "Login with Google is Successfull!!!!", Toast.LENGTH_SHORT).show();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	@Override
	public void setScreenData(Hashtable obj, byte type, String Responce) {
		switch (type) {
			case Constants.VERIFY_OTP:
				ResponseInfo responseInfoForgotPassword = (ResponseInfo) obj.get("responseInfo");
				if (responseInfoForgotPassword != null && responseInfoForgotPassword.getMessage() != null)
				{
					Utility.ShowToast(mContext, responseInfoForgotPassword.getMessage());

					if (PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE).equalsIgnoreCase(Constants.USER_TYPE_INDIVIDUAL)) {
						Intent intentProfile = new Intent(mContext, ProfileActivity.class);
						startActivity(intentProfile);
					} else if (PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE).equalsIgnoreCase(Constants.USER_TYPE_ORGANIZATION)) {
						Intent intentVerificationPending = new Intent(mContext, VerificationPendingSignUpAsOrganizationActivity.class);
						startActivity(intentVerificationPending);
					}
					finish();
				}
				break;

		}
	}

	@Override
	public void setScreenData(String obj) {

	}

	@Override
	public void setScreenMessage(String obj, byte type) {

	}

	@Override
	public void setCancelMessage(String obj, byte type) {

	}
}
