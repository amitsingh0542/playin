package playin.orgname.com.playin.constants;

/**
 * Created by Shashvat on 6/23/2016.
 */
public class Constants {
    public static final byte REQUEST_TYPE_GET = 0;
    public static final byte REQUEST_TYPE_POST = 1;
    public static final byte REQUEST_TYPE_PUT = 2;

    public static final String HEADER_TYPE_NORMAL = "normal";
    public static final String HEADER_TYPE_PLAY_IN = "playin";
    public static final String HEADER_TYPE_TOKEN = "token";
    public static final String HEADER_TYPE_PLAY_IN_TOKEN = "playin_token";

    public static final String BASE_URL = "http://ec2-52-36-88-102.us-west-2.compute.amazonaws.com:8086/v1/";

    public static final byte REGISTER = 1;
    public static final byte LOGIN = 2;
    public static final byte FORGOT_PASSWORD = 3;
    public static final byte UPDATE_USER_DETAILS = 4;
    public static final byte GET_USER_BY_ID=5;

    public static final byte EVENT_FOR_ME=6;
    public static final byte VERIFY_OTP=7;

    //My Event
    public static final byte CREATE_EVENT=8;
    public static final byte MY_EVENT=9;
    public static final byte EVENT_DETAILS=10;
    public static final byte CONFIRM_EVENT=11;
    public static final byte UPDATE_EVENT=12;
    public static final byte DELETE_EVENT=13;
    public static final byte UPCOMING_EVENT=14;
    public static final byte JOINED_EVENT=15;

    //Sports
    public static final byte GET_ALL_SPORTS=16;

    //My Teams
    public static final byte CREATE_TEAM=17;
    public static final byte MY_TEAM=18;
    public static final byte MY_TEAMS_DETAILS=19;



    //User Type
    public static String USER_TYPE_INDIVIDUAL="Individual";
    public static String USER_TYPE_ORGANIZATION="Organization";
    public static String USER_TYPE_FACEBOOK="facebook";
    public static String USER_TYPE_GOOGLE_PLUS="google";


    //User Module APIs
    public static String REGISTRATION_METHOD = "user/signUp";
    public static String LOGIN_METHOD = "user/login";
    public static String FORGOT_PASSWORD_API = "authenticate/userforgot";
    public static String GET_USER_DETAILS_BY_ID = "user/getUserById/";
    public static String UPDATE_USER_DETAILS_API ="user/updateUserDetails";
    public static String VERIFY_OTP_API ="user/verifyOTP?otp=";


    //Organization
    public static String REGISTRATION_ORGANIZATION_METHOD = "organization/signUp";


    //Discover APIs
    public static String EVENT_FOR_ME_API="discover/eventsForMe";

    // My Event Module and APIs

    public static String CREATE_EVENT_API="event/createEvent";
    public static String MY_EVENT_API="event/myEvents";
    public static String EVENT_DETAILS_API="event/getEventDetails/";
    public static String CONFIRM_EVENT_API="event/confirmevent";
    public static String UPDATE_EVENT_API="event/updateEvent/";
    public static String DELETE_EVENT_API="event/deleteevent/";
    public static String UPCOMING_EVENT_API="event/upcomingEvents";
    public static String JOINED_EVENT_API="event/joinedEvents";


    //My Teams
    public static String CREATE_TEAMS_API="team/createTeam";
    public static String MY_TEAMS_API="team/myTeams";
    public static String MY_TEAMS_DETAILS_API="team/getTeamDetails/";



    //Sports
    public static String GET_ALL_SPORTS_API = "sport/getallsports";

    //Messages
    public static String PLEASE_ENTER_EMAIL = "Please enter email.";
    public static String PLEASE_ENTER_CONFIRM_EMAIL = "Please enter confirm email.";
    public static String EMAIL_MISMATCH = "Confirm email address doesn't match";
    public static String INVALID_EMAIL = "Invalid email address.";

    public static String PLEASE_ENTER_PASSWORD = "Please enter password.";
    public static String PASSWORD_MISMATCH = "Confirm password doesn't match";
    public static String PLEASE_ENTER_MOBILE_NUMBER = "Please enter mobile number";
    public static String RULE_VALID_MOBILE_NUMBER = "Mobile Number Should be 10 digits and should not start with 0.";

    public static String PLEASE_ENTER_OTP = "Please enter OTP.";
    public static String PLEASE_ENTER_VALID_OTP = "Please enter valid OTP.";
    public static String UNDER_DEVELOPMENT = "In Progress...";

}
