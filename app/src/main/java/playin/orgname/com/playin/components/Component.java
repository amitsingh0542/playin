package playin.orgname.com.playin.components;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.ParseException;
import android.view.Surface;
import android.view.WindowManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;

public class Component {
	private Activity activity = null;
	private Context context = null;
	private Dialog popupDialog = null; 
	private PrefrenceManager session = null;
	public Component(){

	}
	public Component(Activity _activity, Context _context){
		activity = _activity;
		context = _context;
		session = new PrefrenceManager(context);
	}

	public String replaceSessionId(String startTag, String endTag, String sessionId, String data){

		int startIndex = data.indexOf(startTag);
		int endIndex = data.indexOf(endTag);

		int tagLength01 = startTag.length();
		int tagLength02 = endTag.length();

		String value01 = data.substring(0, startTag.length() + startIndex);
		String value02 = data.substring(endIndex, data.length());

		String str3 = value01 + sessionId + value02;

		return str3;
	}
	public String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

		Date parsed = null;
		String outputDate = "";

		SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
		SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

		try {
			parsed = df_input.parse(inputDate);
			outputDate = df_output.format(parsed);

		}catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) { 
			e.printStackTrace();
		}

		return outputDate;

	}

	public int getTimeDiffrence(long oldTime, long currentTime){
		//c_date=18/05/2012 04:55:00.  and  saved_date=17/05/2012 03:53:00
		long diffInMillisec = currentTime - oldTime;
		long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMillisec);
		int seconds = (int) (diffInSec % 60);
		diffInSec/= 60;
		int minutes =(int) (diffInSec % 60);
		diffInSec /= 60;
		int hours = (int) (diffInSec % 24);
		diffInSec /= 24;
		int days = (int) diffInSec;
		return minutes;
	}
	
	public String getCurrentTimeFormated() {
		String formatedDateTime = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();

		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm a");
		SimpleDateFormat progressTime = new SimpleDateFormat("HH");
		String	currentTime = sdf.format(c.getTime());
		
		SimpleDateFormat displayFormat;
		try {
			displayFormat = new SimpleDateFormat("HH:mm");
			SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
			Date dt = parseFormat.parse(currentTime);
			currentTime = displayFormat.format(date);
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		formatedDateTime = dateFormat.format(date)+"T"+currentTime+":00.00+05:30";
		return formatedDateTime;

	}
	
	public void unlockScreenOrientation(Activity activity){
		activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
	}
	public  void lockScreenOrientation(Activity activity){   
		WindowManager windowManager =  (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);   
		Configuration configuration = activity.getResources().getConfiguration();   
		int rotation = windowManager.getDefaultDisplay().getRotation(); 

		// Search for the natural position of the device    
		if(configuration.orientation == Configuration.ORIENTATION_LANDSCAPE &&  
				(rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) ||  
				configuration.orientation == Configuration.ORIENTATION_PORTRAIT &&   
				(rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270))   
		{   
			// Natural position is Landscape    
			switch (rotation)   
			{   
			case Surface.ROTATION_0:    
				activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);    
				break;      
			case Surface.ROTATION_90:   
				activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT); 
				break;      
			case Surface.ROTATION_180: 
				activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE); 
				break;          
			case Surface.ROTATION_270: 
				activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
				break;
			}
		}else{
			// Natural position is Portrait
			switch (rotation) 
			{
			case Surface.ROTATION_0: 
				activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 
				break;   
			case Surface.ROTATION_90: 
				activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); 
				break;   
			case Surface.ROTATION_180: 
				activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT); 
				break;          
			case Surface.ROTATION_270: 
				activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE); 
				break;
			}
		}
	}
	
	
	public static ArrayList<String> uniqueShorting(String[] dataArray) {
		ArrayList<String> list = new ArrayList<String>();
		int k = 0;
		int j = 0;
		int i;
		for (i = 0; i < dataArray.length - 1; i++) {

			for (j = 0; j < k; j++) {
				if (dataArray[i + 1].equalsIgnoreCase(dataArray[j])) {
					break;
				}
			}
			if (j == k) {
				if (i == 0) {
					if (!dataArray[i + 1].equalsIgnoreCase(dataArray[j])) {
						list.add(dataArray[i]);
					}
				}
				list.add(dataArray[i + 1]);
				dataArray[k++] = dataArray[i + 1];
			}
		}
		return list;
	}
	public String isErrorMessage(String key, Hashtable tabel){

		String temp = null;	
		if(tabel != null){
			Enumeration e = tabel.keys();
			while (e.hasMoreElements()) {
				e.nextElement();
				Object val = tabel.get(key);
				if(val instanceof String){
					temp = (String) val;
				}
			}
		}

		return temp;
	}
	public ArrayList getDataFromHashTable(String key, Hashtable tabel){

		ArrayList list = new ArrayList();

		if(tabel != null){
			Enumeration e = tabel.keys();
			while (e.hasMoreElements()) {
				e.nextElement();
				Object val = tabel.get(key);
				if(val instanceof ArrayList){
					list =  (ArrayList) val;
				}
			}
		}
		return list;
	}
	
	
	/*public void showAlertMoveToMenu() {
		// TODO Auto-generated method stub
		final Dialog dialog;

		dialog = new Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		dialog.setContentView(R.layout.sessionexpired);

		TextView txtMsg  = (TextView)dialog.findViewById(R.id.txt_msg);
		TextView btn_yesChanges= (TextView)dialog.findViewById(R.id.btn_yesChanges);

		dialog.setCancelable(false); 
		
		txtMsg.setTypeface(Utility.UbuntuRegular);
		btn_yesChanges.setTypeface(Utility.UbuntuRegular);
		// Take Delivery
		btn_yesChanges.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				clearApplicationData();
				Intent ii = new Intent(activity, MenuActivity.class);
				ii.putExtra("ISALREADYLOGIN", true);
				ii.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
				activity.startActivity(ii);
				dialog.dismiss();
			}

		});
	
		try{
		dialog.show();
		}
		catch(Exception e)
		{
			
		}
	}*/
	
	
	
/*	public void showAlertMoveToSplash() {
		// TODO Auto-generated method stub
		
		
		clearApplicationData();
		Intent ii = new Intent(activity, MenuActivity.class);
		ii.putExtra("Splash", true);
		ii.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		activity.startActivity(ii);*/
		
		/*final Dialog dialog;

		dialog = new Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		dialog.setContentView(R.layout.sessionexpired);

		TextView txtMsg  = (TextView)dialog.findViewById(R.id.txt_msg);
		TextView btn_yesChanges= (TextView)dialog.findViewById(R.id.btn_yesChanges);

		txtMsg.setTypeface(Utility.UbuntuRegular);
		btn_yesChanges.setTypeface(Utility.UbuntuRegular);
		// Take Delivery
		btn_yesChanges.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				clearApplicationData();
				Intent ii = new Intent(activity, MenuActivity.class);
				ii.putExtra("Splash", true);
				ii.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				activity.startActivity(ii);
				dialog.dismiss();
			}

		});
	

		dialog.show();
	}*/
	
	private void clearApplicationData(){
		SharedPreferences sharedpreferences = activity.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
		Editor editor = sharedpreferences.edit();
		for(int i=0;i<6;i++){
			editor.putString("addressArray_" + i, null);
			editor.commit();
		}  
		session.setORDER_TIME(null);
		session.clearApplication();
	}
	
	
	
	/*public void connectToSession(DisplayInterface obj){
		MainController controller = new MainController(activity, obj, Constants.NAMESPACE_SECURITY_SERVICE, "StartSession", Constants.SESSION_SERVICE_AUTO,  false);
		ArrayList<Object> list = new ArrayList<Object>();
		ArrayList<Object> dataModelList = new ArrayList<Object>();


		Hashtable<String, Object> table = new Hashtable<String, Object>();
		dataModelList.add(GenerateRequestTag.addTag("v11:ApplicationId", Constants.APPID));
		dataModelList.add(GenerateRequestTag.addTag("v11:CultureCode", controller.CULTURE_CODE));

		table.put("null", dataModelList);
		list.add(table);

		controller.RequestService(list);
	}*/
	

}
