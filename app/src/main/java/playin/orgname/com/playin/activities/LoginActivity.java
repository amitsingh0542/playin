package playin.orgname.com.playin.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.UUID;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.base.BaseActivity;
import playin.orgname.com.playin.bootstrap.PlayInLogManager;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.DataModel;
import playin.orgname.com.playin.model.FacebookUserModel;
import playin.orgname.com.playin.model.LoginModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.preference.PlayInSharedPrefKeys;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;

public class LoginActivity extends BaseActivity implements OnClickListener, DisplableInterface {
    public static final String TAG = "LoginActivity";
    private Button btnLogin;
    private ImageView btnLoginWithFacebook, btnLoginWithGoogle,imgPrevious;
    //private LoginButton btnLoginWithFacebook;
    private TextView tvForgetPassword;

    private EditText mUserIDForSignIn, mPassForSignIn;
    private String username;
    private String password;
    private String passwordNormal;
    private String passwordForRecharge;

    private View mLlCheckBoxRem;
    private String userType = "";
    private Context mContext;
    private CallbackManager callbackManager;
    FacebookUserModel facebookUserModel = null;
    private TextView tvSignUp,tvHeader;
    //private ProgressDialog progress;

    private String facebook_id, f_name, m_name, l_name, gender, profile_image, full_name, email_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mContext = getApplicationContext();
        FacebookSdk.sdkInitialize(mContext);
        callbackManager = CallbackManager.Factory.create();
        getFbKeyHash("playin.orgname.com.playin");
        setContentView(R.layout.activity_login);

        initViews();
        initListener();
    }

    private void initViews() {

        mUserIDForSignIn = (EditText) findViewById(R.id.edt_username_login);
        mPassForSignIn = (EditText) findViewById(R.id.edt_password_login);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnLoginWithFacebook = (ImageView) findViewById(R.id.btn_facebook_login);
        btnLoginWithGoogle = (ImageView) findViewById(R.id.btn_google_login);
        tvForgetPassword = (TextView) findViewById(R.id.tv_forget_password_login);
        tvSignUp = (TextView) findViewById(R.id.tv_signup);
        tvHeader= (TextView) findViewById(R.id.tv_header_topbar);
        imgPrevious=(ImageView)findViewById(R.id.img_previous);

        if (PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE).equalsIgnoreCase(Constants.USER_TYPE_INDIVIDUAL)) {
            tvHeader.setText(Constants.USER_TYPE_INDIVIDUAL);
            mUserIDForSignIn.setText("ashu@gmail.com");
            mPassForSignIn.setText("welcome");

        } else if (PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE).equalsIgnoreCase(Constants.USER_TYPE_ORGANIZATION)) {
            tvHeader.setText(Constants.USER_TYPE_ORGANIZATION +"  "+"Login");
        }

    }

    private void initListener() {
        btnLogin.setOnClickListener(this);
        btnLoginWithFacebook.setOnClickListener(this);
        btnLoginWithGoogle.setOnClickListener(this);
        tvForgetPassword.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);
        imgPrevious.setOnClickListener(this);
        initFacebookListener();
    }

    private void initFacebookListener() {

		/*progress=new ProgressDialog(LoginActivity.this);
        //progress.setMessage(getResources().getString(R.string.please_wait_facebooklogin));
		progress.setMessage("Please wait......");
		progress.setIndeterminate(false);
		progress.setCancelable(false);*/
        //register callback object for facebook result
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //progress.show();
                Profile profile = Profile.getCurrentProfile();
                if (profile != null) {
                    facebook_id = profile.getId();
                    f_name = profile.getFirstName();
                    m_name = profile.getMiddleName();
                    l_name = profile.getLastName();
                    full_name = profile.getName();
                    profile_image = profile.getProfilePictureUri(400, 400).toString();

                    facebookUserModel = new FacebookUserModel();
                    facebookUserModel.setFirstName(profile.getFirstName());
                    facebookUserModel.setMiddleName(profile.getMiddleName());
                    facebookUserModel.setLastName(profile.getLastName());
                    facebookUserModel.setFullName(profile.getName());
                    facebookUserModel.setProfileImage(profile.getProfilePictureUri(400, 400).toString());

                    //Utility.ShowToast(mContext, facebook_id + "," + f_name + "," + l_name + "," + full_name + "," + profile_image);

                    PlayInLogManager.v(TAG, "" + facebook_id + "," + f_name + "," + l_name + "," + full_name + "," + profile_image);
                }
                //Toast.makeText(FacebookLogin.this,"Wait...",Toast.LENGTH_SHORT).show();
                GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    email_id = object.getString("email");
                                    gender = object.getString("gender");
                                    //Start new activity or use this info in your project.
                                    //progress.dismiss();
                                    finish();
                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    //  e.printStackTrace();
                                }

                            }

                        });

                request.executeAsync();
                Utility.ShowToast(mContext, "Login Successful!");
                Intent intentProfile = new Intent(LoginActivity.this, ProfileActivity.class);
                intentProfile.putExtra("TAG", "facebook");
                intentProfile.putExtra("keySignUpData", facebookUserModel);
                startActivity(intentProfile);
            }

            @Override
            public void onCancel() {
                //Toast.makeText(mContext,getResources().getString(R.string.login_canceled_facebooklogin),Toast.LENGTH_SHORT).show();
                Utility.ShowToast(mContext, "Cancel by user");
                //progress.dismiss();
            }

            @Override
            public void onError(FacebookException error) {
                //Toast.makeText(mContext,getResources().getString(R.string.login_failed_facebooklogin),Toast.LENGTH_SHORT).show();
                Utility.ShowToast(mContext, "Error Occured");
                //progress.dismiss();
            }
        });
    }

    //for facebook callback result.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE).equalsIgnoreCase(Constants.USER_TYPE_INDIVIDUAL)) {
            tvHeader.setText(Constants.USER_TYPE_INDIVIDUAL+"  "+"Login");
        } else if (PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE).equalsIgnoreCase(Constants.USER_TYPE_ORGANIZATION)) {
            tvHeader.setText(Constants.USER_TYPE_ORGANIZATION +"  "+"Login");
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                getLogin();
                break;
            case R.id.tv_forget_password_login:
                showForgetPasswordDialog();
                break;

            case R.id.btn_facebook_login:
                getLoginWithFacebook();
                break;
            case R.id.btn_google_login:
                getLoginWithGoogle();
                break;

            case R.id.tv_signup:
                signUp();
                break;

            case R.id.img_previous:
                finish();
                break;

            default:
                break;
        }
    }

    private void getLogin() {
        if (!TextUtils.isEmpty(mUserIDForSignIn.getText().toString())) {
            if (Utility.isValidEmail(mUserIDForSignIn.getText().toString().trim())) {
                if (!TextUtils.isEmpty(mPassForSignIn.getText().toString().trim())) {
                    loginRequest(mUserIDForSignIn.getText().toString().trim(), mPassForSignIn.getText().toString().trim());
                } else
                    Utility.ShowToast(mContext, Constants.PLEASE_ENTER_PASSWORD);
            } else
                Utility.ShowToast(mContext, Constants.INVALID_EMAIL);
        } else
            Utility.ShowToast(mContext, Constants.PLEASE_ENTER_EMAIL);

    }

    private void signUp()
    {
        Intent intentIndividual = new Intent(mContext, SignUpActivity.class);
        startActivity(intentIndividual);
    }

    private void getLoginWithFacebook() {
        //Toast.makeText(mContext, "Login with Facebook is Successfull!!!!", Toast.LENGTH_SHORT).show();

        PlayInSharedPrefUtils.getInstance(mContext).saveStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE, Constants.USER_TYPE_FACEBOOK);
        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "user_friends", "email"));
    }

    private void getLoginWithGoogle() {
        Utility.ShowToast(mContext, Constants.UNDER_DEVELOPMENT);

       /* PlayInSharedPrefUtils.getInstance(mContext).saveStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE, Constants.USER_TYPE_GOOGLE_PLUS);
        Utility.ShowToast(mContext, Constants.UNDER_DEVELOPMENT);*/

       /* Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
		startActivity(intent);*/

    }

    private void showForgetPasswordDialog() {
        try {
            //tvHeader.setText("Forgot Password");
            final Dialog dialogForgetPassword = new Dialog(LoginActivity.this);
            dialogForgetPassword.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogForgetPassword.setContentView(R.layout.dialog_forget_password);
            Button btnSendTemporaryPassword = (Button) dialogForgetPassword.findViewById(R.id.btn_send_temp_password_forgot_password);

            final EditText edtEmail = (EditText) dialogForgetPassword.findViewById(R.id.edt_email_forgot_password);
            btnSendTemporaryPassword.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(edtEmail.getText())) {
                        if (Utility.isValidEmail(edtEmail.getText().toString()))
                        {
                            requestForgotPassword(edtEmail.getText().toString().trim());
                            dialogForgetPassword.dismiss();

                        } else {
                            Utility.ShowToast(mContext, Constants.INVALID_EMAIL);
                        }
                    } else {
                        Utility.ShowToast(mContext, Constants.PLEASE_ENTER_EMAIL);
                    }
                }
            });

            dialogForgetPassword.show();
            //Toast.makeText(mContext, "Login with Google is Successfull!!!!", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setScreenData(Hashtable obj, byte type, String Responce) {
        switch (type) {
            case Constants.LOGIN:
                Hashtable<String, Object> result = (Hashtable<String, Object>) obj;
                ArrayList<LoginModel> list = (ArrayList<LoginModel>) obj.get("data");
                if (list != null && list.size() > 0) {
                    LoginModel model = list.get(0);
                    //	Utility.ShowToast(getApplicationContext(), model.getMessage());

                    PlayInSharedPrefUtils.getInstance(mContext).saveStringPrefernce(PlayInSharedPrefKeys.KEY_PLAYIN_ID,model.getPlayinId());
                    PlayInSharedPrefUtils.getInstance(mContext).saveStringPrefernce(PlayInSharedPrefKeys.KEY_TOKEN, model.getToken());
                    PlayInSharedPrefUtils.getInstance(mContext).saveStringPrefernce(PlayInSharedPrefKeys.KEY_TOKEN_EXPIRE_ON, model.getTempExpiryTime());

                  /*  Intent intentProfile = new Intent(mContext, ProfileActivity.class);
                    startActivity(intentProfile);
                    finish();*/


                    //This is for temp for create module.
                    Intent intentProfile = new Intent(mContext, BaseActivity.class);
                    startActivity(intentProfile);
                    finish();
                }

                ResponseInfo responseInfo = (ResponseInfo) obj.get("responseInfo");
                if (responseInfo != null && responseInfo.getMessage() != null)
                    Utility.ShowToast(mContext, responseInfo.getMessage());

                break;

            case Constants.FORGOT_PASSWORD:
                //Hashtable<String, Object> resultForgotPassword = (Hashtable<String, Object>) obj;
                //ArrayList<LoginModel> list = (ArrayList<LoginModel>) obj.get("data");
                //Utility.ShowToast(mContext,""+resultForgotPassword.get("data"));
                //Utility.ShowToast(mContext,getResources().getString(R.string.password_sent));

                ResponseInfo responseInfoForgotPassword = (ResponseInfo) obj.get("responseInfo");
                if (responseInfoForgotPassword != null && responseInfoForgotPassword.getMessage() != null)
                    Utility.ShowToast(mContext, responseInfoForgotPassword.getMessage());

                break;

        }
    }

    private void loginRequest(String username, String password) {

        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        ArrayList<DataModel> list = new ArrayList<DataModel>();
        DataModel eMailModel = new DataModel();
        eMailModel.setKey("email");
        eMailModel.setValue(username);

        DataModel passwordModel = new DataModel();
        passwordModel.setKey("password");
        passwordModel.setValue(password);

        DataModel deviceIdentifier = new DataModel();


        deviceIdentifier.setKey("deviceIdentifier");
        deviceIdentifier.setValue(randomUUIDString);

        list.add(eMailModel);


        list.add(passwordModel);
        list.add(deviceIdentifier);

        MainController controller = new MainController(LoginActivity.this, this, Constants.LOGIN, true);
        controller.RequestService(list, Constants.BASE_URL + Constants.LOGIN_METHOD, Constants.REQUEST_TYPE_POST, Constants.HEADER_TYPE_NORMAL);
    }

    private void requestForgotPassword(String strEmailID) {
        ArrayList<DataModel> list = new ArrayList<DataModel>();
        DataModel eMailModel = new DataModel();
        eMailModel.setKey("email");
        eMailModel.setValue(strEmailID);

        list.add(eMailModel);

        MainController controller = new MainController(LoginActivity.this, this, Constants.FORGOT_PASSWORD, true);
        controller.RequestService(list, Constants.BASE_URL + Constants.FORGOT_PASSWORD_API, Constants.REQUEST_TYPE_POST, Constants.HEADER_TYPE_TOKEN);
    }

    @Override
    public void setScreenData(String obj) {

    }

    @Override
    public void setScreenMessage(String obj, byte type) {

    }

    @Override
    public void setCancelMessage(String obj, byte type) {

    }

    public void getFbKeyHash(String packageName) {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    packageName,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("YourKeyHash :", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                System.out.println("YourKeyHash: " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

    }
}
