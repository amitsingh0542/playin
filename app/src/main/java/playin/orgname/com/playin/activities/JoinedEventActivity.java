package playin.orgname.com.playin.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Hashtable;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.adapters.JoinedEventAdapter;
import playin.orgname.com.playin.adapters.MyEventAdapter;
import playin.orgname.com.playin.base.BaseActivity;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.JoinedEventModel;
import playin.orgname.com.playin.model.MyEventModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.preference.PlayInSharedPrefKeys;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;

public class JoinedEventActivity extends BaseActivity implements OnClickListener,DisplableInterface
{
	public static final String TAG = "JoinedEventActivity";

	private Context mContext;
	private ListView lvJoinedEvent;
	private TextView tvHeaderTitle,tvHeaderDone;
	private ImageView imgPrevious;

	private ArrayList<JoinedEventModel>  listJoinedEvent=new ArrayList<JoinedEventModel> ();


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		//getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_joined_event);
		mContext=getApplicationContext();
		initViews();
		initData();
		initListener();
	}

	private void initViews()
	{
		imgPrevious=(ImageView)findViewById(R.id.img_previous);
		tvHeaderTitle=(TextView)findViewById(R.id.tv_header_topbar);
		tvHeaderDone=(TextView)findViewById(R.id.tv_done_topbar);
		imgPrevious.setVisibility(View.INVISIBLE);
		tvHeaderDone.setVisibility(View.GONE);
		lvJoinedEvent=(ListView)findViewById(R.id.lv_joined_event);
		tvHeaderTitle.setText("Joined Events");
		tvHeaderDone.setText("Create");
	}

	private void initListener()
	{
		tvHeaderTitle.setOnClickListener(this);
		tvHeaderDone.setOnClickListener(this);

		lvJoinedEvent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				/*MyEventModel objMyEventModel = (MyEventModel)lvMyEvent.getAdapter().getItem(position);
				objMyEventModel.getEventId();*/

			}
		});





	}
	private void initData()
	{
		getJoinedEventList();
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.tv_header_topbar:
				break;

		}
	}


	private void getJoinedEventList()
	{
		MainController controller = new MainController(JoinedEventActivity.this, this, Constants.JOINED_EVENT, true);
		String finalUrl= Constants.BASE_URL + Constants.JOINED_EVENT_API;
		controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN_TOKEN);
	}


	@Override
	public void setScreenData(Hashtable obj, byte type, String Responce) {
		switch (type)
		{
			case Constants.JOINED_EVENT:
				Hashtable<String, Object> hashtableMyEvent = (Hashtable<String, Object>) obj;
				  listJoinedEvent = (ArrayList< JoinedEventModel>) obj.get("data");
				if (listJoinedEvent != null &&   listJoinedEvent.size() > 0)
				{
					JoinedEventAdapter joinedEventAdapter=new JoinedEventAdapter(mContext, listJoinedEvent);
					lvJoinedEvent.setAdapter(joinedEventAdapter);
				}

				ResponseInfo responseInfoEventForMe = (ResponseInfo) obj.get("responseInfo");
				if (responseInfoEventForMe != null && responseInfoEventForMe.getMessage() != null) {
					if(!TextUtils.isEmpty(responseInfoEventForMe.getMessage()))
					Utility.ShowToast(mContext, responseInfoEventForMe.getMessage());
				}

				break;

		}
	}

	@Override
	public void setScreenData(String obj) {

	}

	@Override
	public void setScreenMessage(String obj, byte type) {

	}

	@Override
	public void setCancelMessage(String obj, byte type) {

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==RESULT_OK)
		{
			switch (requestCode)
			{

			}
		}
	}
}
