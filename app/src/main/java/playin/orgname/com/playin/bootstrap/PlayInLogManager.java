package playin.orgname.com.playin.bootstrap;
/**
 * @author Ashutosh.Srivastava
 */
import android.util.Log;



public class PlayInLogManager {
 //private static enum MODE{USER,DEVELOPER};
	static int log_flag=1;//DEVELOPER MODE

	public static void d(String tag,String msg)
	{
		if(log_flag==1)		
			Log.d(tag, msg);		
	}
	public static void e(String tag,String msg)
	{
		if(log_flag==1)		
			Log.e(tag, msg);		
	}
	public static void i(String tag,String msg)
	{
		if(log_flag==1)		
			Log.i(tag, msg);		
	}
	public static void w(String tag,String msg)
	{
		if(log_flag==1)		
			Log.w(tag, msg);		
	}
	public static void v(String tag,String msg)
	{
		if(log_flag==1)		
			Log.v(tag, msg);		
	}
}
