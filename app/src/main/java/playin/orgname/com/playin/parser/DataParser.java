package playin.orgname.com.playin.parser;

import android.app.Activity;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Hashtable;

import playin.orgname.com.playin.components.PrefrenceManager;
import playin.orgname.com.playin.model.EventDetailsLocationModel;
import playin.orgname.com.playin.model.EventDetailsModel;
import playin.orgname.com.playin.model.EventForMeModel;
import playin.orgname.com.playin.model.JoinedEventModel;
import playin.orgname.com.playin.model.LoginModel;
import playin.orgname.com.playin.model.MyEventModel;
import playin.orgname.com.playin.model.MyTeamsModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.model.SignUpModel;
import playin.orgname.com.playin.model.SportsModel;
import playin.orgname.com.playin.model.TeamDetailsModel;
import playin.orgname.com.playin.model.UpComingEventModel;
import playin.orgname.com.playin.model.UserModel;

public class DataParser {

    private static DataParser iInstance;
    private static Activity mActivity = null;
    private static PrefrenceManager session = null;

    public DataParser(Activity iActivity) {
        mActivity = iActivity;
    }

    public DataParser() {
    }


    public static DataParser getParseInstance(Activity iActivity) {

        if (iInstance == null) {
            if (mActivity != null) {
                iInstance = new DataParser(mActivity);
                session = new PrefrenceManager(mActivity.getApplicationContext());
            } else
                iInstance = new DataParser();

        }
        return iInstance;
    }

    public Hashtable<String, Object> getRegisterResponse(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        ArrayList<SignUpModel> list = new ArrayList<SignUpModel>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                            SignUpModel model = new SignUpModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));
                            String playinId = obj.getString("playinId");
                            String token = obj.getString("token");
                            String tempExpiryTime = obj.getString("tokenExpiresOn");

                            model.setPlayinId(playinId);
                            model.setToken(token);
                            model.setTempExpiryTime(tempExpiryTime);
                            list.add(model);

                        }

                        result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }


    public Hashtable<String, Object> getLoginResponse(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        ArrayList<LoginModel> list = new ArrayList<LoginModel>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                            LoginModel loginModel = new LoginModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));

                            String playinId = obj.getString("playinId");
                            String tempExpiryTime = obj.getString("tokenExpiresOn");
                            String token = obj.getString("token");
                            loginModel.setPlayinId(playinId);
                            loginModel.setTempExpiryTime(tempExpiryTime);
                            loginModel.setToken(token);
                            list.add(loginModel);
                        }

                        result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("LoginResponse", response);
        }
        return result;

    }


    public Hashtable<String, Object> getCreateEventResponse(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        ArrayList<UserModel> list = new ArrayList<UserModel>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                           /* UserModel userModel = new UserModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));
                            list.add(userModel);*/
                        }

                        result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("CreateEvent", response);
        }
        return result;

    }

    public Hashtable<String, Object> getUserDetails(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        ArrayList<UserModel> list = new ArrayList<UserModel>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                            UserModel userModel = new UserModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));

                            String playinId = obj.getString("playinId");
                            String firstName = obj.getString("firstName");
                            String lastName = obj.getString("lastName");
                            String mobile = obj.getString("mobile");
                            String email = obj.getString("email");
                            String password = obj.getString("password");
                            String timestampOfBirth = obj.getString("timestampOfBirth");
                            String gender = obj.getString("gender");

                            String location = obj.getString("location");
                            boolean isMobileVerified = obj.getBoolean("isMobileVerified");
                            boolean isEmailVerified = obj.getBoolean("isEmailVerified");
                            String responseInfo = obj.getString("responseInfo");
                            String oauth = obj.getString("oauth");
                            String fboauth = obj.getString("fboauth");
                            String googleUserId = obj.getString("googleUserId");
                            String facebookUserId = obj.getString("facebookUserId");
                            String tempPassword = obj.getString("tempPassword");
                            String tempExpiryTime = obj.getString("tempExpiryTime");
                            String isTempPassword = obj.getString("isTempPassword");
                            String imagePath = obj.getString("imagePath");
                            boolean isSuperUser = obj.getBoolean("isSuperUser");
                            int playinStatus = obj.getInt("playinStatus");
                            String featureAccessLevel = obj.getString("featureAccessLevel");
                            String title = obj.getString("title");
                            String message = obj.getString("message");
                            int responseCode = obj.getInt("responseCode");


                            userModel.setPlayinId(playinId);
                            userModel.setFirstName(firstName);
                            userModel.setLastName(lastName);
                            userModel.setMobile(mobile);
                            userModel.setEmail(email);
                            userModel.setPassword(password);
                            userModel.setTimestampOfBirth(timestampOfBirth);
                            userModel.setGender(gender);
                            userModel.setLocation(location);
                            userModel.setIsMobileVerified(isMobileVerified);
                            userModel.setIsEmailVerified(isEmailVerified);

                            userModel.setResponseInfo(responseInfo);
                            userModel.setOauth(oauth);
                            userModel.setFboauth(fboauth);
                            userModel.setGoogleUserId(googleUserId);
                            userModel.setFacebookUserId(facebookUserId);
                            userModel.setTempPassword(tempPassword);
                            userModel.setTempExpiryTime(tempExpiryTime);
                            userModel.setIsTempPassword(isTempPassword);
                            userModel.setImagePath(imagePath);
                            userModel.setIsSuperUser(isSuperUser);
                            userModel.setPlayinStatus(playinStatus);
                            userModel.setFeatureAccessLevel(featureAccessLevel);
                            userModel.setTitle(title);
                            userModel.setMessage(message);
                            userModel.setResponseCode(responseCode);
                            list.add(userModel);
                        }

                        result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("UserDetails", response);
        }
        return result;

    }

    public Hashtable<String, Object> updateUserDetails(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("UserUpdateDetails", response);
        }
        return result;

    }

    public Hashtable<String, Object> getForgotPasswordDetails(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    if (resultStr != null) {
                        object = new JSONTokener(resultStr).nextValue();
                        if (object instanceof JSONObject) {

                        }
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);
                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("ForgotPassword", response);
        }
        return result;

    }

    public Hashtable<String, Object> getVerifyOtp(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    if (resultStr != null) {
                        object = new JSONTokener(resultStr).nextValue();
                        if (object instanceof JSONObject) {

                        }
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);
                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("ForgotPassword", response);
        }
        return result;

    }


    public Hashtable<String, Object> getMyEvent(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        ArrayList<MyEventModel> list = new ArrayList<MyEventModel>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                            MyEventModel myEventModel = new MyEventModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));

                            String eventId = obj.getString("eventId");
                            String creatorPlayinId = obj.getString("creatorPlayinId");
                            String title = obj.getString("title");
                            String eventStartDate = obj.getString("eventStartDate");
                            int sportId = obj.getInt("sportId");
                            String area = obj.getString("area");
                            String city = obj.getString("city");

                            myEventModel.setEventId(eventId);
                            myEventModel.setCreatorPlayinId(creatorPlayinId);
                            myEventModel.setTitle(title);
                            myEventModel.setEventStartDate(eventStartDate);
                            myEventModel.setSportId(sportId);
                            myEventModel.setArea(area);
                            myEventModel.setCity(city);
                            list.add(myEventModel);
                        }

                        result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("LoginResponse", response);
        }
        return result;

    }


    public Hashtable<String, Object> getEventForMe(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        ArrayList<EventForMeModel> list = new ArrayList<EventForMeModel>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                            EventForMeModel eventForMeModel = new EventForMeModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));
                            String eventId = obj.getString("eventId");
                            String title = obj.getString("title");
                            String eventStartDate = obj.getString("eventStartDate");
                            int sportId = obj.getInt("sportId");
                            double latitude = obj.getDouble("latitude");
                            double longitude = obj.getDouble("longitude");
                            String area = obj.getString("area");
                            String city = obj.getString("city");

                            eventForMeModel.setEventId(eventId);
                            eventForMeModel.setTitle(title);
                            eventForMeModel.setEventStartDate(eventStartDate);
                            eventForMeModel.setSportId(sportId);
                            eventForMeModel.setLatitude(latitude);
                            eventForMeModel.setLongitude(longitude);
                            eventForMeModel.setArea(area);
                            eventForMeModel.setCity(city);

                            list.add(eventForMeModel);
                        }

                        result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("LoginResponse", response);
        }
        return result;

    }

    public Hashtable<String, Object> getEventDetails(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        ArrayList<EventDetailsModel> list = new ArrayList<EventDetailsModel>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                            EventDetailsModel eventDetailsModel = new EventDetailsModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));
                            if (obj != null) {

                                eventDetailsModel.setEventId(obj.get("eventId").toString());
                                eventDetailsModel.setTitle(obj.get("title").toString());
                                eventDetailsModel.setDescription(obj.get("description").toString());
                                eventDetailsModel.setSportId(Integer.parseInt(obj.getString("sportId").toString()));
                                eventDetailsModel.setPlayinId(obj.get("playinId").toString());
                                eventDetailsModel.setCreatorName(obj.get("creatorName").toString());
                                eventDetailsModel.setEntityType(Integer.parseInt(obj.getString("entityType")));
                                eventDetailsModel.setEventStartDate(obj.getLong("eventStartDate"));
                                eventDetailsModel.setEventEndDate(obj.getLong("eventEndDate"));
                                eventDetailsModel.setPreferredPlayers(obj.get("preferredPlayers").toString());

                                eventDetailsModel.setIsTeamEvent(obj.getBoolean("isTeamEvent"));
                                eventDetailsModel.setNumberOfPlayersRequired(obj.getInt("numberOfPlayersRequired"));
                                eventDetailsModel.setNumberOfPlayersJoined(obj.getInt("numberOfPlayersJoined"));

                                EventDetailsLocationModel eventDetailsLocationModel = new EventDetailsLocationModel();

                                JSONObject objLocation = new JSONObject(obj.getJSONObject("location").toString());

                                eventDetailsLocationModel.setLocationId(objLocation.getInt("locationId"));
                                eventDetailsLocationModel.setLatitude(objLocation.getDouble("latitude"));
                                eventDetailsLocationModel.setLongitude(objLocation.getInt("longitude"));
                                eventDetailsLocationModel.setGeoCodeAddress(objLocation.getString("geoCodeAddress").toString());
                                eventDetailsLocationModel.setUserEnteredAddress(objLocation.get("userEnteredAddress").toString());
                                eventDetailsLocationModel.setShortAddress(objLocation.getString("shortAddress").toString());
                                eventDetailsLocationModel.setShortAddress(objLocation.getString("area").toString());
                                eventDetailsLocationModel.setShortAddress(objLocation.getString("city").toString());
                                eventDetailsLocationModel.setIsEventLocation(objLocation.getBoolean("isEventLocation"));
                                eventDetailsLocationModel.setLocationName(objLocation.getString("locationName").toString());

                                eventDetailsModel.setLocation(eventDetailsLocationModel);
                                eventDetailsModel.setIsCreatorAParticipant(obj.getBoolean("isCreatorAParticipant"));
                                eventDetailsModel.setImagePath(obj.getString("imagePath"));
                                eventDetailsModel.setNeedsCreatorPermissionToJoinEvent(obj.getBoolean("needsCreatorPermissionToJoinEvent"));
                                eventDetailsModel.setJoinStatus(obj.getInt("joinStatus"));

                                list.add(eventDetailsModel);
                            }
                        }

                        result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("UserDetails", response);
        }
        return result;

    }


    public Hashtable<String, Object> getAllSports(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        ArrayList<SportsModel> list = new ArrayList<SportsModel>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                            SportsModel sportsModel = new SportsModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));
                            if (obj != null) {

                                sportsModel.setSportId(obj.getInt("sportId"));
                                sportsModel.setName(obj.getString("name"));
                                sportsModel.setSportId(obj.getInt("numOfPlayers"));
                                list.add(sportsModel);
                            }
                        }

                        result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("UserDetails", response);
        }
        return result;

    }

    public Hashtable<String, Object> confirmEvent(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                            SportsModel sportsModel = new SportsModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));
                            if (obj != null) {
                            }
                        }

                        //result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("UserDetails", response);
        }
        return result;

    }


    public Hashtable<String, Object> updateEvent(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                            SportsModel sportsModel = new SportsModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));
                            if (obj != null) {
                            }
                        }

                        //result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("UpdateEvent", response);
        }
        return result;

    }


    public Hashtable<String, Object> deleteEvent(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                            SportsModel sportsModel = new SportsModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));
                            if (obj != null) {
                            }
                        }
                        //result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("DeleteEvent", response);
        }
        return result;
    }

    public Hashtable<String, Object> getUpComingEvent(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        ArrayList<UpComingEventModel> list = new ArrayList<UpComingEventModel>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                            UpComingEventModel upComingEventModel = new UpComingEventModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));
                            if (obj != null) {


                                list.add(upComingEventModel);
                            }
                        }

                        result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("UserDetails", response);
        }
        return result;

    }

    public Hashtable<String, Object> getJoinedEvent(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        ArrayList<JoinedEventModel> list = new ArrayList<JoinedEventModel>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                            JoinedEventModel myEventModel = new JoinedEventModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));

                            String eventId = obj.getString("eventId");
                            String creatorPlayinId = obj.getString("creatorPlayinId");
                            String title = obj.getString("title");
                            String eventStartDate = obj.getString("eventStartDate");
                            int sportId = obj.getInt("sportId");
                            String area = obj.getString("area");
                            String city = obj.getString("city");

                            myEventModel.setEventId(eventId);
                            myEventModel.setCreatorPlayinId(creatorPlayinId);
                            myEventModel.setTitle(title);
                            myEventModel.setEventStartDate(eventStartDate);
                            myEventModel.setSportId(sportId);
                            myEventModel.setArea(area);
                            myEventModel.setCity(city);
                            list.add(myEventModel);
                        }

                        result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("JoinedResponse", response);
        }
        return result;

    }


    public Hashtable<String, Object> getMyTeams(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        ArrayList<MyTeamsModel> list = new ArrayList<MyTeamsModel>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                            MyTeamsModel myTeamsModel = new MyTeamsModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));

                            myTeamsModel.setTeamId(obj.getInt("teamId"));
                            myTeamsModel.setName(obj.getString("name"));
                            myTeamsModel.setNumberOfPlayersRequired(obj.getInt("numberOfPlayersRequired"));
                            myTeamsModel.setNumberOfPlayersJoined(obj.getInt("numberOfPlayersJoined"));
                            myTeamsModel.setSport(obj.getString("sport"));
                            myTeamsModel.setCreatedBy(obj.getString("CreatedBy"));
                            myTeamsModel.setCreatorPlayinId(obj.getString("creatorPlayinId"));
                            myTeamsModel.setCreatorImage(obj.getString("creatorImage"));
                            list.add(myTeamsModel);
                        }

                        result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("MyTeamsResponse", response);
        }
        return result;

    }

    public Hashtable<String, Object> getCreateTeamResponse(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        ArrayList<MyTeamsModel> list = new ArrayList<MyTeamsModel>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                           /* UserModel userModel = new UserModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));
                            list.add(userModel);*/
                        }

                        result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("CreateTeams", response);
        }
        return result;

    }

    public Hashtable<String, Object> getMyTeamDetails(String response) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        ArrayList<TeamDetailsModel> list = new ArrayList<TeamDetailsModel>();
        Object object = null;
        if (response != null) {
            try {
                JSONObject resultObj = new JSONObject(response);
                if (resultObj.has("results")) {
                    String resultStr = resultObj.getString("results");
                    object = new JSONTokener(resultStr).nextValue();
                    if (object instanceof JSONObject) {

                    } else if (object instanceof JSONArray) {
                        JSONArray resultArray = new JSONArray(resultStr);
                        for (int i = 0; i < resultArray.length(); i++) {
                            TeamDetailsModel teamDetailsModel = new TeamDetailsModel();
                            JSONObject obj = new JSONObject(resultArray.getString(i));
                            if (obj != null) {

                                teamDetailsModel.setTeamId(obj.getInt("teamId"));
                                teamDetailsModel.setName(obj.getString("name"));
                                teamDetailsModel.setNumberOfPlayersRequired(obj.getInt("numberOfPlayersRequired"));
                                teamDetailsModel.setStandbyPlayers(obj.getInt("standbyPlayers"));
                                teamDetailsModel.setNumberOfPlayersJoined(obj.getInt("numberOfPlayersJoined"));
                                teamDetailsModel.setPreferredPlayers(obj.getInt("preferredPlayers"));
                                teamDetailsModel.setCreatedByPlayinId(obj.getString("createdByentityType"));
                                teamDetailsModel.setSportId(obj.getInt("sportId"));


                                EventDetailsLocationModel eventDetailsLocationModel = new EventDetailsLocationModel();

                                JSONObject objLocation = new JSONObject(obj.getJSONObject("preferredLocation").toString());

                                eventDetailsLocationModel.setLocationId(objLocation.getInt("locationId"));
                                eventDetailsLocationModel.setLatitude(objLocation.getDouble("latitude"));
                                eventDetailsLocationModel.setLongitude(objLocation.getInt("longitude"));
                                eventDetailsLocationModel.setGeoCodeAddress(objLocation.getString("geoCodeAddress").toString());
                                eventDetailsLocationModel.setUserEnteredAddress(objLocation.get("userEnteredAddress").toString());
                                eventDetailsLocationModel.setShortAddress(objLocation.getString("shortAddress").toString());
                                eventDetailsLocationModel.setShortAddress(objLocation.getString("area").toString());
                                eventDetailsLocationModel.setShortAddress(objLocation.getString("city").toString());
                                eventDetailsLocationModel.setIsEventLocation(objLocation.getBoolean("isEventLocation"));
                                eventDetailsLocationModel.setLocationName(objLocation.getString("locationName").toString());

                                teamDetailsModel.setIsCreatorATeamMember(obj.getBoolean("isCreatorATeamMember"));
                                teamDetailsModel.setNeedsCreatorPermissionToJoin(obj.getBoolean("needsCreatorPermissionToJoin"));
                                teamDetailsModel.setImagePath(obj.getString("imagePath"));
                                teamDetailsModel.setEntityType(obj.getInt("entityType"));
                                teamDetailsModel.setSearchKeyword(obj.getString("searchKeyword"));
                                teamDetailsModel.setJoinStatus(obj.getInt("joinStatus"));

                                teamDetailsModel.setPreferredLocation(eventDetailsLocationModel);
                                list.add(teamDetailsModel);
                            }
                        }

                        result.put("data", list);
                    }

                    String responseInfo = resultObj.getString("responseInfo");
                    if (responseInfo != null) {
                        ResponseInfo responseInfoModel = new ResponseInfo();

                        JSONObject responseObj = new JSONObject(responseInfo);
                        String title = responseObj.getString("title");
                        String message = responseObj.getString("message");
                        int responseCode = responseObj.getInt("responseCode");

                        responseInfoModel.setTitle(title);
                        responseInfoModel.setMessage(message);
                        responseInfoModel.setResponseCode(responseCode);

                        result.put("responseInfo", responseInfoModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.v("TeamDetails", response);
        }
        return result;

    }

}
