package playin.orgname.com.playin.controller;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Build;

import java.util.ArrayList;
import java.util.Hashtable;

import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.connection.GetHttpConnector;
import playin.orgname.com.playin.connection.Request;
import playin.orgname.com.playin.connection.VolleyConnection;
import playin.orgname.com.playin.connection.VolleyConnectionForPost;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.interfaces.ConnManagerInterface;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.DataModel;
import playin.orgname.com.playin.parser.DataParser;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;

public class MainController implements ConnManagerInterface {

    private DisplableInterface iDispInterface;
    private Context mContext;
    private VolleyConnection connection;
    private VolleyConnectionForPost volleyConnectionForPost;
    private byte TYPE = -1;
    private boolean isLoaderVisible = false;
    private DataParser iParser;
    AssetManager am;
    private GetHttpConnector iHttpServer;
    private String KEY="KEY";

    public MainController(Activity activity, DisplableInterface dispInterface, byte type, boolean _isLoaderVisible) {
        this.mContext = activity.getApplicationContext();
        this.iDispInterface = dispInterface;
        this.connection = new VolleyConnection(activity, this);
        this.volleyConnectionForPost = new VolleyConnectionForPost(activity, this, null);
        this.TYPE = type;
        iParser = DataParser.getParseInstance(activity);
        this.isLoaderVisible = _isLoaderVisible;
        iHttpServer = new GetHttpConnector(activity, this, _isLoaderVisible);
    }


    public void RequestService(ArrayList<DataModel> list, String url,byte requestType,String headerType) {
        if(Utility.isInternetAvailable(mContext))
        {
            String jsonRequest = Utility.getJsonRequest(list);
            Request request = new Request();
            request.setRequest(jsonRequest);
            request.setRequestType(requestType);
            request.setHeaderType(headerType);
            request.setUrl(url);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                iHttpServer.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, request);
            } else {
                iHttpServer.execute(request);
            }
        }
        else
        {
            String response= PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(KEY+TYPE);
            successResponseProcess(response);
        }
    }

    public void RequestServiceGet(String url,byte requestType,String headerType) {
        if(Utility.isInternetAvailable(mContext))
        {
            Request request = new Request();
            request.setUrl(url);
            request.setRequestType(requestType);
            request.setHeaderType(headerType);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                iHttpServer.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, request);
            } else {
                iHttpServer.execute(request);
            }
        }
        else
        {
            String response= PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(KEY+TYPE);
            successResponseProcess(response);
        }
    }

    public void PostMethod(String url, ArrayList<DataModel> list) {
        volleyConnectionForPost.makeRequest(list, url, true);
    }

    @Override
    public void successResponseProcess(String response) {
        // TODO Auto-generated method stub


        switch (TYPE) {

            case Constants.REGISTER:
                if (response != null)
                {
                    PlayInSharedPrefUtils.getInstance(mContext).saveStringPrefernce(KEY+TYPE,response);
                    System.out.println("REGISTER response >> " + response);
                    Hashtable<String, Object> registerResponse = iParser.getRegisterResponse(response);
                    iDispInterface.setScreenData(registerResponse, Constants.REGISTER, null);
                }
                break;

            case Constants.LOGIN:
                if (response != null) {
                    PlayInSharedPrefUtils.getInstance(mContext).saveStringPrefernce(KEY+TYPE, response);
                    System.out.println("LOGIN response >> " + response);
                    Hashtable<String, Object> loginResponse = iParser.getLoginResponse(response);
                    iDispInterface.setScreenData(loginResponse, Constants.LOGIN, null);
                }
                break;

            case Constants.FORGOT_PASSWORD:
                if (response != null) {
                    System.out.println(response);
                    Hashtable<String, Object> forgotMessage = iParser.getForgotPasswordDetails(response);
                    iDispInterface.setScreenData(forgotMessage, Constants.FORGOT_PASSWORD, null);
                }
                break;

            case Constants.UPDATE_USER_DETAILS:
                if (response != null) {
                    System.out.println("User Update Response >> " + response);
                    Hashtable<String, Object> updateUserDetails = iParser.updateUserDetails(response);
                    iDispInterface.setScreenData(updateUserDetails, Constants.UPDATE_USER_DETAILS, null);
                }
                break;

            case Constants.GET_USER_BY_ID:
                if (response != null) {
                    System.out.println("GET User Response >> " + response);
                    Hashtable<String, Object> getUserDetails = iParser.getUserDetails(response);
                    iDispInterface.setScreenData(getUserDetails, Constants.GET_USER_BY_ID, null);
                }
                break;

            case Constants.EVENT_FOR_ME:
                if (response != null) {
                    System.out.println("EVENT_FOR_ME Response >> " + response);
                    Hashtable<String, Object> hashtableEventForMe = iParser.getEventForMe(response);
                    iDispInterface.setScreenData(hashtableEventForMe, Constants.EVENT_FOR_ME, null);
                }
                break;

            case Constants.VERIFY_OTP:
                if (response != null) {
                    System.out.println("VERIFY_OTP Response >> " + response);
                    Hashtable<String, Object> hashtableVerifyOtp = iParser.getVerifyOtp(response);
                    iDispInterface.setScreenData(hashtableVerifyOtp, Constants.VERIFY_OTP, null);
                }
                break;

            case Constants.CREATE_EVENT:
                if (response != null) {
                    System.out.println("CREATE_EVENT Response >> " + response);
                    Hashtable<String, Object> hashtableCreateEvent = iParser.getCreateEventResponse(response);
                    iDispInterface.setScreenData(hashtableCreateEvent, Constants.CREATE_EVENT, null);
                }
                break;

            case Constants.MY_EVENT:
                if (response != null) {
                    System.out.println("MY_EVENT Response >> " + response);
                    Hashtable<String, Object> hashtableMyEvent = iParser.getMyEvent(response);
                    iDispInterface.setScreenData(hashtableMyEvent, Constants.MY_EVENT, null);
                }
                break;

            case Constants.EVENT_DETAILS:
                if (response != null) {
                    System.out.println("EVENT_DETAILS Response >> " + response);
                    Hashtable<String, Object> hashtableEventDetails = iParser.getEventDetails(response);
                    iDispInterface.setScreenData(hashtableEventDetails, Constants.EVENT_DETAILS, null);
                }
                break;

            case Constants.GET_ALL_SPORTS:
                if (response != null) {
                    System.out.println("GET_ALL_SPORTS Response >> " + response);
                    Hashtable<String, Object> hashtableAllSports= iParser.getAllSports(response);
                    iDispInterface.setScreenData(hashtableAllSports, Constants.GET_ALL_SPORTS, null);
                }
                break;

            case Constants.CONFIRM_EVENT:
                if (response != null) {
                    System.out.println("CONFIRM_EVENT Response >> " + response);
                    Hashtable<String, Object> hashtableConfirmEvent= iParser.confirmEvent(response);
                    iDispInterface.setScreenData(hashtableConfirmEvent, Constants.CONFIRM_EVENT, null);
                }
                break;

            case Constants.UPDATE_EVENT:
                if (response != null) {
                    System.out.println("UPDATE_EVENT Response >> " + response);
                    Hashtable<String, Object> hashtableUpdateEvent= iParser.updateEvent(response);
                    iDispInterface.setScreenData(hashtableUpdateEvent, Constants.UPDATE_EVENT, null);
                }
                break;

            case Constants.DELETE_EVENT:
                if (response != null) {
                    System.out.println("DELETE_EVENT Response >> " + response);
                    Hashtable<String, Object> hashtableDeleteEvent= iParser.deleteEvent(response);
                    iDispInterface.setScreenData(hashtableDeleteEvent, Constants.DELETE_EVENT, null);
                }
                break;

            case Constants.UPCOMING_EVENT:
                if (response != null) {
                    System.out.println("UPCOMING_EVENT Response >> " + response);
                    Hashtable<String, Object> hashtableUpcomingEvent= iParser.getUpComingEvent(response);
                    iDispInterface.setScreenData(hashtableUpcomingEvent, Constants.UPCOMING_EVENT, null);
                }
                break;

            case Constants.JOINED_EVENT:
                if (response != null) {
                    System.out.println("UPCOMING_EVENT Response >> " + response);
                    Hashtable<String, Object> hashtableJoinedEvent= iParser.getJoinedEvent(response);
                    iDispInterface.setScreenData(hashtableJoinedEvent, Constants.JOINED_EVENT, null);
                }
                break;

            default:
                break;


            case Constants.CREATE_TEAM:
                if (response != null) {
                    System.out.println("CREATE_TEAM Response >> " + response);
                    Hashtable<String, Object> hashtableCreateTeams = iParser.getCreateTeamResponse(response);
                    iDispInterface.setScreenData(hashtableCreateTeams, Constants.CREATE_TEAM, null);
                }
                break;

            case Constants.MY_TEAM:
                if (response != null) {
                    System.out.println("MY_TEAM Response >> " + response);
                    Hashtable<String, Object> hashtableMyTeams = iParser.getMyTeams(response);
                    iDispInterface.setScreenData(hashtableMyTeams, Constants.MY_TEAM, null);
                }
                break;

            case Constants.MY_TEAMS_DETAILS:
                if (response != null) {
                    System.out.println("MY_TEAMS_DETAILS Response >> " + response);
                    Hashtable<String, Object> hashtableMyTeamsDetails = iParser.getMyTeamDetails(response);
                    iDispInterface.setScreenData(hashtableMyTeamsDetails, Constants.MY_TEAMS_DETAILS, null);
                }
                break;
        }
    }

    @Override
    public void failedResponseProcess(String response) {
        // TODO Auto-generated method stub
        System.out.println(response);
        iDispInterface.setScreenData(response);
    }

    @Override
    public void excuteError(String error) {
        // TODO Auto-generated method stub
        System.out.println(error);
    }


}
