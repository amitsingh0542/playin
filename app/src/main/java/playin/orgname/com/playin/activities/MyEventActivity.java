package playin.orgname.com.playin.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Hashtable;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.adapters.MyEventAdapter;
import playin.orgname.com.playin.base.BaseActivity;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.MyEventModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.preference.PlayInSharedPrefKeys;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;

public class MyEventActivity extends BaseActivity implements OnClickListener,DisplableInterface
{
	public static final String TAG = "MyEventActivity";

	private Context mContext;
	private ListView lvMyEvent;
	private TextView tvHeaderTitle,tvHeaderDone;
	private ImageView imgPrevious;

	private ArrayList<MyEventModel> listMyEvent=new ArrayList<MyEventModel> ();
	private  final int CREATE_NEW_EVENT=0;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	//	getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_my_event);
		mContext=getApplicationContext();
		initViews();
		initData();
		initListener();
	}

	private void initViews()
	{
		imgPrevious=(ImageView)findViewById(R.id.img_previous);
		tvHeaderTitle=(TextView)findViewById(R.id.tv_header_topbar);
		tvHeaderDone=(TextView)findViewById(R.id.tv_done_topbar);
		imgPrevious.setVisibility(View.INVISIBLE);
		tvHeaderDone.setVisibility(View.VISIBLE);
		lvMyEvent=(ListView)findViewById(R.id.lv_my_event);
		tvHeaderTitle.setText("Hosted Events");
		tvHeaderDone.setText("Create");
	}

	private void initListener()
	{
		tvHeaderTitle.setOnClickListener(this);
		tvHeaderDone.setOnClickListener(this);

		lvMyEvent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				/*MyEventModel objMyEventModel = (MyEventModel)lvMyEvent.getAdapter().getItem(position);
				objMyEventModel.getEventId();*/

				Intent intent = new Intent(mContext, EventDetailActivity.class);
				PlayInSharedPrefUtils.getInstance(mContext).saveStringPrefernce(PlayInSharedPrefKeys.KEY_EVENT_ID, listMyEvent.get(position).getEventId());
				intent.putExtra("EVENT_ID", listMyEvent.get(position).getEventId());
				startActivity(intent);
			}
		});





	}
	private void initData()
	{
		getMyEventList();
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.tv_header_topbar:
				break;

			case R.id.tv_done_topbar:
				createNewEvent();
				break;

			case R.id.btn_delete_item:
				createNewEvent();
				break;

		}
	}


	private void getMyEventList()
	{
		MainController controller = new MainController(MyEventActivity.this, this, Constants.MY_EVENT, true);
		String finalUrl= Constants.BASE_URL + Constants.MY_EVENT_API;
		controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN_TOKEN);
	}

	private void createNewEvent()
	{
		Intent intent = new Intent(mContext, CreateEventActivity.class);
		startActivityForResult(intent, CREATE_NEW_EVENT);
	}

	@Override
	public void setScreenData(Hashtable obj, byte type, String Responce) {
		switch (type)
		{
			case Constants.MY_EVENT:
				Hashtable<String, Object> hashtableMyEvent = (Hashtable<String, Object>) obj;
				listMyEvent = (ArrayList<MyEventModel>) obj.get("data");
				if (listMyEvent != null && listMyEvent.size() > 0)
				{
					MyEventAdapter myEventAdapter=new MyEventAdapter(mContext,listMyEvent);
					lvMyEvent.setAdapter(myEventAdapter);
				}

				ResponseInfo responseInfoEventForMe = (ResponseInfo) obj.get("responseInfo");
				if (responseInfoEventForMe != null && responseInfoEventForMe.getMessage() != null) {
					if(!TextUtils.isEmpty(responseInfoEventForMe.getMessage()))
					Utility.ShowToast(mContext, responseInfoEventForMe.getMessage());
				}

				break;

			case Constants.DELETE_EVENT:
				Hashtable<String, Object> hashtableDeleteEvent = (Hashtable<String, Object>) obj;
				//getMyEventList();

				ResponseInfo responseInfoDeleteEvent = (ResponseInfo) obj.get("responseInfo");
				if (responseInfoDeleteEvent != null && responseInfoDeleteEvent.getMessage() != null) {

					if(!TextUtils.isEmpty(responseInfoDeleteEvent.getMessage()))
					Utility.ShowToast(mContext, responseInfoDeleteEvent.getMessage());
				}

				break;
		}
	}

	@Override
	public void setScreenData(String obj) {

	}

	@Override
	public void setScreenMessage(String obj, byte type) {

	}

	@Override
	public void setCancelMessage(String obj, byte type) {

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==RESULT_OK)
		{
			switch (requestCode)
			{
				case CREATE_NEW_EVENT:
					getMyEventList();
					break;
			}
		}
	}

	private void deteleEvent(String eventId)
	{
		MainController controller = new MainController(MyEventActivity.this, this, Constants.DELETE_EVENT, true);
		String finalUrl= Constants.BASE_URL + Constants.DELETE_EVENT_API+eventId;
		controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_POST, Constants.HEADER_TYPE_PLAY_IN_TOKEN);
	}
}
