/*
package playin.orgname.com.playin.connection;


import android.app.Activity;
import android.os.AsyncTask;

import playin.orgname.com.playin.components.ShowLoader;
import playin.orgname.com.playin.interfaces.ConnManagerInterface;

*/
/**
 * Created by Shashvat Srivastava on 6/24/2015.
 *//*

public class Connection extends AsyncTask<Request, Void, String> {
    String iContent = null;
    private Activity mActivity = null;
    private ConnManagerInterface iConInterface;
    private boolean isShowLoader = false;
    private ShowLoader dialog = null;
    public static String status_code,message;
    public Connection(Activity activity, ConnManagerInterface mConInterface, boolean isLoaderShowing) {
        mActivity = activity;
        this.iConInterface = mConInterface;
        isShowLoader = isLoaderShowing;
        dialog = ShowLoader.getInstance(mActivity);
    }

    @Override
    protected String doInBackground(Request... params) {

        String url = params[0].getUri();
        List<NameValuePair> jsonRequest = params[0].getPayload();
        String methodName = params[0].getMethodName();
        String requestStr = params[0].getPayloadStr();
    //Constants.printOnConsole("Request ",requestStr);
      */
/*  List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id", "98"));*//*

        String response = postData(url + methodName, requestStr);

        System.out.println(getTestData());

        iContent = getWebServiceData(url + methodName, mActivity, "{"+jsonRequest+"}");
    Log.e("Forgot Url",""+url+methodName);
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        if(dialog != null && dialog.isShowing())
        dialog.dismis(false);

        if(iContent != null){
            if(iContent.equalsIgnoreCase(Constants.CONNECTION_TIMED_OUT) || iContent.equalsIgnoreCase(Constants.SOCKET_TIMED_OUT) || iContent.equalsIgnoreCase(Constants.SOME_THING_WENT_WRONG))
            {
                iConInterface.failedResponseProcess(iContent);
            }
            else{

                iConInterface.successResponseProcess(iContent);
            }

        }
    Log.e("Result",""+iContent);


    }

    @Override
    protected void onPreExecute() {
        if(dialog != null && !dialog.isShowing())
        dialog.run(true);

    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }

    public String getWebServiceData(String url, final Activity activity, String _params) {
        InputStream is = null;
        String result = "";
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity;
            HttpParams params = httpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 30000);
            HttpResponse httpResponse;
            switch (1) {
                case 0:
                    HttpGet httpGet = new HttpGet(url);
                    httpResponse = httpClient.execute(httpGet);
                    httpEntity = httpResponse.getEntity();
                    is = httpEntity.getContent();
                    break;
                case 1:
                    */
/*HttpPost httpPost = new HttpPost(url);
                    //String _params = null;
                    StringEntity entity = new StringEntity(_params);
                    httpPost.setEntity(entity);
                    httpPost.setHeader("Content-Type", "application/json");
                    httpPost.setHeader("Accept", "application/json");
                    httpResponse = httpClient.execute(httpPost);
                    httpEntity = httpResponse.getEntity();
                    is = httpEntity.getContent();*//*

                    String data = makeRequest(url, _params.toString());
                    Constants.printOnConsole("Res ", data);
                    break;
            }

        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
        } catch (ClientProtocolException e) {

            e.printStackTrace();
        } catch (ConnectTimeoutException e) {
            iContent = Constants.CONNECTION_TIMED_OUT;

            Log.e("CONN TIMEOUT", e.toString());

        } catch (SocketTimeoutException e) {


            Log.e("SOCK TIMEOUT", e.toString());
            iContent = Constants.SOCKET_TIMED_OUT;
        } catch (IOException e) {

            e.printStackTrace();
        } catch (NullPointerException e) {

            e.printStackTrace();
        } catch (Exception e) {
            Log.e("OTHER EXCEPTIONS", e.toString());
            iContent = Constants.SOME_THING_WENT_WRONG;
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"), 8);//is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            if (is != null)
                is.close();
            iContent = sb.toString();
            // Log.e("STRING",sb.toString());
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());

        }


       return iContent;
    }


    public static String makeRequest(String uri, String json) {
        HttpURLConnection urlConnection;
        String url;
        String data = json;
        String result = null;
        try {
            //Connect
            urlConnection = (HttpURLConnection) ((new URL(uri).openConnection()));
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.connect();

            //Write
            OutputStream outputStream = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            writer.write(data);
            writer.close();
            outputStream.close();

            //Read
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));

            String line = null;
            StringBuilder sb = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            bufferedReader.close();
            result = sb.toString();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }



    public String getTestData() {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id", "98"));
        String response = getJSONFromUrl("http://52.74.155.166/settings/getcitylist.php", params);
        return response;
    }

    public String getJSONFromUrl(String url, List<NameValuePair> params) {
        StringBuilder sb =null;
        String response =null;
        InputStream is = null;
        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            // httpPost.set
            httpPost.setEntity(new UrlEncodedFormEntity(params));

            HttpResponse httpResponse = httpClient.execute(httpPost);
            // httpResponse.get
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            response = sb.toString();
            //			Log.e("JSON", json);
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        return response;
    }

    public String postData(String url,String obj) {
        // Create a new HttpClient and Post Header
    String responsedata = null;
        HttpParams myParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(myParams, 10000);
        HttpConnectionParams.setSoTimeout(myParams, 10000);
        HttpClient httpclient = new DefaultHttpClient(myParams );

        try {

            HttpPost httppost = new HttpPost(url.toString());
            httppost.setHeader("Content-type", "application/json");

            StringEntity se = new StringEntity(obj);
            se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            httppost.setEntity(se);

            HttpResponse response = httpclient.execute(httppost);
            String temp = EntityUtils.toString(response.getEntity());
            responsedata = temp;
            Log.i("tag", temp);


        } catch (ClientProtocolException e) {

        } catch (IOException e) {
        }
        return responsedata;
    }

}*/
