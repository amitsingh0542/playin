package playin.orgname.com.playin.preference;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import playin.orgname.com.playin.bootstrap.PlayInApplicationBootstrap;

/**
 * @author Ashutosh srivastava
 */
@SuppressLint("CommitPrefEdits")
public class PlayInSharedPrefUtils {

	private SharedPreferences sharedPref;
	private Editor editor;

	private Context mContext;
	private static PlayInSharedPrefUtils mPlayInSharedPrefUtils;

	private PlayInSharedPrefUtils(Context context) {
		mContext = context;
		sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		editor = sharedPref.edit();
	}
	
	private PlayInSharedPrefUtils() {
		
	}

	public static PlayInSharedPrefUtils getInstance(Context context) {
		if (mPlayInSharedPrefUtils == null) {
			mPlayInSharedPrefUtils = new PlayInSharedPrefUtils(context);
		}
		return mPlayInSharedPrefUtils;

	}

	// for fetching data from shared pref.
	public String fetchStringPrefernce(String key) {

		sharedPref = PreferenceManager.getDefaultSharedPreferences(PlayInApplicationBootstrap.getContext());
		editor = sharedPref.edit();
		return sharedPref.getString(key,"");
	}

	// for save data in to shared perf.
	public void saveStringPrefernce(String key, String value) {

		sharedPref = PreferenceManager.getDefaultSharedPreferences(PlayInApplicationBootstrap.getContext());
		editor = sharedPref.edit();
		editor.putString(key, value);
		editor.apply();

	}

	// for fetching data from shared pref.
	public boolean fetchBooleanPrefernce(String key, Boolean defaultValue) {

		return sharedPref.getBoolean(key, defaultValue);
	}

	// for save data in to shared perf.
	public void saveBooleanPrefernce(String key, Boolean value) {

		editor.putBoolean(key, value);
		editor.commit();

	}

	// for save long data in to shared perf.
	public void saveLongPrefernce(String key, long value) {
		editor.putLong(key, value);
		editor.commit();

	}

	public long fetchLongPrefernce(String key, long defaultValue) {
		return sharedPref.getLong(key, defaultValue);
	}

	// for fetching data from shared pref.
	public int fetchIntegerPrefernce(String key, int defaultValue) {
		return sharedPref.getInt(key, defaultValue);
	}

	// for save data in to shared perf.
	public void saveIntegerPrefernce(String key, int value) {
		editor.putInt(key, value);
		editor.commit();
	}

	public void removeFromSharedPreference(String key) {
		editor.remove(key);
		editor.commit();
	}

	public void saveObjectPreference(String key, Object obj) {
		editor.putString(key, new Gson().toJson(obj));
		editor.commit();
	}

	public Object fetchObjectPreference(String key, Class<?> class1) {
		return new Gson().fromJson(sharedPref.getString(key, ""), class1);
	}

	public void clearAllData() {
		editor = sharedPref.edit().clear();
		editor.commit();
	}
}
