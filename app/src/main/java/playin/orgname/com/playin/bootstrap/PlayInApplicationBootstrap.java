package playin.orgname.com.playin.bootstrap;

import android.content.Context;
import android.content.res.Configuration;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Ashutosh.Srivastava
 * 
 *         Class to receive callbacks from the Android OS. Long running tasks
 *         for initialization can be spawned here.
 * 
 *         Also, this class provides the global reference to the application's
 *         context which can be used throughout the application.
 * 
 */
public class PlayInApplicationBootstrap extends android.app.Application {
	public String LOG_TAG = playin.orgname.com.playin.bootstrap.PlayInApplicationBootstrap.class.getSimpleName();
	private List<Integer> dashboardItem = new ArrayList<Integer>();
	private int lastUpdatedLayoutHeight;
	private int accountIdLayoutHeigth;
	private int tabBarHeigth;
	private int containerHeight;
	private int accountBalancelayoutHeigth ;
	private int dashboardSelectedPage = 1;
	private int indicatorHeigth;
	private byte[] encryptionKey;
	private int selectedAccountIndex=0;
	private String selectedAccountId;
	private static Context app_context;
	private RequestQueue mRequestQueue;
	private static final String TAG = PlayInApplicationBootstrap.class.getName();
	private static PlayInApplicationBootstrap mInstance;
	private static PlayInApplicationBootstrap singleton;

	public int getContainerHeight() {
		return containerHeight;
	}
	public void setContainerHeight(int containerHeight) {
		this.containerHeight = containerHeight;
	}
	public String getSelectedAccountId() {
		return selectedAccountId;
	}
	public void setSelectedAccountId(String selectedAccountId) {
		this.selectedAccountId = selectedAccountId;
	}
	public int getSelectedAccountIndex() {
		return selectedAccountIndex;
	}
	public void setSelectedAccountIndex(int selectedAccountIndex) {
		this.selectedAccountIndex = selectedAccountIndex;
	}

	private DisplayMetrics displayMetrics;
	
	public int getLastUpdatedLayoutHeight() {
		return lastUpdatedLayoutHeight;
	}
	public void setLastUpdatedLayoutHeight(int lastUpdatedLayoutHeight) {
		this.lastUpdatedLayoutHeight = lastUpdatedLayoutHeight;
	}
	public void setTabBarHeigth(int tabBarHeigth) {
		this.tabBarHeigth = tabBarHeigth;
	}
	public List<Integer> getDashboardItem() {
		return dashboardItem;
	}
	public void setAddDashboardItem(List<Integer> dashboardItem) {
		this.dashboardItem = dashboardItem;
	}
	public int getIndicatorHeigth() {
		return indicatorHeigth;
	}
	public void setIndicatorHeigth(int indicatorHeigth) {
		this.indicatorHeigth = indicatorHeigth;
	}
	public int getAccountIdLayoutHeigth() {
		return accountIdLayoutHeigth;
	}
	public void setAccountIdLayoutHeigth(int accountIdLayoutHeigth) {
		this.accountIdLayoutHeigth = accountIdLayoutHeigth;
	}
	public int getAccountBalancelayoutHeigth() {
		return accountBalancelayoutHeigth;
	}
	public void setAccountBalancelayoutHeigth(int accountBalancelayoutHeigth) {
		this.accountBalancelayoutHeigth = accountBalancelayoutHeigth;
	}
	public byte[] getEncryptionKey() {
		return encryptionKey;
	}

	public void setEncryptionKey(byte[] encryptionKey) {
		this.encryptionKey = encryptionKey;
	}
	public int getDashboardSelectedPage() {
		return dashboardSelectedPage;
	}

	public void setDashboardSelectedPage(int dashboardSelectedPage) {
		this.dashboardSelectedPage = dashboardSelectedPage;
	}

	public int getTabBarHeigth() {
		return tabBarHeigth;
	}
	public static PlayInApplicationBootstrap getInstance() {
		return singleton;
	}
	protected void init() {
		singleton = this;
	}

	public static Context getContext()
	{
		return app_context;
	}
	@Override
	public void onCreate() {
		super.onCreate();
		displayMetrics = getDisplayMetrics();
		app_context = getApplicationContext();
		/*encryptionKey = JioCypher.getInstace(this).generateUniqueKey();
		setLocalization(MSCSharedPrefUtils.getInstance(this)
				.fetchStringPrefernce(MSCSharedPrefKeys.LANGUAGE_TYPE,
						""));*/
		this.init();
		mInstance = this;
	}

	public int deviceWidth() {
		return displayMetrics.widthPixels;
	}

	public int deviceHeight() {
		return displayMetrics.heightPixels;
	}


	public static SimpleDateFormat simpledateFormat() {
		return new SimpleDateFormat("HH:mm", Locale.ENGLISH);
	}

	public static SimpleDateFormat dividerFormat() {
		return new SimpleDateFormat("dd MMMMMMMMM yyyy", Locale.ENGLISH);
	}

	public static SimpleDateFormat simpleDateFormatEEEEhhmmaa() {
		return new SimpleDateFormat("EEEE,hh:mm  aa", Locale.ENGLISH);
	}

	public static SimpleDateFormat simpleDateFormatddMMMMMMMMMhhmmaa() {
		return new SimpleDateFormat("dd MMMMMMMMM,hh:mm  aa ", Locale.ENGLISH);
	}

	public static SimpleDateFormat simpleDateFormatMMMMMMMMMyyyy() {
		return new SimpleDateFormat("MMMMMMMMM yyyy", Locale.ENGLISH);
	}

	public static SimpleDateFormat simpleDateFormatyyyyMMdd() {
		return new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
	}

	public static SimpleDateFormat simpleDateFormatHHmmddMMyyyy() {
		return new SimpleDateFormat("HH:mm dd-MM-yyyy", Locale.ENGLISH);
	}

	public static SimpleDateFormat simpleDateFormatEEEdMMMyyyyhhmmaa() {
		return new SimpleDateFormat("EEE, d MMM yyyy hh:mm aa", Locale.ENGLISH);
	}

	public static SimpleDateFormat simpleDateFormatEEEdMMMhhmmaa() {
		return new SimpleDateFormat("EEE, d MMM hh:mm aa", Locale.ENGLISH);
	}

	public int getDeviceDensity() {
		return displayMetrics.densityDpi;
	}
	
	private DisplayMetrics getDisplayMetrics() {
		WindowManager wm = (WindowManager) this
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		return metrics;
	}


	@Override
	public void onLowMemory() {
		super.onLowMemory();

	}

	@Override
	public void onTerminate() {
		super.onTerminate();
	}

	public void setLocalization(String langType) {
		if (!TextUtils.isEmpty(langType)) {
			Locale locale = new Locale(langType);
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
	}

		/*
	 * public void touch() { waiter.touch(); }
	 */

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}

		return mRequestQueue;
	}

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}
}
