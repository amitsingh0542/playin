package playin.orgname.com.playin.connection;

/**
 * Created by Shashvat on 6/25/2016.
 */
public class Request {

    private String url = null;
    private String request = null;
    private String headerType = null;
    private byte requestType = 0;

    public String getHeaderType() {
        return headerType;
    }

    public void setHeaderType(String headerType) {
        this.headerType = headerType;
    }


    public String getUrl() {
        return url;
    }

    public byte getRequestType() {
        return requestType;
    }

    public void setRequestType(byte requestType) {
        this.requestType = requestType;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

}
