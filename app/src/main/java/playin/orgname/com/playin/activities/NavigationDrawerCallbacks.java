package playin.orgname.com.playin.activities;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
