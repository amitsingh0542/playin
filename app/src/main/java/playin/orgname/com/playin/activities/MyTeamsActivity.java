package playin.orgname.com.playin.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Hashtable;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.adapters.MyTeamsAdapter;
import playin.orgname.com.playin.base.BaseActivity;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.MyTeamsModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.preference.PlayInSharedPrefKeys;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;

public class MyTeamsActivity extends BaseActivity implements OnClickListener,DisplableInterface
{
	public static final String TAG = "MyTeamsActivity";

	private Context mContext;
	private ListView lvMyTeams;
	private TextView tvHeaderTitle,tvHeaderDone;
	private ImageView imgPrevious;

	private ArrayList<MyTeamsModel> listMyTeams=new ArrayList<MyTeamsModel> ();
	private  final int CREATE_NEW_TEAM=0;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_teams);
		mContext=getApplicationContext();
		initViews();
		initData();
		initListener();
	}

	private void initViews()
	{
		imgPrevious=(ImageView)findViewById(R.id.img_previous);
		tvHeaderTitle=(TextView)findViewById(R.id.tv_header_topbar);
		tvHeaderDone=(TextView)findViewById(R.id.tv_done_topbar);
		imgPrevious.setVisibility(View.INVISIBLE);
		tvHeaderDone.setVisibility(View.VISIBLE);
		lvMyTeams=(ListView)findViewById(R.id.lv_my_teams);
		tvHeaderTitle.setText("Team Events");
		tvHeaderDone.setText("Create");
	}

	private void initListener()
	{
		tvHeaderTitle.setOnClickListener(this);
		tvHeaderDone.setOnClickListener(this);

		lvMyTeams.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				/*MyEventModel objMyEventModel = (MyEventModel)lvMyEvent.getAdapter().getItem(position);
				objMyEventModel.getEventId();*/

				Intent intent = new Intent(mContext, TeamsDetailActivity.class);
				PlayInSharedPrefUtils.getInstance(mContext).saveStringPrefernce(PlayInSharedPrefKeys.KEY_TEAM_ID, String.valueOf(listMyTeams.get(position).getTeamId()));
				intent.putExtra("TEAM_ID", listMyTeams.get(position).getTeamId());
				startActivity(intent);
			}
		});

	}
	private void initData()
	{
		 getMyTeamList();
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.tv_header_topbar:
				break;

			case R.id.tv_done_topbar:
				createNewTeam();
				break;

			case R.id.btn_delete_item:
				createNewTeam();
				break;

		}
	}


	private void getMyTeamList()
	{
		MainController controller = new MainController(MyTeamsActivity.this, this, Constants.MY_TEAM, true);
		String finalUrl= Constants.BASE_URL + Constants.MY_TEAMS_API;
		controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN_TOKEN);
	}

	private void createNewTeam()
	{
		Intent intent = new Intent(mContext, CreateTeamsActivity.class);
		startActivityForResult(intent, CREATE_NEW_TEAM);
	}

	@Override
	public void setScreenData(Hashtable obj, byte type, String Responce) {
		switch (type)
		{
			case Constants.MY_TEAM:
				Hashtable<String, Object> hashtableMyEvent = (Hashtable<String, Object>) obj;
				listMyTeams = (ArrayList<MyTeamsModel>) obj.get("data");
				if (listMyTeams != null && listMyTeams.size() > 0)
				{
					MyTeamsAdapter myTeamsAdapter=new MyTeamsAdapter(mContext,listMyTeams);
					lvMyTeams.setAdapter(myTeamsAdapter);
				}

				ResponseInfo responseInfoEventForMe = (ResponseInfo) obj.get("responseInfo");
				if (responseInfoEventForMe != null && responseInfoEventForMe.getMessage() != null) {
					if(!TextUtils.isEmpty(responseInfoEventForMe.getMessage()))
					Utility.ShowToast(mContext, responseInfoEventForMe.getMessage());
				}

				break;

			case Constants.DELETE_EVENT:
				Hashtable<String, Object> hashtableDeleteEvent = (Hashtable<String, Object>) obj;
				//getMyEventList();

				ResponseInfo responseInfoDeleteEvent = (ResponseInfo) obj.get("responseInfo");
				if (responseInfoDeleteEvent != null && responseInfoDeleteEvent.getMessage() != null) {

					if(!TextUtils.isEmpty(responseInfoDeleteEvent.getMessage()))
					Utility.ShowToast(mContext, responseInfoDeleteEvent.getMessage());
				}

				break;
		}
	}

	@Override
	public void setScreenData(String obj) {

	}

	@Override
	public void setScreenMessage(String obj, byte type) {

	}

	@Override
	public void setCancelMessage(String obj, byte type) {

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==RESULT_OK)
		{
			switch (requestCode)
			{
				case CREATE_NEW_TEAM:
					 getMyTeamList();
					break;
			}
		}
	}

	private void deteleTeams(String eventId)
	{
		MainController controller = new MainController(MyTeamsActivity.this, this, Constants.DELETE_EVENT, true);
		String finalUrl= Constants.BASE_URL + Constants.DELETE_EVENT_API+eventId;
		controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_POST, Constants.HEADER_TYPE_PLAY_IN_TOKEN);
	}
}
