package playin.orgname.com.playin.Utility;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.DatePicker;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import playin.orgname.com.playin.model.DataModel;

/**
 * Created by Shashvat on 6/25/2016.
 */
public class Utility {

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static void ShowToast(Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    }

    public static boolean isInternetAvailable(Context activity) {
        ConnectivityManager manager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        if (info != null && info.isAvailable()) {
            return true;
        }
        else {
            return false;
        }
    }

    public static String getJsonRequest(ArrayList<DataModel> list){
        JSONObject obj = new JSONObject();
        Map<String,String> params = new HashMap<String,String>();
        for (int i=0 ; i < list.size();i++)
        {
            params.put(list.get(i).getKey(), list.get(i).getValue());

            try {
                obj.put(list.get(i).getKey(),list.get(i).getValue());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            System.out.println(list.get(i).getKey()+ " " +list.get(i).getValue());
        }

        return obj.toString();

    }


    public static String getDateFromLong(long longDate)
    {
        //long val = 1346524199000l;
        Date date=new Date(longDate);
        SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yy");
        String dateText = df2.format(date);
        System.out.println(dateText);
        return dateText;
    }

    public String openDatePicker(final Activity myActivity)
    {
        int year, month, day;
        final Calendar myCalendarDOB = Calendar.getInstance();
        year = myCalendarDOB.get(Calendar.YEAR);
        month = myCalendarDOB.get(Calendar.MONTH);
        day = myCalendarDOB.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker = new DatePickerDialog(myActivity, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                myCalendarDOB.set(Calendar.YEAR, selectedyear);
                myCalendarDOB.set(Calendar.MONTH, selectedmonth);
                myCalendarDOB.set(Calendar.DAY_OF_MONTH, selectedday);

                SimpleDateFormat fmtDate = new SimpleDateFormat("dd-MM-yyyy");
                Calendar myCalendarTemp = Calendar.getInstance();
                Calendar tempmyCalendarDOB = myCalendarTemp;
                tempmyCalendarDOB.set(Calendar.YEAR, myCalendarTemp.get(Calendar.DAY_OF_MONTH)-1);

                if (myCalendarDOB.before(tempmyCalendarDOB) || myCalendarDOB.equals(tempmyCalendarDOB))
                {
                   // return fmtDate.format(myCalendarDOB.getTime().getTime()).toString();

                } else {
                    myCalendarDOB.clear();
                    Utility.ShowToast(myActivity, "Event date can be less current date.");
                }
            }
        }, year, month, day);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
        return null;
    }
}
