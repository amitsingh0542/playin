package playin.orgname.com.playin.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;

import playin.orgname.com.playin.R;


public class BaseListAdapter extends BaseAdapter{
	protected Activity mActivity;

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView != null && mActivity != null) 
			if ((position % 2) == 0)
			{
				convertView.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.trans_left_in));
			} else {
				convertView.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.trans_right_in));
			}
		return convertView;
	}

	@Override
	public int getCount() {
		return 0;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
}
