package playin.orgname.com.playin.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Hashtable;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.EventForMeModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.model.UserModel;
import playin.orgname.com.playin.preference.PlayInSharedPrefKeys;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;

public class HomeActivity extends ActionBarActivity
        implements NavigationDrawerCallbacks, DisplableInterface {
//    private PreferenceManager pm;
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar mToolbar;
    private Context mContext;
    public static Activity mActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        mContext = getApplicationContext();
        // pm = new PreferenceManager(getApplicationContext());

        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        // setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.fragment_drawer);

        getUserDetailsById();
        // Set up the drawer.
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        // populate the navigation drawer
        //mNavigationDrawerFragment.setUserData("Username", "test@gmail.com", BitmapFactory.decodeResource(getResources(), R.drawable.avatar));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Fragment fragment;
        switch (position) {
            case 0: //search//todo
                fragment = getFragmentManager().findFragmentByTag(DiscoverFragment.TAG);
                if (fragment == null) {
                    fragment = new DiscoverFragment();
                }
                getFragmentManager().beginTransaction().replace(R.id.container, fragment, DiscoverFragment.TAG).commit();
                break;
            case 1: //stats
               /* fragment = getFragmentManager().findFragmentByTag(ListPicsFargment.TAG);
                if (fragment == null) {
                    fragment = new com.urbanhomez.fragments.ListPicsFargment();
                }
                getFragmentManager().beginTransaction().replace(R.id.container, fragment, ListPicsFargment.TAG).commit();*/
                break;
            case 2: //my account //todo
                break;
            case 3: //settings //todo
                break;
            case 4: //settings //todo
                break;
            case 5: //settings //todo
                break;
            case 6: //settings //todo
                break;
            case 7: //settings //todo
                //pm.clearApplication();
                finish();
                Intent in = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(in);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else {
            //super.onBackPressed();
            showLogoutDialog();
        }
    }


    private void showLogoutDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);
        alertDialog.setTitle("Logout");
        alertDialog.setMessage("Are you sure you want to logout?");
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    private void getUserDetailsById() {
        MainController controller = new MainController(HomeActivity.this, this, Constants.GET_USER_BY_ID, true);
        String finalUrl = Constants.BASE_URL + Constants.GET_USER_DETAILS_BY_ID + PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_PLAYIN_ID);
        controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN_TOKEN);
    }

    @Override
    public void setScreenData(Hashtable obj, byte type, String Responce) {
        switch (type) {
            case Constants.GET_USER_BY_ID:
                Hashtable<String, Object> result = (Hashtable<String, Object>) obj;
                ArrayList<UserModel> list = (ArrayList<UserModel>) obj.get("data");
                if (list != null && list.size() > 0) {
                    UserModel model = list.get(0);
                    Utility.ShowToast(mContext, model.getFirstName());
                    mNavigationDrawerFragment.setUserData(model.getFirstName(), model.getEmail(), BitmapFactory.decodeResource(getResources(), R.drawable.avatar));
                }

                ResponseInfo responseInfo = (ResponseInfo) obj.get("responseInfo");
                if (responseInfo != null && responseInfo.getMessage() != null)
                    Utility.ShowToast(mContext, responseInfo.getMessage());

                break;
        }
    }

    @Override
    public void setScreenData(String obj) {

    }

    @Override
    public void setScreenMessage(String obj, byte type) {

    }

    @Override
    public void setCancelMessage(String obj, byte type) {

    }
}
