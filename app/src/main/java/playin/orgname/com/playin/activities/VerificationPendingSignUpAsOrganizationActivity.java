package playin.orgname.com.playin.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.base.BaseActivity;

public class VerificationPendingSignUpAsOrganizationActivity extends BaseActivity implements OnClickListener
{
	public static final String TAG = "VerificationPendingSignUpAsOrganizationActivity";

	private Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		//getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_verification_pending_signup_as_organization);
		mContext=getApplicationContext();
		initViews();
		initListener();
	}

	private void initViews()
	{
	}

	private void initListener()
	{
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{

		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		System.exit(0);
	}
}
