package playin.orgname.com.playin.exceptionhandler;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread.UncaughtExceptionHandler;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;


import playin.orgname.com.playin.entry.SplashActivity;
import playin.orgname.com.playin.preference.PlayInSharedPrefKeys;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;

public class ExceptionHandler implements
		java.lang.Thread.UncaughtExceptionHandler {
	private final Context myContext;
	private final String LINE_SEPARATOR = "\n";

	public ExceptionHandler(Context context) {
		myContext = context;
	}

	@Override
	public void uncaughtException(Thread thread, Throwable exception) {
		exception.printStackTrace();
		StringWriter stackTrace = new StringWriter();
		exception.printStackTrace(new PrintWriter(stackTrace));
		StringBuilder errorReport = new StringBuilder();
		errorReport.append("************ CAUSE OF ERROR ************\n\n");
		errorReport.append(stackTrace.toString());

		errorReport.append("\n************ DEVICE INFORMATION ***********\n");
		errorReport.append("Brand: ");
		errorReport.append(Build.BRAND);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Device: ");
		errorReport.append(Build.DEVICE);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Model: ");
		errorReport.append(Build.MODEL);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Id: ");
		errorReport.append(Build.ID);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Product: ");
		errorReport.append(Build.PRODUCT);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("\n************ FIRMWARE ************\n");
		errorReport.append("SDK: ");
		errorReport.append(Build.VERSION.SDK_INT);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Release: ");
		errorReport.append(Build.VERSION.RELEASE);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Incremental: ");
		errorReport.append(Build.VERSION.INCREMENTAL);
		errorReport.append(LINE_SEPARATOR);

		PlayInSharedPrefUtils mPlayInSharedPrefUtils = PlayInSharedPrefUtils
				.getInstance(myContext);
		int splashLaunchCount = mPlayInSharedPrefUtils.fetchIntegerPrefernce(
				PlayInSharedPrefKeys.PREF_KEY_SPLASH_LAUNCH_COUNT, 0);
		Log.e("ExceptionHandler", "Count=" + splashLaunchCount);
		if (splashLaunchCount < 2) {
			PlayInSharedPrefUtils.getInstance(myContext).saveIntegerPrefernce(PlayInSharedPrefKeys.PREF_KEY_SPLASH_LAUNCH_COUNT,++splashLaunchCount);
			Intent intent = new Intent(myContext, SplashActivity.class);
			intent.putExtra("error", errorReport.toString());
			myContext.startActivity(intent);
			Log.e("Exception Handle", errorReport.toString());
		} else {
			splashLaunchCount = 0;
			mPlayInSharedPrefUtils.saveIntegerPrefernce(
					PlayInSharedPrefKeys.PREF_KEY_SPLASH_LAUNCH_COUNT,
					splashLaunchCount);
//			if (mUncaughtExceptionHandler != null) {
//				mUncaughtExceptionHandler.uncaughtException(thread, exception);
//			} else {
//			}

		}
		android.os.Process.killProcess(android.os.Process.myPid());
		System.exit(0);

		// Toast.makeText(myContext, "Count="+splashLaunchCount,
		// Toast.LENGTH_LONG).show();

	}

}
