package playin.orgname.com.playin.connection;

import android.app.Activity;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import playin.orgname.com.playin.bootstrap.PlayInApplicationBootstrap;
import playin.orgname.com.playin.components.ShowLoader;
import playin.orgname.com.playin.interfaces.ConnManagerInterface;
import playin.orgname.com.playin.model.DataModel;

/**
 * Created by Unikove pc on 6/30/2015.
 */
public class VolleyConnectionForPost {
    private ConnManagerInterface iConnInterface;
    private String tag_string_req = "string_req1";
    private ShowLoader loader;
    String URL = null;
    private Activity mActivity;
    public VolleyConnectionForPost (Activity activity, ConnManagerInterface connInterface , String URL)
    {
        this.iConnInterface = connInterface;
        mActivity = activity;
//        loader = AwfisLoader.getInstance(mActivity);
        loader = ShowLoader.getInstance(activity);
        this.URL = URL;
       // this.URL = "http://unikove.com/projects/AwfisNew/webservice/service_getSpaceDetail.php?SpaceId=7";
    }

    public void makeRequest(final ArrayList<DataModel> list, String url, boolean isLoaderVisible) {
        showProgressDialog();


        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        /*Log.d(TAG, response.toString());
						msgResponse.setText(response.toString());*/
                        iConnInterface.successResponseProcess(response);

                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.d(TAG, "Error: " + error.getMessage());
                if(error instanceof NoConnectionError) {
                    String strerror = "No internet Access, Check your internet connection.";
                    iConnInterface.failedResponseProcess(strerror);
                    System.out.println(strerror);
                }
                else  if(error instanceof TimeoutError) {
                    String strerror = "Connection timed out.";
                    iConnInterface.failedResponseProcess(strerror);
                    System.out.println(strerror);
                }
                else
                {
                    String strerror = "Please try again.";
                    iConnInterface.failedResponseProcess(strerror);
                }
                hideProgressDialog();
            }
        })

        {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type"," application/json");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
//                Map<String, String> params = temp;
                JSONObject obj = new JSONObject();
                Map<String,String> params = new HashMap<String,String>();
                for (int i=0 ; i < list.size();i++)
                {
                    params.put(list.get(i).getKey(), list.get(i).getValue());

                    try {
                        obj.put(list.get(i).getKey(),list.get(i).getValue());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    System.out.println(list.get(i).getKey()+ " " +list.get(i).getValue());
                }

                System.out.println("Request==> "+obj.toString());

                System.out.println(params.size()+ " params.size()  VolleyConnectionForPost");
                return params;
            }

        };

        // Adding request to request queue
//        AppController.getInstance().addToRequestQueue(jsonObjReq,tag_json_obj);
        sr.setShouldCache(false);
        PlayInApplicationBootstrap.getInstance().addToRequestQueue(sr, tag_string_req);
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
    }

    private void showProgressDialog() {
        loader.run(false);
    }

    private void hideProgressDialog() {
        loader.dismis(false);
    }
}
