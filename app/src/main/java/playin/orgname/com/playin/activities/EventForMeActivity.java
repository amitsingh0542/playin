package playin.orgname.com.playin.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.adapters.EventForMeAdapter;
import playin.orgname.com.playin.base.BaseActivity;
import playin.orgname.com.playin.model.EventForMeModel;

public class EventForMeActivity extends BaseActivity implements OnClickListener
{
	public static final String TAG = "EventForMeActivity";

	private Context mContext;
	private ListView lvEventForMe;
	ArrayList<EventForMeModel> listEventForMe = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		//getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_event_for_me);
		mContext=getApplicationContext();
		initViews();
		initData();
		initListener();
	}

	private void initViews()
	{
		Intent intentEventForMe=getIntent();
		listEventForMe=(ArrayList<EventForMeModel> )intentEventForMe.getSerializableExtra("EVENT_FOR_ME");
		lvEventForMe=(ListView)findViewById(R.id.lv_event_for_me);
	}

	private void initListener()
	{
		lvEventForMe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,	long id)
			{
				Intent intent = new Intent(mContext, EventDetailActivity.class);
				/*String message = "abc";
				intent.putExtra(EXTRA_MESSAGE, message);*/
				startActivity(intent);
			}
		});
	}
	private void initData()
	{
		/*List<String> listMyEvent=new ArrayList<>();
		listMyEvent.add("Cricket Tournament");
		listMyEvent.add("Chess");
		listMyEvent.add("Carrom");
		listMyEvent.add("Table Tennis");
		listMyEvent.add("Yoga");
		listMyEvent.add("Badmintion");
		listMyEvent.add("Hockey");*/

		EventForMeAdapter myEventAdapter=new EventForMeAdapter(mContext,listEventForMe);
		lvEventForMe.setAdapter(myEventAdapter);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{

		}
	}



}
