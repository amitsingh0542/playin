package playin.orgname.com.playin.model;

import java.io.Serializable;

/**
 * Created by Ashutosh on 6/25/2016.
 */
public class LoginModel implements Serializable
{
    private String playinId = null;
    private String tempExpiryTime = null;
    private String token=null;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPlayinId() {
        return playinId;
    }

    public void setPlayinId(String playinId) {
        this.playinId = playinId;
    }

    public String getTempExpiryTime() {
        return tempExpiryTime;
    }

    public void setTempExpiryTime(String tempExpiryTime) {
        this.tempExpiryTime = tempExpiryTime;
    }

}
