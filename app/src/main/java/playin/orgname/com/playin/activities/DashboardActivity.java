package playin.orgname.com.playin.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Hashtable;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.base.BaseActivity;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.EventForMeModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.model.UserModel;
import playin.orgname.com.playin.preference.PlayInSharedPrefKeys;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;

public class DashboardActivity extends BaseActivity implements OnClickListener, DisplableInterface {
    public static final String TAG = "DashboardActivity";
    private Button btnProfile, btnDiscover, btnMyEvent, btnUpcomingEvent, btn_joined_event_dashboard,btn_myteam_dashboard;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_dashboard);
        mContext = getApplicationContext();
        initViews();
        initListener();
    }

    private void initViews() {
        btnProfile = (Button) findViewById(R.id.btnProfile);
        btnDiscover = (Button) findViewById(R.id.btnDiscover);
        btnMyEvent = (Button) findViewById(R.id.btnMyEvent);
        btnUpcomingEvent = (Button) findViewById(R.id.btn_upcoming_event_dashboard);
        btn_joined_event_dashboard = (Button) findViewById(R.id.btn_joined_event_dashboard);
        btn_myteam_dashboard = (Button) findViewById(R.id.btn_myteam_dashboard);


    }

    private void initListener() {
        btnProfile.setOnClickListener(this);
        btnDiscover.setOnClickListener(this);
        btnMyEvent.setOnClickListener(this);
        btnUpcomingEvent.setOnClickListener(this);
        btn_joined_event_dashboard.setOnClickListener(this);
        btn_myteam_dashboard.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnProfile:
                getUserDetailsById();
                /*Intent intentProfile = new Intent(mContext, ProfileActivity.class);
				startActivity(intentProfile);*/
                break;
            case R.id.btnDiscover:
                getEventForMe();
				/*Intent intentDiscover = new Intent(mContext, DiscoverFragment.class);
				startActivity(intentDiscover);*/
                break;

            case R.id.btnMyEvent:
                Intent intentMyEvent = new Intent(mContext, MyEventActivity.class);
                startActivity(intentMyEvent);
                break;

            case R.id.btn_upcoming_event_dashboard:
                moveToUpcomingEvent();
                break;

            case R.id.btn_joined_event_dashboard:
                Intent intentUpComingEventActivity = new Intent(mContext, JoinedEventActivity.class);
                startActivity(intentUpComingEventActivity);
                break;

            case R.id.btn_myteam_dashboard:
                Intent intentMyTeams = new Intent(mContext, MyTeamsActivity.class);
                startActivity(intentMyTeams);
                break;



            default:
                break;
        }
    }

    private void moveToUpcomingEvent() {
        Intent intentUpComingEventActivity = new Intent(mContext, UpComingEventActivity.class);
        startActivity(intentUpComingEventActivity);
    }


    private void getUserDetailsById() {
        MainController controller = new MainController(DashboardActivity.this, this, Constants.GET_USER_BY_ID, true);
        String finalUrl = Constants.BASE_URL + Constants.GET_USER_DETAILS_BY_ID + PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_PLAYIN_ID);
        controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN);
    }

    private void getEventForMe() {
        MainController controller = new MainController(DashboardActivity.this, this, Constants.EVENT_FOR_ME, true);
        String finalUrl = Constants.BASE_URL + Constants.EVENT_FOR_ME_API;
        controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN);
    }

    @Override
    public void setScreenData(Hashtable obj, byte type, String Responce) {
        switch (type) {
            case Constants.GET_USER_BY_ID:
                Hashtable<String, Object> result = (Hashtable<String, Object>) obj;
                ArrayList<UserModel> list = (ArrayList<UserModel>) obj.get("data");
                if (list != null && list.size() > 0) {
                    UserModel model = list.get(0);
                    Utility.ShowToast(mContext, model.getFirstName());
                }

                ResponseInfo responseInfo = (ResponseInfo) obj.get("responseInfo");
                if (responseInfo != null && responseInfo.getMessage() != null)
                    Utility.ShowToast(mContext, responseInfo.getMessage());

                break;

            case Constants.EVENT_FOR_ME:
                Hashtable<String, Object> hashtableEventForMe = (Hashtable<String, Object>) obj;
                ArrayList<EventForMeModel> listEventForMe = (ArrayList<EventForMeModel>) obj.get("data");
                if (listEventForMe != null && listEventForMe.size() > 0) {
                    EventForMeModel model = listEventForMe.get(0);
                    //Utility.ShowToast(mContext,model.getTitle());

                    Intent intentEventForMe = new Intent(mContext, EventForMeActivity.class);
                    intentEventForMe.putExtra("EVENT_FOR_ME", listEventForMe);
                    startActivity(intentEventForMe);
                    //finish();
                }

                ResponseInfo responseInfoEventForMe = (ResponseInfo) obj.get("responseInfo");
                if (responseInfoEventForMe != null && responseInfoEventForMe.getMessage() != null)
                    Utility.ShowToast(mContext, responseInfoEventForMe.getMessage());

                break;
        }
    }

    @Override
    public void setScreenData(String obj) {

    }

    @Override
    public void setScreenMessage(String obj, byte type) {

    }

    @Override
    public void setCancelMessage(String obj, byte type) {

    }
}
