package playin.orgname.com.playin.connection;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import playin.orgname.com.playin.components.PrefrenceManager;
import playin.orgname.com.playin.components.ShowLoader;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.interfaces.ConnManagerInterface;
import playin.orgname.com.playin.model.DataModel;
import playin.orgname.com.playin.preference.PlayInSharedPrefKeys;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;


public class GetHttpConnector extends AsyncTask<Request, Void, Void> {

    // Required initialization

    private final HttpClient Client = new DefaultHttpClient();
    private String iContent = "";
    private String iXmlData = "";
    private String Error = null;
    //private ProgressDialog Dialog;
    private int CONNECTION_TIME_OUT = 10000 * 2;
    String data = "";
    int sizeData = 0;
    private Activity activity = null;
    private ConnManagerInterface iConInterface;
    private boolean isShowLoader = false;
    private ShowLoader dialog;
    private PrefrenceManager session = null;

    public static boolean isConnectivity = false;
    ArrayList<DataModel> list;
    String url = null;
    private String response = null;

    public GetHttpConnector(Activity mActivity, ConnManagerInterface mConInterface, boolean isLoading) {
        this.iConInterface = mConInterface;
        //	Dialog = new ProgressDialog(mActivity);
        dialog = ShowLoader.getInstance(mActivity);
        this.isShowLoader = isLoading;
        activity = mActivity;
        isConnectivity = false;
        this.list = list;
        this.url = url;
        session = new PrefrenceManager(mActivity.getApplicationContext());
    }


    protected void onPreExecute() {
        //Start Progress Dialog (Message)
        if (isShowLoader) {
            dialog.run(false);
            /*Dialog.setMessage("Please wait..");
			Dialog.show();	*/
        }
    }

    protected Void doInBackground(Request... urls) {
        try {
            //sendData(urls[0]);
            //iContent = makeRequest(urls[0]);
            iContent = getWebServiceData(urls[0]);
            //iContent = postData(urls[0]);
            //iContent = sendRequest(urls[0]);


            Log.d("Response=> ", iContent);
            //iConnInterface.successResponseProcess(response);

        } catch (Exception e) {
            // TODO: handle exception
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    //	Constants.showToastMessage(activity.getApplicationContext(), "Server error.");

                }
            });
        }
        return null;
    }

    protected void onPostExecute(Void unused) {
        // NOTE: You can call UI Element here.
        if (isShowLoader)
            dialog.dismis(false);
        //Dialog.dismiss();
        if (!isConnectivity) {
            if (Error != null) {
                iConInterface.excuteError(Error);
            } else {
                if (iContent.contains("Customer is not logged in.")) {
                    iConInterface.failedResponseProcess(iContent);
                } else if (!iContent.contains("SessionExpired")) {
                    iConInterface.successResponseProcess(iContent);
                } else {
                    if (iContent.contains("SessionExpired"))
                        iConInterface.failedResponseProcess(iContent);
                }
            }
        } else {
            //	iConInterface.successResponseProcess(Constants.MSG_CANCLE_REQUEST, null);
        }
    }

    public String makeRequest(Request request) {
        HttpURLConnection urlConnection;
        String url = request.getUrl();
        String data = request.getRequest();
        String result = null;
        try {
            //Connect
            urlConnection = (HttpURLConnection) ((new URL(url).openConnection()));
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            //	urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.connect();

            //Write
            OutputStream outputStream = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            writer.write(data);
            writer.close();
            outputStream.close();

            //Read
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));

            String line = null;
            StringBuilder sb = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            bufferedReader.close();
            result = sb.toString();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }


    public String getWebServiceData(Request request) {
        InputStream is = null;
        String result = "";
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity;
            HttpParams params = httpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 30000);
            HttpResponse httpResponse;
            switch (request.getRequestType()) {
                case 0:
                    HttpGet httpGet = new HttpGet(request.getUrl());
                    if (request.getHeaderType().equalsIgnoreCase(Constants.HEADER_TYPE_PLAY_IN_TOKEN)) {
                        httpGet.setHeader("playinId", PlayInSharedPrefUtils.getInstance(activity).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_PLAYIN_ID));
                        httpGet.setHeader("token", PlayInSharedPrefUtils.getInstance(activity).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_TOKEN));
                    } else if (request.getHeaderType().equalsIgnoreCase(Constants.HEADER_TYPE_PLAY_IN)) {
                        httpGet.setHeader("playinId", PlayInSharedPrefUtils.getInstance(activity).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_PLAYIN_ID));
                    } else if (request.getHeaderType().equalsIgnoreCase(Constants.HEADER_TYPE_TOKEN)) {
                        httpGet.setHeader("token", PlayInSharedPrefUtils.getInstance(activity).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_TOKEN));
                    }
                    httpResponse = httpClient.execute(httpGet);
                    httpEntity = httpResponse.getEntity();
                    is = httpEntity.getContent();
                    break;
                case 1:
                    HttpPost httpPost = new HttpPost(request.getUrl());
                    //String _params = null;
                    StringEntity entity = new StringEntity(request.getRequest());
                    httpPost.setEntity(entity);
                    httpPost.setHeader("Content-Type", "application/json");
                    httpPost.setHeader("Accept", "application/json");

                    if (request.getHeaderType().equalsIgnoreCase(Constants.HEADER_TYPE_PLAY_IN_TOKEN)) {
                        httpPost.setHeader("playinId", PlayInSharedPrefUtils.getInstance(activity).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_PLAYIN_ID));
                        httpPost.setHeader("token", PlayInSharedPrefUtils.getInstance(activity).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_TOKEN));
                    } else if (request.getHeaderType().equalsIgnoreCase(Constants.HEADER_TYPE_PLAY_IN)) {
                        httpPost.setHeader("playinId", PlayInSharedPrefUtils.getInstance(activity).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_PLAYIN_ID));
                    } else if (request.getHeaderType().equalsIgnoreCase(Constants.HEADER_TYPE_TOKEN)) {
                        httpPost.setHeader("token", PlayInSharedPrefUtils.getInstance(activity).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_TOKEN));
                    }
                    httpResponse = httpClient.execute(httpPost);
                    httpEntity = httpResponse.getEntity();
                    is = httpEntity.getContent();
                    //String data = makeRequest(url, _params.toString());
                    //Constants.printOnConsole("Res ", data);
                    break;

                case 2:
                    //Need to modify this PUT Request
                /*    URL url = new URL(request.getUrl());
                    HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
                    httpCon.setDoOutput(true);
                    httpCon.setRequestMethod("PUT");
                    if (request.getHeaderType().equalsIgnoreCase(Constants.HEADER_TYPE_PLAY_IN_TOKEN)) {
                        httpGet.setHeader("playinId", PlayInSharedPrefUtils.getInstance(activity).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_PLAYIN_ID));
                        httpGet.setHeader("token", PlayInSharedPrefUtils.getInstance(activity).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_TOKEN));
                    } else if (request.getHeaderType().equalsIgnoreCase(Constants.HEADER_TYPE_PLAY_IN)) {
                        httpGet.setHeader("playinId", PlayInSharedPrefUtils.getInstance(activity).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_PLAYIN_ID));
                    } else if (request.getHeaderType().equalsIgnoreCase(Constants.HEADER_TYPE_TOKEN)) {
                        httpGet.setHeader("token", PlayInSharedPrefUtils.getInstance(activity).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_TOKEN));
                    }
                    OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
                    out.write("Data you want to put");
                    out.close();
                    break;*/
            }

        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
        } catch (ClientProtocolException e) {

            e.printStackTrace();
        } catch (ConnectTimeoutException e) {
            //iContent = Constants.CONNECTION_TIMED_OUT;

            Log.e("CONN TIMEOUT", e.toString());

        } catch (SocketTimeoutException e) {


            Log.e("SOCK TIMEOUT", e.toString());
            //	iContent = Constants.SOCKET_TIMED_OUT;
        } catch (IOException e) {

            e.printStackTrace();
        } catch (NullPointerException e) {

            e.printStackTrace();
        } catch (Exception e) {
            Log.e("OTHER EXCEPTIONS", e.toString());
            //iContent = Constants.SOME_THING_WENT_WRONG;
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"), 8);//is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            if (is != null)
                is.close();
            iContent = sb.toString();
            Log.e("Response => ", iContent);
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());

        }


        return iContent;
    }


    public String postData(Request request) {
        // Create a new HttpClient and Post Header
        String responsedata = null;
        HttpParams myParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(myParams, 10000);
        HttpConnectionParams.setSoTimeout(myParams, 10000);
        HttpClient httpclient = new DefaultHttpClient(myParams);

        try {

            HttpPost httppost = new HttpPost(request.getUrl());
            httppost.setHeader("Content-type", "application/json");

            StringEntity se = new StringEntity(request.getRequest());
            se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            httppost.setEntity(se);

            HttpResponse response = httpclient.execute(httppost);
            String temp = EntityUtils.toString(response.getEntity());
            responsedata = temp;
            Log.i("tag", temp);


        } catch (ClientProtocolException e) {

        } catch (IOException e) {
        }
        return responsedata;
    }

    public static String sendRequest(Request request) {
        String downloadedString = null;

        HttpClient httpclient = new DefaultHttpClient();


        //for registerhttps://te
        HttpPost httppost = new HttpPost(request.getUrl());
        //add data
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("email", "test@testing.com"));
            nameValuePairs.add(new BasicNameValuePair("password", "111111"));
            nameValuePairs.add(new BasicNameValuePair("deviceIdentifier", "Android"));

            //add data
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);

            InputStream in = response.getEntity().getContent();
            StringBuilder stringbuilder = new StringBuilder();
            BufferedReader bfrd = new BufferedReader(new InputStreamReader(in), 1024);
            String line;
            while ((line = bfrd.readLine()) != null)
                stringbuilder.append(line);

            downloadedString = stringbuilder.toString();

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("downloadedString:in login:::" + downloadedString);


        return downloadedString;
    }

}
