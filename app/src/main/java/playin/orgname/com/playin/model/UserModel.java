package playin.orgname.com.playin.model;

import java.io.Serializable;

/**
 * Created by Ashutosh on 6/25/2016.
 */
public class UserModel implements Serializable
{
    private String playinId = null;
    private String firstName = null;
    private String lastName = null;
    private String mobile = null;
    private String email = null;
    private String password = null;
    private String timestampOfBirth = null;
    private String gender = null;

    private String location = null;
    private boolean isMobileVerified = false;
    private boolean isEmailVerified = false;
    private String responseInfo = null;
    private String oauth = null;
    private String fboauth = null;
    private String googleUserId = null;
    private String facebookUserId = null;
    private String tempPassword= null;
    private String tempExpiryTime= null;
    private String isTempPassword= null;
    private String imagePath= null;
    private boolean isSuperUser= false;
    private int playinStatus= 1;
    private String featureAccessLevel= null;
    private String title= null;
    private String message= null;
    private int responseCode= 0;

    public String getPlayinId() {
        return playinId;
    }

    public void setPlayinId(String playinId) {
        this.playinId = playinId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTimestampOfBirth() {
        return timestampOfBirth;
    }

    public void setTimestampOfBirth(String timestampOfBirth) {
        this.timestampOfBirth = timestampOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isMobileVerified() {
        return isMobileVerified;
    }

    public void setIsMobileVerified(boolean isMobileVerified) {
        this.isMobileVerified = isMobileVerified;
    }

    public boolean isEmailVerified() {
        return isEmailVerified;
    }

    public void setIsEmailVerified(boolean isEmailVerified) {
        this.isEmailVerified = isEmailVerified;
    }

    public String getResponseInfo() {
        return responseInfo;
    }

    public void setResponseInfo(String responseInfo) {
        this.responseInfo = responseInfo;
    }

    public String getOauth() {
        return oauth;
    }

    public void setOauth(String oauth) {
        this.oauth = oauth;
    }

    public String getFboauth() {
        return fboauth;
    }

    public void setFboauth(String fboauth) {
        this.fboauth = fboauth;
    }

    public String getGoogleUserId() {
        return googleUserId;
    }

    public void setGoogleUserId(String googleUserId) {
        this.googleUserId = googleUserId;
    }

    public String getFacebookUserId() {
        return facebookUserId;
    }

    public void setFacebookUserId(String facebookUserId) {
        this.facebookUserId = facebookUserId;
    }

    public String getTempPassword() {
        return tempPassword;
    }

    public void setTempPassword(String tempPassword) {
        this.tempPassword = tempPassword;
    }

    public String getTempExpiryTime() {
        return tempExpiryTime;
    }

    public void setTempExpiryTime(String tempExpiryTime) {
        this.tempExpiryTime = tempExpiryTime;
    }

    public String getIsTempPassword() {
        return isTempPassword;
    }

    public void setIsTempPassword(String isTempPassword) {
        this.isTempPassword = isTempPassword;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public boolean isSuperUser() {
        return isSuperUser;
    }

    public void setIsSuperUser(boolean isSuperUser) {
        this.isSuperUser = isSuperUser;
    }

    public int getPlayinStatus() {
        return playinStatus;
    }

    public void setPlayinStatus(int playinStatus) {
        this.playinStatus = playinStatus;
    }

    public String getFeatureAccessLevel() {
        return featureAccessLevel;
    }

    public void setFeatureAccessLevel(String featureAccessLevel) {
        this.featureAccessLevel = featureAccessLevel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }



}
