package playin.orgname.com.playin.model;

import java.io.Serializable;

/**
 * Created by Ashutosh on 7/3/2016.
 */
public class TeamDetailsModel implements Serializable {


    private int teamId = 0;
    private String name = null;
    private int numberOfPlayersRequired = 0;
    private int standbyPlayers = 0;
    private int numberOfPlayersJoined = 1;
    private int preferredPlayers = 0;
    private String createdByPlayinId = null;
    private String createdByentityType = null;
    private int sportId = 1;
    private EventDetailsLocationModel preferredLocation = null;
    private boolean isCreatorATeamMember = true;
    private boolean needsCreatorPermissionToJoin = true;
    private String imagePath = null;
    private int entityType = 0;
    private String searchKeyword = null;
    private int joinStatus = 0;

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfPlayersRequired() {
        return numberOfPlayersRequired;
    }

    public void setNumberOfPlayersRequired(int numberOfPlayersRequired) {
        this.numberOfPlayersRequired = numberOfPlayersRequired;
    }

    public int getStandbyPlayers() {
        return standbyPlayers;
    }

    public void setStandbyPlayers(int standbyPlayers) {
        this.standbyPlayers = standbyPlayers;
    }

    public int getNumberOfPlayersJoined() {
        return numberOfPlayersJoined;
    }

    public void setNumberOfPlayersJoined(int numberOfPlayersJoined) {
        this.numberOfPlayersJoined = numberOfPlayersJoined;
    }

    public int getPreferredPlayers() {
        return preferredPlayers;
    }

    public void setPreferredPlayers(int preferredPlayers) {
        this.preferredPlayers = preferredPlayers;
    }

    public String getCreatedByPlayinId() {
        return createdByPlayinId;
    }

    public void setCreatedByPlayinId(String createdByPlayinId) {
        this.createdByPlayinId = createdByPlayinId;
    }

    public String getCreatedByentityType() {
        return createdByentityType;
    }

    public void setCreatedByentityType(String createdByentityType) {
        this.createdByentityType = createdByentityType;
    }

    public int getSportId() {
        return sportId;
    }

    public void setSportId(int sportId) {
        this.sportId = sportId;
    }

    public EventDetailsLocationModel getPreferredLocation() {
        return preferredLocation;
    }

    public void setPreferredLocation(EventDetailsLocationModel preferredLocation) {
        this.preferredLocation = preferredLocation;
    }

    public boolean isCreatorATeamMember() {
        return isCreatorATeamMember;
    }

    public void setIsCreatorATeamMember(boolean isCreatorATeamMember) {
        this.isCreatorATeamMember = isCreatorATeamMember;
    }

    public boolean isNeedsCreatorPermissionToJoin() {
        return needsCreatorPermissionToJoin;
    }

    public void setNeedsCreatorPermissionToJoin(boolean needsCreatorPermissionToJoin) {
        this.needsCreatorPermissionToJoin = needsCreatorPermissionToJoin;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getEntityType() {
        return entityType;
    }

    public void setEntityType(int entityType) {
        this.entityType = entityType;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public int getJoinStatus() {
        return joinStatus;
    }

    public void setJoinStatus(int joinStatus) {
        this.joinStatus = joinStatus;
    }


}
