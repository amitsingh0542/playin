package playin.orgname.com.playin.base;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Hashtable;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.activities.DiscoverFragment;
import playin.orgname.com.playin.activities.EventForMeActivity;
import playin.orgname.com.playin.activities.JoinedEventActivity;
import playin.orgname.com.playin.activities.MyEventActivity;
import playin.orgname.com.playin.activities.MyTeamsActivity;
import playin.orgname.com.playin.activities.NavigationDrawerCallbacks;
import playin.orgname.com.playin.activities.NavigationDrawerFragment;
import playin.orgname.com.playin.activities.ProfileDetailActivity;
import playin.orgname.com.playin.activities.UpComingEventActivity;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.EventForMeModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.model.UserModel;
import playin.orgname.com.playin.preference.PlayInSharedPrefKeys;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;


public class BaseActivity extends Activity
		implements NavigationDrawerCallbacks, DisplableInterface,View.OnClickListener {

	private static int mHeight;
	private static int mWidth;
	private NavigationDrawerFragment mNavigationDrawerFragment;
	private Toolbar mToolbar;

	public static final String TAG = "DashboardActivity";
	private Button btnProfile, btnDiscover, btnMyEvent, btnUpcomingEvent, btn_joined_event_dashboard,btn_myteam_dashboard;
	private Context mContext;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		if (mHeight == 0) {
			setDeviceSize();
		}

		mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
		// setSupportActionBar(mToolbar);
//		getSupportActionBar().setDisplayShowHomeEnabled(true);

		mNavigationDrawerFragment = (NavigationDrawerFragment)
				getFragmentManager().findFragmentById(R.id.fragment_drawer);

		// Set up the drawer.
		mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
		// populate the navigation drawer
		//mNavigationDrawerFragment.setUserData("Username", "test@gmail.com", BitmapFactory.decodeResource(getResources(), R.drawable.avatar));
		mContext = getApplicationContext();
		initViews();
		initListener();

	}

	private void setDeviceSize() {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		mHeight = displaymetrics.heightPixels;
		mWidth = displaymetrics.widthPixels;
	}

	public int getHeight() {
		return mHeight;
	}

	public void setHeight(int height) {
		mHeight = height;
	}

	public int getWidth() {
		return mWidth;
	}

	public void setWidth(int width) {
		mWidth = width;
	}



	@Override
	public void onNavigationDrawerItemSelected(int position) {
		Fragment fragment;
		switch (position) {
			case 0: //search//todo
				/*fragment = getFragmentManager().findFragmentByTag(DiscoverFragment.TAG);
				if (fragment == null) {
					fragment = new DiscoverFragment();
					updateView(fragment);
				}*/
			//	getFragmentManager().beginTransaction().replace(R.id.container, fragment, DiscoverFragment.TAG).commit();
				break;
			case 1: //stats
				/*fragment = getFragmentManager().findFragmentByTag(ProfileDetailActivity.TAG);
				if (fragment == null) {
					fragment = new DiscoverFragment();
					updateView(fragment);
				}*/
			//	getFragmentManager().beginTransaction().replace(R.id.container, fragment, DiscoverFragment.TAG).commit();
				break;
			case 2: //my account //todo
				break;
			case 3: //settings //todo
				break;
			case 4: //settings //todo
				break;
			case 5: //settings //todo
				break;
			case 6: //settings //todo
				break;
			case 7: //settings //todo
				//pm.clearApplication();
				finish();
				/*Intent in = new Intent(HomeActivity.this, LoginActivity.class);
				startActivity(in);*/
				break;
		}
	}


	public void updateView(Fragment fragment){
		if (fragment != null) {
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.container, fragment).commit();


		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();


		return super.onOptionsItemSelected(item);
	}



	@Override
	public void setScreenData(String obj) {

	}

	@Override
	public void setScreenMessage(String obj, byte type) {

	}

	@Override
	public void setCancelMessage(String obj, byte type) {

	}

	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
								long id) {
			// display view for selected nav drawer item
			//displayView(position);
		}
	}


	private void initViews() {
		btnProfile = (Button) findViewById(R.id.btnProfile);
		btnDiscover = (Button) findViewById(R.id.btnDiscover);
		btnMyEvent = (Button) findViewById(R.id.btnMyEvent);
		btnUpcomingEvent = (Button) findViewById(R.id.btn_upcoming_event_dashboard);
		btn_joined_event_dashboard = (Button) findViewById(R.id.btn_joined_event_dashboard);
		btn_myteam_dashboard = (Button) findViewById(R.id.btn_myteam_dashboard);

	}

	private void initListener() {
		btnProfile.setOnClickListener(this);
		btnDiscover.setOnClickListener(this);
		btnMyEvent.setOnClickListener(this);
		btnUpcomingEvent.setOnClickListener(this);
		btn_joined_event_dashboard.setOnClickListener(this);
		btn_myteam_dashboard.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnProfile:
				getUserDetailsById();
                /*Intent intentProfile = new Intent(mContext, ProfileActivity.class);
				startActivity(intentProfile);*/
				break;
			case R.id.btnDiscover:
				getEventForMe();
				/*Intent intentDiscover = new Intent(mContext, DiscoverFragment.class);
				startActivity(intentDiscover);*/
				break;

			case R.id.btnMyEvent:
				Intent intentMyEvent = new Intent(mContext, MyEventActivity.class);
				startActivity(intentMyEvent);
				break;

			case R.id.btn_upcoming_event_dashboard:
				moveToUpcomingEvent();
				break;

			case R.id.btn_joined_event_dashboard:
				Intent intentUpComingEventActivity = new Intent(mContext, JoinedEventActivity.class);
				startActivity(intentUpComingEventActivity);
				break;

			case R.id.btn_myteam_dashboard:
				Intent intentMyTeams = new Intent(mContext, MyTeamsActivity.class);
				startActivity(intentMyTeams);
				break;


			default:
				break;
		}
	}

	private void moveToUpcomingEvent() {
		Intent intentUpComingEventActivity = new Intent(mContext, UpComingEventActivity.class);
		startActivity(intentUpComingEventActivity);
	}


	private void getUserDetailsById() {
		MainController controller = new MainController(BaseActivity.this, this, Constants.GET_USER_BY_ID, true);
		String finalUrl = Constants.BASE_URL + Constants.GET_USER_DETAILS_BY_ID + PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_PLAYIN_ID);
		controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN);
	}

	private void getEventForMe() {
		MainController controller = new MainController(BaseActivity.this, this, Constants.EVENT_FOR_ME, true);
		String finalUrl = Constants.BASE_URL + Constants.EVENT_FOR_ME_API;
		controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN);
	}

	@Override
	public void setScreenData(Hashtable obj, byte type, String Responce) {
		switch (type) {
			case Constants.GET_USER_BY_ID:
				Hashtable<String, Object> result = (Hashtable<String, Object>) obj;
				ArrayList<UserModel> list = (ArrayList<UserModel>) obj.get("data");
				if (list != null && list.size() > 0) {
					UserModel model = list.get(0);
					mNavigationDrawerFragment.setUserData(model.getFirstName(), model.getEmail(), BitmapFactory.decodeResource(getResources(), R.drawable.avatar));
					Utility.ShowToast(mContext, model.getFirstName());
				}

				ResponseInfo responseInfo = (ResponseInfo) obj.get("responseInfo");
				if (responseInfo != null && responseInfo.getMessage() != null)
					Utility.ShowToast(mContext, responseInfo.getMessage());

				break;

			case Constants.EVENT_FOR_ME:
				Hashtable<String, Object> hashtableEventForMe = (Hashtable<String, Object>) obj;
				ArrayList<EventForMeModel> listEventForMe = (ArrayList<EventForMeModel>) obj.get("data");
				if (listEventForMe != null && listEventForMe.size() > 0) {
					EventForMeModel model = listEventForMe.get(0);
					//Utility.ShowToast(mContext,model.getTitle());

					Intent intentEventForMe = new Intent(mContext, EventForMeActivity.class);
					intentEventForMe.putExtra("EVENT_FOR_ME", listEventForMe);
					startActivity(intentEventForMe);
					//finish();
				}

				ResponseInfo responseInfoEventForMe = (ResponseInfo) obj.get("responseInfo");
				if (responseInfoEventForMe != null && responseInfoEventForMe.getMessage() != null)
					Utility.ShowToast(mContext, responseInfoEventForMe.getMessage());

				break;
		}
	}

}
