package playin.orgname.com.playin.components;

import android.app.Activity;
import android.app.ProgressDialog;


public class ShowLoader {
	private static ShowLoader loadDialog = null;
	private static Component iComponent;
	private static Activity iActivity = null;
	private ShowLoader() {
		// TODO Auto-generated constructor stub
	}

	ProgressDialog dialog = null;

	public static ShowLoader getInstance(Activity mActivity){
		iActivity = mActivity;
		iComponent = new Component();
		loadDialog = new ShowLoader();
		return loadDialog;
	}

	public void run(boolean isLocked) {
		// TODO Auto-generated method stub
		if(isLocked){
			iComponent.lockScreenOrientation(iActivity);
		}
		CustomProgressDialog.showProgressDialog(iActivity,"", false);
	}
	public void dismis(boolean isLocked){
		if(isLocked){
			iComponent.unlockScreenOrientation(iActivity);
		}
		CustomProgressDialog.removeDialog();		
	}
	
	public boolean isShowing(){
		return CustomProgressDialog.isShowDialog();
	}
}
