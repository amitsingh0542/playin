package playin.orgname.com.playin.model;

import java.io.Serializable;

/**
 * Created by Ashutosh on 7/3/2016.
 */
public class JoinedEventModel implements Serializable
{
    private String eventId = null;
    private String creatorPlayinId = null;
    private String title = null;
    private String eventStartDate = null;
    private int sportId = 1;
    private String area = null;
    private String city = null;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getCreatorPlayinId() {
        return creatorPlayinId;
    }

    public void setCreatorPlayinId(String creatorPlayinId) {
        this.creatorPlayinId = creatorPlayinId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public int getSportId() {
        return sportId;
    }

    public void setSportId(int sportId) {
        this.sportId = sportId;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
