package playin.orgname.com.playin.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Hashtable;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.base.BaseActivity;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.EventDetailsModel;
import playin.orgname.com.playin.model.ResponseInfo;

public class EventDetailActivity extends BaseActivity implements OnClickListener, DisplableInterface {
    public static final String TAG = "EventDetailActivity";
    private Context mContext;
    private TextView tvEventTitle;
    private TextView tvEventDateDetail, tvEventTimeDetail;
    private ImageView imgEventVenue, imgPreferredPlayer;
    private TextView tvEventVenueDetail, tvSportNameDetail, tvHostName, tvPermission, tvParticipating, tvNumberOFPlayersJoinedValue, tvNumberOfPlayersRequired, tvDescription;
    private TextView tvHeaderTitle, tvHeaderDone;
    private ImageView imgPrevious;
    private Button btnConfirmEvent;
    private EventDetailsModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_event_detail);
        mContext = getApplicationContext();
        initViews();
        initData();
        initListener();
    }

    private void initViews() {

        imgPrevious = (ImageView) findViewById(R.id.img_previous);
        tvHeaderTitle = (TextView) findViewById(R.id.tv_header_topbar);
        tvHeaderDone = (TextView) findViewById(R.id.tv_done_topbar);
        tvHeaderDone.setVisibility(View.VISIBLE);

        tvHeaderTitle.setText("Event Details");
        tvHeaderDone.setText("Edit");

        tvEventTitle = (TextView) findViewById(R.id.tv_event_title);
        tvEventDateDetail = (TextView) findViewById(R.id.tv_event_date_detail);
        tvEventTimeDetail = (TextView) findViewById(R.id.tv_event_time_detail);
        imgEventVenue = (ImageView) findViewById(R.id.img_event_venue);
        tvEventVenueDetail = (TextView) findViewById(R.id.tv_event_venue_detail);
        tvSportNameDetail = (TextView) findViewById(R.id.tv_sport_name_detail);
        tvHostName = (TextView) findViewById(R.id.tv_host_name_value);
        tvNumberOFPlayersJoinedValue = (TextView) findViewById(R.id.tv_number_of_players_joined_value);
        tvNumberOfPlayersRequired = (TextView) findViewById(R.id.tv_number_of_players_value);
        tvPermission = (TextView) findViewById(R.id.tv_permission_event);
        tvParticipating = (TextView) findViewById(R.id.tv_participating_event_value);
        tvDescription = (TextView) findViewById(R.id.tv_description_event_detail);
        imgPreferredPlayer = (ImageView) findViewById(R.id.img_preferred_players_event);

        btnConfirmEvent = (Button) findViewById(R.id.btn_confirm_event);
    }

    private void initListener() {
        imgPrevious.setOnClickListener(this);
        tvHeaderTitle.setOnClickListener(this);
        tvHeaderDone.setOnClickListener(this);
        btnConfirmEvent.setOnClickListener(this);
    }

    private void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            getEventDetails(intent.getExtras().getString("EVENT_ID"));
        }
    }

    private void getEventDetails(String eventId) {
        MainController controller = new MainController(EventDetailActivity.this, this, Constants.EVENT_DETAILS, true);
        String finalUrl = Constants.BASE_URL + Constants.EVENT_DETAILS_API + eventId;
        controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN_TOKEN);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_previous:
                finish();

            case R.id.btn_confirm_event:
                confirmEvent();

            case R.id.tv_done_topbar:
                editEvent();


            default:
                break;
        }
    }

    private void editEvent() {
        // Utility.ShowToast(EventDetailActivity.this,Constants.UNDER_DEVELOPMENT);
        Intent intentEditEvent = new Intent(mContext, EditEventActivity.class);
        intentEditEvent.putExtra("MY_EVENT", mModel);
        startActivity(intentEditEvent);
    }

    private void confirmEvent() {
        finish();
       /* MainController controller = new MainController(EventDetailActivity.this, this, Constants.CONFIRM_EVENT, true);
        String finalUrl = Constants.BASE_URL + Constants.CONFIRM_EVENT_API ;
        controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN_TOKEN);*/
    }


    @Override
    public void setScreenData(Hashtable obj, byte type, String Responce) {
        switch (type) {
            case Constants.EVENT_DETAILS:
                Hashtable<String, Object> result = (Hashtable<String, Object>) obj;
                ArrayList<EventDetailsModel> list = (ArrayList<EventDetailsModel>) obj.get("data");
                if (list != null && list.size() > 0) {
                    mModel = list.get(0);
                    setEventDetails(mModel);
                }

                ResponseInfo responseInfo = (ResponseInfo) obj.get("responseInfo");
                if (responseInfo != null && responseInfo.getMessage() != null) {
                    if (!TextUtils.isEmpty(responseInfo.getMessage()))
                        Utility.ShowToast(mContext, responseInfo.getMessage());
                }
                break;

            case Constants.CONFIRM_EVENT:
                Hashtable<String, Object> resultConfirmEvent = (Hashtable<String, Object>) obj;
               /* ArrayList<EventDetailsModel> listConfirmEvent = (ArrayList<EventDetailsModel>) obj.get("data");
                if (listConfirmEvent != null && listConfirmEvent.size() > 0) {
                    mModel = listConfirmEvent.get(0);
                    setEventDetails(mModel);
                }*/

                ResponseInfo responseInfoConfirmEvent = (ResponseInfo) obj.get("responseInfo");
                if (responseInfoConfirmEvent != null && responseInfoConfirmEvent.getMessage() != null) {
                    if (!TextUtils.isEmpty(responseInfoConfirmEvent.getMessage()))
                        Utility.ShowToast(mContext, responseInfoConfirmEvent.getMessage());
                    finish();
                }
                break;
        }
    }

    private void setEventDetails(EventDetailsModel model) {

       /*{
    "results": [
        {
            "eventId": 228,
            "title": "Football India",
            "description": "India is home to a diverse population playing many different sports across the country",
            "sportId": 5,
            "playinId": "224b81fc-9fec-47e9-9656-13988a288f22",
            "creatorName": "Ashutosh Srivastava ",
            "entityType": 1,
            "eventStartDate": 0,
            "eventEndDate": 0,
            "preferredPlayers": 0,
            "isTeamEvent": false,
            "numberOfPlayersRequired": 7,
            "numberOfPlayersJoined": 0,
            "location": {
                "locationId": 56,
                "latitude": 12.378,
                "longitude": 13.78786,
                "geoCodeAddress": "agsdhgaksas",
                "userEnteredAddress": "A code and abstract concept expressing a location on the Earth's surface",
                "shortAddress": null,
                "area": "Sarnath",
                "city": "Varanasi",
                "isEventLocation": true,
                "locationName": null
            },
            "isCreatorAParticipant": false,
            "imagePath": null,
            "needsCreatorPermissionToJoinEvent": true,
            "joinStatus": 1
        }
    ],
    "responseInfo": {
        "title": "Successfull",
        "message": "",
        "responseCode": 0
    }
}*/

        tvEventTitle.setText("Event From   " + model.getCreatorName());

        tvEventVenueDetail.setText(model.getTitle());
        tvSportNameDetail.setText(model.getTitle());

        tvHostName.setText(model.getCreatorName());

        tvNumberOFPlayersJoinedValue.setText(String.valueOf(model.getNumberOfPlayersJoined()));
        tvNumberOfPlayersRequired.setText(String.valueOf(model.getNumberOfPlayersRequired()));

        if (model.isNeedsCreatorPermissionToJoinEvent())
            tvPermission.setText("YES");
        else
            tvPermission.setText("NO");

        if (model.isCreatorAParticipant())
            tvParticipating.setText("YES");
        else
            tvParticipating.setText("YES");

        if (model.getPreferredPlayers().equalsIgnoreCase("1")) {
            imgPreferredPlayer.setImageDrawable(getResources().getDrawable(R.mipmap.icon_female));
        } else if (model.getPreferredPlayers().equalsIgnoreCase("2")) {
            imgPreferredPlayer.setImageDrawable(getResources().getDrawable(R.mipmap.icon_male));
        } else {

        }
        tvDescription.setText(model.getDescription());
    }

    @Override
    public void setScreenData(String obj) {

    }

    @Override
    public void setScreenMessage(String obj, byte type) {

    }

    @Override
    public void setCancelMessage(String obj, byte type) {

    }
}
