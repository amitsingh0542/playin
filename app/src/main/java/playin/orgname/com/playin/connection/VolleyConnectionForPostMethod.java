/*
package playin.orgname.com.playin.connection;

import android.app.Activity;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import playin.orgname.com.playin.components.ShowLoader;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.interfaces.ConnManagerInterface;

public class VolleyConnectionForPostMethod {
    private ConnManagerInterface iConnInterface;
    private String tag_string_req = "string_req1";
    private ShowLoader loader;
    private Activity mActivity;

    public VolleyConnectionForPostMethod(Activity activity, ConnManagerInterface connInterface) {
        this.iConnInterface = connInterface;
        mActivity = activity;
        loader = ShowLoader.getInstance(mActivity);
    }


    public void makeJsonObjReq(final Map<String, String> temp, boolean isLoaderVisible) {
        showProgressDialog();
        String regURL = null;
        regURL = Constants.REQUEST_BASE_URL + Constants.USERPROFILE;
        StringRequest sr = new StringRequest(Method.POST, regURL,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        */
/*Log.d(TAG, response.toString());
						msgResponse.setText(response.toString());*//*

                        iConnInterface.successResponseProcess(response);

                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.d(TAG, "Error: " + error.getMessage());
                if(error instanceof NoConnectionError) {
                    String strerror = "No internet Access, Check your internet connection.";
                    iConnInterface.failedResponseProcess(strerror);
                    System.out.println(strerror);
                }
               else  if(error instanceof TimeoutError) {
                    String strerror = "Connection timed out.";
                    iConnInterface.failedResponseProcess(strerror);
                    System.out.println(strerror);
                }
                else
                {
                    String strerror = "Please try again.";
                    iConnInterface.failedResponseProcess(strerror);
                }
                hideProgressDialog();
            }
        })

        {

            */
/**
             * Passing some request headers
             * *//*

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
//                Map<String, String> params = temp;
                Map<String,String> params = new HashMap<String,String>();
                params =temp;
                System.out.println(params.size()+ " params.size()");
                return params;
            }

        };

        // Adding request to request queue
//        AppController.getInstance().addToRequestQueue(jsonObjReq,tag_json_obj);
        sr.setShouldCache(false);
        AwfisApplication.getInstance().addToRequestQueue(sr, tag_string_req);
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
    }

    private void showProgressDialog() {
        loader.run(false);
    }

    private void hideProgressDialog() {
        loader.dismis();
    }
}
*/
