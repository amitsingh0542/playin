package playin.orgname.com.playin.connection;

import android.app.Activity;
import android.util.Log;

import com.android.volley.NoConnectionError;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

import playin.orgname.com.playin.bootstrap.PlayInApplicationBootstrap;
import playin.orgname.com.playin.components.ShowLoader;
import playin.orgname.com.playin.interfaces.ConnManagerInterface;

public class VolleyConnection {
	private ConnManagerInterface iConnInterface;
	private String tag_string_req = "string_req1";
    private ShowLoader loader;
    private Activity mActivity;
	String textfile;

	public VolleyConnection(Activity activity, ConnManagerInterface connInterface){
		this.iConnInterface = connInterface;
        mActivity = activity;
        loader = ShowLoader.getInstance(mActivity);
	}

	public static String convertStreamToString(InputStream is)
			throws IOException {
		Writer writer = new StringWriter();

		char[] buffer = new char[2048];
		try {
			Reader reader = new BufferedReader(new InputStreamReader(is,
					"UTF-8"));
			int n;
			while ((n = reader.read(buffer)) != -1) {
				writer.write(buffer, 0, n);
			}
		} finally {
			is.close();
		}
		String text = writer.toString().trim();
		return text;
	}
	
	
	public void executeRequest(String url, boolean isLoaderVisible) {
		showProgressDialog();

		try {
			//mContext.getAssets().open("response.txt");
			InputStream is =  mActivity.getApplication().getAssets().open("response.txt");
			textfile = convertStreamToString(is);

		} catch (IOException e) {
			e.printStackTrace();
		}

		//System.out.println(textfile);

		StringRequest strReq = new StringRequest(Method.GET, url, new Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.d("", response.toString()); 
						//msgResponse.setText(response.toString());
						//iConnInterface.successResponseProcess(textfile);
						iConnInterface.successResponseProcess(response);
						hideProgressDialog();

					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d("", "Error: " + error.getMessage()); 
						if(error instanceof NoConnectionError) {
						       String strerror = "No internet Access, Check your internet connection.";
						       iConnInterface.failedResponseProcess(strerror);
						       System.out.println(strerror);
						   }
						else if(error instanceof TimeoutError) {
						       String strerror = "Connection timed out.";
						       iConnInterface.failedResponseProcess(strerror);
						       System.out.println(strerror);
						   }
						else
						{
							String strerror = "Please try again";
							iConnInterface.failedResponseProcess(strerror);
						}

						 
						hideProgressDialog();
					}
				});
		

		// Adding request to request queue

		strReq.setShouldCache(false);

		PlayInApplicationBootstrap.getInstance().addToRequestQueue(strReq, tag_string_req);

	}
	
	private void showProgressDialog() {
       loader.run(true);
	}

	private void hideProgressDialog() {

		loader.dismis(true);
	}
}
