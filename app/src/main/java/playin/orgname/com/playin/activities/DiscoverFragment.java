package playin.orgname.com.playin.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Hashtable;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.adapters.EventForMeAdapter;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.EventForMeModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.model.UserModel;

public class DiscoverFragment extends Fragment implements GoogleMap.OnMapLongClickListener,DisplableInterface
{
	public static final String TAG = "DiscoverFragment";
	private Context mContext;
	 //static final LatLng TutorialsPoint = new LatLng(21 , 57);
	static final LatLng TutorialsPoint = new LatLng(19.1579 , 72.9935);

	final int RQS_GooglePlayServices = 1;
	GoogleMap myMap;
	//TextView tvLocInfo;

	private GoogleMap googleMap;
	private View view = null;
	private ListView lvEventForMe;
	ArrayList<EventForMeModel> listEventForMe = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			view = inflater.inflate(R.layout.activity_event_for_me, container, false);
		} catch (InflateException e) {

		}
		initViews(view);
		initListener();
		//getEventForMe();
		return view;
	}

	private void initViews(View view)
	{
		//initMap(view);
		lvEventForMe=(ListView)view.findViewById(R.id.lv_event_for_me);
	}

	private void initListener()
	{
		lvEventForMe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,	long id)
			{
				Intent intent = new Intent(mContext, EventDetailActivity.class);
				startActivity(intent);
			}
		});
	}

	private void getEventForMe()
	{
		MainController controller = new MainController(getActivity(), this, Constants.EVENT_FOR_ME, true);
		String finalUrl= Constants.BASE_URL + Constants.EVENT_FOR_ME_API;
		controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN);
	}





	@Override
	public void onMapLongClick(LatLng point) {
		Marker newMarker = myMap.addMarker(new MarkerOptions()
				.position(point)
				.snippet(point.toString()));
		newMarker.setTitle(newMarker.getId());

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void setScreenData(Hashtable obj, byte type, String Responce) {
		switch (type)
		{
			case Constants.EVENT_FOR_ME:
				Hashtable<String, Object> hashtableEventForMe = (Hashtable<String, Object>) obj;
				ArrayList<EventForMeModel> listEventForMe = (ArrayList<EventForMeModel>) obj.get("data");
				if (listEventForMe != null && listEventForMe.size() > 0)
				{
					EventForMeModel model = listEventForMe.get(0);
					EventForMeAdapter myEventAdapter=new EventForMeAdapter(getActivity(),listEventForMe);
					lvEventForMe.setAdapter(myEventAdapter);
				}

				ResponseInfo responseInfoEventForMe = (ResponseInfo) obj.get("responseInfo");
				if (responseInfoEventForMe != null && responseInfoEventForMe.getMessage() != null)
					Utility.ShowToast(mContext, responseInfoEventForMe.getMessage());

				break;
		}
	}

	@Override
	public void setScreenData(String obj) {

	}

	@Override
	public void setScreenMessage(String obj, byte type) {

	}

	@Override
	public void setCancelMessage(String obj, byte type) {

	}
}
