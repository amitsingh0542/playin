package playin.orgname.com.playin.interfaces;

import java.util.Hashtable;

public interface DisplableInterface {
	public void setScreenData(final Hashtable obj, final byte type, final String Responce);
	public void setScreenData(final String obj);
	public void setScreenMessage(final String obj, final byte type);
	public void setCancelMessage(final String obj, final byte type);
}
