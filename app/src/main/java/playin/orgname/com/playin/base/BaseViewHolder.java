package playin.orgname.com.playin.base;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;

public abstract class BaseViewHolder {
	
	public  Activity mActivity;
	public LayoutInflater mLayoutInflater;
	protected View mView;

	public BaseViewHolder(Activity activity) {
		this.mActivity = activity;
		this.mLayoutInflater = activity.getLayoutInflater();
	}
	
	public abstract void applyData();
	public abstract void initializeData(Object data);


}
