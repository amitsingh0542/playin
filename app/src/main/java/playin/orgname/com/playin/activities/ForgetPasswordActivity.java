package playin.orgname.com.playin.activities;

import java.net.MalformedURLException;
import java.net.URL;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.base.BaseActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.Window;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;



public class ForgetPasswordActivity extends BaseActivity {

	WebView appWebview;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//getWindow().requestFeature(Window.FEATURE_PROGRESS);
		setContentView(R.layout.activity_forget_password);
		appWebview = (WebView) findViewById(R.id.webviewid);
		startWebView();
	}
	
	private void startWebView() {
        
		appWebview.setWebViewClient(new WebViewClient() {      
            ProgressDialog progressDialog;
          
            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {              
                view.loadUrl(url);
                return true;
            }
        
            //Show loader on url load
            public void onLoadResource (WebView view, String url) {
                if (progressDialog == null) {
                    // in standard case YourActivity.this
                    progressDialog = new ProgressDialog(ForgetPasswordActivity.this);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }
            }
            public void onPageFinished(WebView view, String url) {
                try{
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
                }catch(Exception exception){
                    exception.printStackTrace();
                }
            }
             
        }); 
          
         // Javascript inabled on webview  
		appWebview.getSettings().setJavaScriptEnabled(true); 
         
         
        //Load url in webview
		//appWebview.loadUrl(KYCConstants.Forget_Password);
          
          
    }
     

	@SuppressLint("SetJavaScriptEnabled")
	private void setWebViewSettings() {
		// Enable Java Script and Dom Storage
		appWebview.getSettings().setJavaScriptEnabled(true);
		appWebview.getSettings().setPluginState(PluginState.ON);
		appWebview.getSettings().setDomStorageEnabled(true);
		appWebview.getSettings().setSavePassword(false);
		appWebview.getSettings().setAllowContentAccess(true);
		appWebview.getSettings().setSupportZoom(true);
		appWebview.getSettings().setBuiltInZoomControls(true);
		//appWebview.getSettings().setAllowUniversalAccessFromFileURLs(true);
		appWebview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		appWebview.getSettings().setCacheMode(
				android.webkit.WebSettings.LOAD_DEFAULT);
		appWebview.getSettings().setEnableSmoothTransition(true);
		appWebview.getSettings().setUseWideViewPort(true);
		appWebview.getSettings().setLoadWithOverviewMode(true);
		appWebview.getSettings().setUseWideViewPort(true);
		appWebview.getSettings().setSupportMultipleWindows(true);
	}

	public void initialiseCookieManager() {
		
	}

	private void setFromNativeAppCookie() {
		
	}

	private class CustomWebViewClient extends WebViewClient {
		
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			//Log.d("shouldOverrideUrlLoading : ", " url : " + url + "Uri : " + Uri.parse(url));
			URL url1 = null;
			try {
				url1 = new URL(url);
			}
			catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
			
		}

		public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
			//Log.d("CustomWebViewClient", "shouldInterceptRequest : URl : " + url);
			return null;
		}

		@Override
		public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
			
			handler.proceed();
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			super.onReceivedError(view, errorCode, description, failingUrl);
			//Log.d("onReceivedError", "failingUrl : " + failingUrl);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			//Log.d("onPageStarted : ", "url : " + url);
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
			
			//handler.proceed("saktidatt.pradhan", "ril@9876");
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			// Log.d("onPageFinished : ", "finished url : " + url);
			//CookieSyncManager.getInstance().sync();

			
		}

		@Override
		public void onLoadResource(WebView view, String url) {
			//Log.d("onLoadResource : ", "url : " + url);

			super.onLoadResource(view, url);
		}

	}
	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		if(appWebview.canGoBack()) {
			appWebview.goBack();
        } else {
            // Let the system handle the back button
            super.onBackPressed();
        }
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
        if(appWebview != null) {
        	appWebview.removeAllViews();
        	appWebview.destroy();
        }
        appWebview = null;

	}
	

}
