package playin.orgname.com.playin.model;

/**
 * Created by Shashvat on 6/25/2016.
 */
public class ResponseInfo {
    private String title = null;
    private String message = null;
    private int responseCode = 0;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

}
