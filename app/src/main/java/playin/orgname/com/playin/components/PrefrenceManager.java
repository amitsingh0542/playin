package playin.orgname.com.playin.components;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PrefrenceManager {

	private SharedPreferences pref;

	private Editor editor;
	private Context mContext;

	private final String PREF_NAME = "PizzaHutPref";

	private final String SAVE_REQ_TYPE = "ReqType";
	private final String SAVE_SERVICE_NAME = "ReqName";

	private final String SAVE_REQ_URI = "ReqUri";
	private final String SAVE_REQ_SOAP_ACTION = "ReqSoapAction";
	private final String SAVE_REQ_PAYLOAD = "ReqPayload";

	private final String SAVE_SESSION_ID = "SessionId";
	private final String SAVE_SESSION_ID_STORE_LOCATOR = "SessionIdStoreLocator";

	private final String LAST_CITY_FOR_STORE_LIST = "LastCityForStoreList";
	private final String ATTAMPT_FOR_STORE_LIST = "StoreListAtampt";

	private final String STORE_ID_FOR_BROWSING = "StoreIdForBrowsing";

	private final String BASKET_RESPONSE = "basketresponse";
	private final String BASKET_RESPONSE_TYPE = "basketresponsetype";
	private final String BASKET_RESPONSE_TIME = "basketresponsetime";

	private String SCREEN_ID = "screenid";
	private String ORDER_TIME = "ordertime";
	private String ORDER_CLASS = "orderclass";
	private String ORDER_PTIME = "orderptime";
	private final String ORDER_ID = "Order_id";
	private final String ORDER_OT_ID = "Order_OT_id";
	private String ORDER_DISPLAY_TIME = "orderDtime";
	private String ORDER_MOB_NO = "orderMob";
	private String COPY_CODE_BAR = "copycodebar";

	private String USERNAME = "username";
	private String PASSWORD = "password";

	private String GUEST = "isguest";
	private String REFRESH_APPLICATION = "refreshapplication";

	private String IS_BASKET_ITEM_AVAILABLE = "isbasketitemavailable";

	private String LATITUDE = "latitude";
	private String LONGITUDE = "longitude";
	private String SAVING_TIME = "timeDiffSave";
	private String BASKET_ITEM_COUNT = "BasketItemCount";
	private String SESSION_EXPIRED = "sessionexpired";
	private String ORDER_FAVOURITE = "favouriteorder";
	private String APP_VERSION = "appversion";
	private String SERVICE_REQUEST = "request";
	private String SERVICE_RESPONSE = "response";
	
	private String LAST_UPDATE_TIME_STAMP_VALUE = "lastupdatetimestamp";
	private String LAST_UPDATE_TIME_STAMP = null;

	private boolean isFromNotification = false;
	private String menuCategoryResponse = null;
	private String MENU_CATEGORY = "menucategory";
	private String MENU_CATEGORY_IDS = "menucategoryids";

	private String MENU_CATEGORY_NAME = "menucategoryname";
	private String CART_COUNT_ANIMATE = "cartcountanimate";
	
	private String menuCategoryIds = null;
	
	private String GPS_COUNT = "gpscount";

	
	public PrefrenceManager(Context context) {
		this.mContext = context;
		pref = mContext.getSharedPreferences(PREF_NAME, 0);
		editor = pref.edit();
	}

	public void setisFirstLAunch(boolean name) {
		editor.putBoolean("isFirstlaunch", name);

		editor.commit();
	}

	public boolean getisFirstLAunch() {
		boolean value = pref.getBoolean("isFirstlaunch", false);
		return value;
	}

	public boolean isGPSActive() {
		// TODO Auto-generated method stub
		return pref.getBoolean("isGPSActive", false);
	}

	public void setISGPSActive1(boolean isAct){
		editor.putBoolean("isGPSActive", isAct);
		editor.commit();
	}

	public void setisDelLAunch(boolean name) {
		editor.putBoolean("isDelLAunch", name);
		editor.commit();
	}
	
	public boolean getisDelLAunch() {
		boolean value = pref.getBoolean("isDelLAunch", false);
		return value;
	}
	
	public void setisAddressLAunch(boolean name) {
		editor.putBoolean("isAddressLAunch", name);
		editor.commit();
	}
	
	public boolean getisAddressLAunch() {
		boolean value = pref.getBoolean("isAddressLAunch", false);
		return value;
	}
	
	public void setisPizzaLIst(boolean name) {
		editor.putBoolean("isPizzaLIst", name);
		editor.commit();
	}
	
	public boolean getisPizzaLIst() {
		boolean value = pref.getBoolean("isPizzaLIst", false);
		return value;
	}
	
	public boolean getisTimeLAunch() {
		boolean value = pref.getBoolean("isTimeLAunch", false);
		return value;
	}
	
	public void setisTimeLAunch(boolean name) {
		editor.putBoolean("isTimeLAunch", name);
		editor.commit();
	}

	public void setisPIzzaLAunch(boolean name) {
		editor.putBoolean("isPIzzaLAunch", name);
		editor.commit();
	}
	
	public boolean getisPIzzaLAunch() {
		boolean value = pref.getBoolean("isPIzzaLAunch", false);
		return value;
	}
	
	public void setisCartLaunch(boolean name) {
		editor.putBoolean("isCartLaunch", name);
		editor.commit();
	}
	
	public boolean getisCartLaunch() {
		boolean value = pref.getBoolean("isCartLaunch", false);
		return value;
	}
	
	public void setcopycodebar(String name) {
		editor.putString(COPY_CODE_BAR, name);
		editor.commit();
	}

	public String getcopycodebar() {
		String value = pref.getString(COPY_CODE_BAR, null);
		return value;
	}

	public void setReqType(byte type) {
		editor.putInt(SAVE_REQ_TYPE, type);
		editor.commit();
	}

	public byte getReqType() {
		byte value = (byte) pref.getInt(SAVE_REQ_TYPE, -1);
		return value;
	}

	public void setStoreListScreenAttampt(byte type) {
		editor.putInt(ATTAMPT_FOR_STORE_LIST, type);
		editor.commit();
	}

	public byte getStoreListScreenAttampt() {
		byte value = (byte) pref.getInt(ATTAMPT_FOR_STORE_LIST, -1);
		return value;
	}
	public void setServiceName(String name) {
		editor.putString(SAVE_SERVICE_NAME, name);
		editor.commit();
	}

	public String getServiceName() {
		String value = pref.getString(SAVE_SERVICE_NAME, null);
		return value;
	}
	
	public void setLastStoreCitySel(String name) {
		editor.putString(LAST_CITY_FOR_STORE_LIST, name);
		editor.commit();
	}
	
	public int getStoreIdForJustBrowsing() {
		int value = pref.getInt(STORE_ID_FOR_BROWSING, -1);
		return value;
	}
	
	public void setStoreIdForJustBrowsing(int storeId) {
		editor.putInt(STORE_ID_FOR_BROWSING, storeId);
		editor.commit();
	}

	public String getStoreNumber() {
		String value = pref.getString("StoreNo", "3988-3988");
		return value;
	}
	
	public void setStoreNumber(String storeNo) {
		editor.putString("StoreNo", storeNo);
		editor.commit();
	}
	
	public String getLastStoreCitySel() {
		String value = pref.getString(LAST_CITY_FOR_STORE_LIST, null);
		return value;
	}

	public void setReqUrl(String message) {
		editor.putString(SAVE_REQ_URI, message);
		editor.commit();
	}

	public String getReqUrl() {
		String value = pref.getString(SAVE_REQ_URI, null);
		return value;
	}

	public void setReqPayload(String message) {
		editor.putString(SAVE_REQ_PAYLOAD, message);
		editor.commit();
	}

	public String getReqPayload() {
		String value = pref.getString(SAVE_REQ_PAYLOAD, null);
		return value;
	}

	public void setReqSoapAction(String message) {
		editor.putString(SAVE_REQ_SOAP_ACTION, message);
		editor.commit();
	}

	public String getReqSoapAction() {
		String value = pref.getString(SAVE_REQ_SOAP_ACTION, null);
		return value;
	}

	public void saveSessionId(String message) {
		editor.putString(SAVE_SESSION_ID, message);
		editor.commit();
	}

	public String getSessionId() {
		String value = pref.getString(SAVE_SESSION_ID, null);
		return value;
	}
	
	public void saveSessionIdStorelocator(String message) {
		editor.putString(SAVE_SESSION_ID_STORE_LOCATOR, message);
		editor.commit();
	}

	public String getSessionIdStorelocator() {
		String value = pref.getString(SAVE_SESSION_ID_STORE_LOCATOR, null);
		return value;
	}

	public void saveBasketResponse(String response) {
		editor.putString(BASKET_RESPONSE, response);
		editor.commit();
	}

	public String getBasketResponse() {
		String value = pref.getString(BASKET_RESPONSE, null);
		return value;
	}

	public void saveBasketResponseType(String response) {
		editor.putString(BASKET_RESPONSE_TYPE, response);
		editor.commit();
	}

	public String getBasketResponseType() {
		String value = pref.getString(BASKET_RESPONSE_TYPE, null);
		return value;
	}

	public void saveBasketResponseTime(String response) {
		editor.putString(BASKET_RESPONSE_TIME, response);
		editor.commit();
	}

	public String getBasketResponseTime() {
		String value = pref.getString(BASKET_RESPONSE_TIME, null);
		return value;
	}

	public String getSCREEN_ID() {
		String value = pref.getString(SCREEN_ID, null);
		return value;
	}

	public void setSCREEN_ID(String sCREEN_ID) {
		editor.putString(SCREEN_ID, sCREEN_ID);
		editor.commit();
	}

	public String getORDER_TIME() {
		String value = pref.getString(ORDER_TIME, null);
		return value;
	}

	public void setORDER_TIME(String oRDER_TIME) {
		editor.putString(ORDER_TIME, oRDER_TIME);
		editor.commit();
	}

	public String getORDER_ID() {
		String value = pref.getString(ORDER_ID, null);
		return value;
	}

	public void setORDER_ID(String id) {
		editor.putString(ORDER_ID, id);
		editor.commit();
	}
	public String getORDER_CLASS() {
		String value = pref.getString(ORDER_CLASS, "0");
		return value;
	}

	public void setORDER_CLASS(String oRDER_CLASS) {
		editor.putString(ORDER_CLASS, oRDER_CLASS);
		editor.commit();
	}

	public boolean isBasketItemAvailale() {
		boolean value = pref.getBoolean(IS_BASKET_ITEM_AVAILABLE, false);
		return value;
	}

	public void setBasketItemAvailability(boolean isBasketAvailable) {
		editor.putBoolean(IS_BASKET_ITEM_AVAILABLE, isBasketAvailable);
		editor.commit();
	}

	public boolean isGuest() {
		boolean value = pref.getBoolean(GUEST, false);
		return value;
	}

	public void setGuest(boolean isGuest) {
		editor.putBoolean(GUEST, isGuest);
		editor.commit();
	}


	public String getLatitude() {
		String value = pref.getString(LATITUDE, "0.0");
		return value;
	}

	public void setLatitude(String latitude) {
		editor.putString(LATITUDE, latitude);
		editor.commit();
	}

	public String getLongitude() {
		String value = pref.getString(LONGITUDE,  "0.0");
		return value;
	}

	public void setLongitude(String longitude) {
		editor.putString(LONGITUDE, longitude);
		editor.commit();
	}

	public String getORDER_PTIME() {
		String value = pref.getString(ORDER_PTIME, null);
		return value;
	}

	public void setORDER_PTIME(String oRDER_TIME) {
		editor.putString(ORDER_PTIME, oRDER_TIME);
		editor.commit();
	}

	public String getORDEROTID() {
		String value = pref.getString(ORDER_OT_ID, null);
		return value;
	}

	public void setORDEROTID(String orderId) {
		editor.putString(ORDER_OT_ID, orderId);
		editor.commit();
	}

	public String getORDER_DISPLAY_TIME() {
		return pref.getString(ORDER_DISPLAY_TIME, null);
	}

	public void setORDER_DISPLAY_TIME(String oRDER_DISPLAY_TIME) {
		editor.putString(ORDER_DISPLAY_TIME, oRDER_DISPLAY_TIME);
		editor.commit();
	}

	public String getORDER_MOB_NO() {
		return pref.getString(ORDER_MOB_NO, null);
	}

	public void setORDER_MOB_NO(String oRDER_MOB_NO) {
		editor.putString(ORDER_MOB_NO, oRDER_MOB_NO);
		editor.commit();
	}

	public boolean isFBUser(String userId) {
		boolean value = pref.getBoolean(userId, false);
		return value;
	}

	public void setFBUser(String cId,boolean isfb) {
		editor.putBoolean(cId, isfb);
		editor.commit();
	}

	public String getUsername() {
		return pref.getString(USERNAME, null);
	}

	public void setUsername(String username) {
		editor.putString(USERNAME, username);
		editor.commit();
	}

	public String getPassword() {
		return pref.getString(PASSWORD, null);
	}

	public void setPassword(String password) {
		editor.putString(PASSWORD, password);
		editor.commit();
	}

	public long getOldTime() {
		long value = pref.getLong(SAVING_TIME, 0);
		return value;
	}


	public void setOldTime(long name) {
		editor.putLong(SAVING_TIME, name);
		editor.commit();
	}

	public String getBasketItemCount() {
		return pref.getString(BASKET_ITEM_COUNT, "0");
	}

	public void setBasketItemCount(String basketItemCount) {
		editor.putString(BASKET_ITEM_COUNT, basketItemCount);
		editor.commit();
	}

	public void setStoreAddress(String sa) {
		// TODO Auto-generated method stub
		editor.putString("SSAddress", sa);
		editor.commit();
	}

	public String getStoreAddress() {
		return pref.getString("SSAddress", null);
	}

	public void setOutletAddress(String sa) {
		// TODO Auto-generated method stub
		editor.putString("OutletAddress", sa);
		editor.commit();
	}
	
	public int getOrderType() {
		return pref.getInt("OrderType", 0);
	}

	public void setOrderType(int sa) {
		// TODO Auto-generated method stub
		editor.putInt("OrderType", sa);
		editor.commit();
	}
	
	public String getOutletAddress() {
		return pref.getString("OutletAddress", null);
	}


	public void setStartAndCategoryData(String responce) {
		// TODO Auto-generated method stub
		editor.putString("StartOrderData", responce);
		editor.commit();
	}

	public String getStartAndCategoryData() {
		return pref.getString("StartOrderData", null);
	}

	public boolean isSessionExpired() {
		boolean value = pref.getBoolean(SESSION_EXPIRED, false);
		return value;
	}

	public void setSessionExpired(boolean sessionExpired) {
		editor.putBoolean(SESSION_EXPIRED, sessionExpired);
		editor.commit();
	}
	
	public void setFavouriteOrder(String orderid) {
		// TODO Auto-generated method stub
		editor.putString(ORDER_FAVOURITE, orderid);
		editor.commit();
	}
	
	public String getFavouriteOrder() {
		return pref.getString(ORDER_FAVOURITE, null);
	}
	
	public void setAppCurrentVersion(String appVersion) {
		editor.putString(APP_VERSION, appVersion);
		editor.commit();
	}

	public String getAppCurrentVersion() {
		String value = pref.getString(APP_VERSION, "0");
		return value;
	}
	
	public void setServiceRequest(String request) {
		editor.putString(SERVICE_REQUEST, request);
		editor.commit();
	}

	public String getServiceRequest() {
		String value = pref.getString(SERVICE_REQUEST, null);
		return value;
	}
	
	public void setServiceResponse(String response) {
		editor.putString(SERVICE_RESPONSE, response);
		editor.commit();
	}

	public String getServiceResponse() {
		String value = pref.getString(SERVICE_RESPONSE, null);
		return value;
	}
	
	public String getUpdateversion() {
		return pref.getString("CheckDate", "1.4");
	}
	
	public void setUpdateversion(String d) {
		editor.putString("CheckDate", d);
		editor.commit();
	}
	
	public String getLastUpdatedTimeStamp() {
		String value = pref.getString(LAST_UPDATE_TIME_STAMP_VALUE, null);
		return value;
	}

	public void setLastUpdatedTimeStamp(String lAST_UPDATE_TIME_STAMP) {
		editor.putString(LAST_UPDATE_TIME_STAMP_VALUE, lAST_UPDATE_TIME_STAMP);
		editor.commit();
	}

	public boolean isFromNotification() {
		boolean value = pref.getBoolean("isFromNotification", isFromNotification);
		return value;
	}

	public void setFromNotification(boolean isFromNotification) {
		editor.putBoolean("isFromNotification", isFromNotification);
		editor.commit();
	}


	public String getMenuCategoryResponse() {
		String value = pref.getString(MENU_CATEGORY, null);
		return value;
		}

	public void setMenuCategoryResponse(String menuCategoryResponse) {
		editor.putString(MENU_CATEGORY, menuCategoryResponse);
		editor.commit();
	}
	
	
	public String getMenuCategoryIds() {
		String value = pref.getString(MENU_CATEGORY_IDS, null);
		return value;
	}

	public void setMenuCategoryIds(String menuCategoryIds) {
		editor.putString(MENU_CATEGORY_IDS, menuCategoryIds);
		editor.commit();
	}

	public String getMenuCategoryName() {
		String value = pref.getString(MENU_CATEGORY_NAME, null);
		return value;
	}

	public void setMenuCategoryName(String menuCategoryName) {
		editor.putString(MENU_CATEGORY_NAME, menuCategoryName);
		editor.commit();
	}

	
	public boolean isCartCountAnimation() {
		boolean value = pref.getBoolean(CART_COUNT_ANIMATE, false);
		return value;
	}

	public void setCartCountAnimate(boolean isAnimation) {
		editor.putBoolean(CART_COUNT_ANIMATE, isAnimation);
		editor.commit();
	}
	
	public int getGpsPopUpCount() {
		int value = pref.getInt(GPS_COUNT, 0); 
		return value;
	}

	public void setGpsPopUpCount(int gpsPopUpCount) {
		editor.putInt(GPS_COUNT, gpsPopUpCount);
		editor.commit();
	}
	
	public void clearGPSData(){
		editor.putString("CureentCity", null);
		editor.putString("urrentState", null);
		editor.putString("currentLocality", null);
		editor.putString("subsubLocality", null);
		editor.putString(LAST_CITY_FOR_STORE_LIST, null);
		editor.putInt(GPS_COUNT, 0);
		editor.putBoolean("isGPSActive", false);
		editor.putString(LATITUDE, "0.0");
		editor.putString(LONGITUDE, "0.0");
		editor.commit();
	}
	
	
	public void clearApplication() {
		editor.putBoolean(GUEST, false);
		editor.putString(BASKET_RESPONSE, null);
		editor.putString(BASKET_RESPONSE_TYPE, null);
		editor.putString(BASKET_RESPONSE_TIME, null);
		editor.putString(SCREEN_ID, null);
		editor.putString(ORDER_TIME, null);
		editor.putString(ORDER_CLASS, null);
		editor.putString(BASKET_RESPONSE, null);
		editor.putString(ORDER_MOB_NO, null);
		editor.putString(ORDER_DISPLAY_TIME, null);
//		editor.putInt(STORE_ID_FOR_BROWSING, -1);
		editor.putBoolean(IS_BASKET_ITEM_AVAILABLE, false);
		editor.putBoolean(SESSION_EXPIRED, false);
		editor.putString(BASKET_ITEM_COUNT, "0");
		editor.putString("SSAddress", null);
		editor.putString("StartOrderData", null);
		editor.putBoolean("isFromNotification", false);
		editor.putString(MENU_CATEGORY_IDS, null);
		editor.putString(MENU_CATEGORY_NAME, null);
		editor.putBoolean(CART_COUNT_ANIMATE, false);

		//editor.putString(SERVICE_REQUEST, null);
		editor.commit();
	}

}
