package playin.orgname.com.playin.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Hashtable;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.base.BaseActivity;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.EventDetailsModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.model.TeamDetailsModel;

public class TeamsDetailActivity extends BaseActivity implements OnClickListener, DisplableInterface {
    public static final String TAG = "TeamsDetailActivity";
    private Context mContext;
    private TextView tvEventTitle;
    private TextView tvEventDateDetail, tvEventTimeDetail;
    private ImageView imgEventVenue, imgPreferredPlayer;
    private TextView tvEventVenueDetail, tvSportNameDetail, tvHostName, tvPermission, tvParticipating, tvNumberOFPlayersJoinedValue, tvNumberOfPlayersRequired, tvDescription;
    private TextView tvHeaderTitle, tvHeaderDone;
    private ImageView imgPrevious;
    private Button btnConfirmEvent;
    private TeamDetailsModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_team_detail);
        mContext = getApplicationContext();
        initViews();
        initData();
        initListener();
    }

    private void initViews() {

        imgPrevious = (ImageView) findViewById(R.id.img_previous);
        tvHeaderTitle = (TextView) findViewById(R.id.tv_header_topbar);
        tvHeaderDone = (TextView) findViewById(R.id.tv_done_topbar);
        tvHeaderDone.setVisibility(View.VISIBLE);

        tvHeaderTitle.setText("Team Details");
        tvHeaderDone.setText("Edit");

        tvEventTitle = (TextView) findViewById(R.id.tv_event_title);
        tvEventDateDetail = (TextView) findViewById(R.id.tv_event_date_detail);
        tvEventTimeDetail = (TextView) findViewById(R.id.tv_event_time_detail);
        imgEventVenue = (ImageView) findViewById(R.id.img_event_venue);
        tvEventVenueDetail = (TextView) findViewById(R.id.tv_event_venue_detail);
        tvSportNameDetail = (TextView) findViewById(R.id.tv_sport_name_detail);
        tvHostName = (TextView) findViewById(R.id.tv_host_name_value);
        tvNumberOFPlayersJoinedValue = (TextView) findViewById(R.id.tv_number_of_players_joined_value);
        tvNumberOfPlayersRequired = (TextView) findViewById(R.id.tv_number_of_players_value);
        tvPermission = (TextView) findViewById(R.id.tv_permission_event);
        tvParticipating = (TextView) findViewById(R.id.tv_participating_event_value);
        tvDescription = (TextView) findViewById(R.id.tv_description_event_detail);
        imgPreferredPlayer = (ImageView) findViewById(R.id.img_preferred_players_event);

        btnConfirmEvent = (Button) findViewById(R.id.btn_confirm_event);
    }

    private void initListener() {
        imgPrevious.setOnClickListener(this);
        tvHeaderTitle.setOnClickListener(this);
        tvHeaderDone.setOnClickListener(this);
        btnConfirmEvent.setOnClickListener(this);
    }

    private void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            getEventDetails(intent.getExtras().getString("TEAM_ID"));
        }
    }

    private void getEventDetails(String teamId) {
        MainController controller = new MainController(TeamsDetailActivity.this, this, Constants.MY_TEAMS_DETAILS, true);
        String finalUrl = Constants.BASE_URL + Constants.MY_TEAMS_DETAILS_API + teamId;
        controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN_TOKEN);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_previous:
                finish();

            case R.id.btn_confirm_event:
                confirmEvent();

            case R.id.tv_done_topbar:
                editEvent();


            default:
                break;
        }
    }

    private void editEvent() {
        // Utility.ShowToast(EventDetailActivity.this,Constants.UNDER_DEVELOPMENT);
      /*  Intent intentEditEvent = new Intent(mContext, EditEventActivity.class);
        intentEditEvent.putExtra("MY_EVENT", mModel);
        startActivity(intentEditEvent);*/
    }

    private void confirmEvent() {
        finish();
       /* MainController controller = new MainController(EventDetailActivity.this, this, Constants.CONFIRM_EVENT, true);
        String finalUrl = Constants.BASE_URL + Constants.CONFIRM_EVENT_API ;
        controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN_TOKEN);*/
    }


    @Override
    public void setScreenData(Hashtable obj, byte type, String Responce) {
        switch (type) {
            case Constants.MY_TEAMS_DETAILS:
                Hashtable<String, Object> result = (Hashtable<String, Object>) obj;
                ArrayList<TeamDetailsModel> list = (ArrayList<TeamDetailsModel>) obj.get("data");
                if (list != null && list.size() > 0) {
                    mModel = list.get(0);
                    setEventDetails(mModel);
                }

                ResponseInfo responseInfo = (ResponseInfo) obj.get("responseInfo");
                if (responseInfo != null && responseInfo.getMessage() != null) {
                    if (!TextUtils.isEmpty(responseInfo.getMessage()))
                        Utility.ShowToast(mContext, responseInfo.getMessage());
                }
                break;

            case Constants.CONFIRM_EVENT:
                Hashtable<String, Object> resultConfirmEvent = (Hashtable<String, Object>) obj;
               /* ArrayList<EventDetailsModel> listConfirmEvent = (ArrayList<EventDetailsModel>) obj.get("data");
                if (listConfirmEvent != null && listConfirmEvent.size() > 0) {
                    mModel = listConfirmEvent.get(0);
                    setEventDetails(mModel);
                }*/

                ResponseInfo responseInfoConfirmEvent = (ResponseInfo) obj.get("responseInfo");
                if (responseInfoConfirmEvent != null && responseInfoConfirmEvent.getMessage() != null) {
                    if (!TextUtils.isEmpty(responseInfoConfirmEvent.getMessage()))
                        Utility.ShowToast(mContext, responseInfoConfirmEvent.getMessage());
                    finish();
                }
                break;
        }
    }

    private void setEventDetails(TeamDetailsModel model) {

       /*"name": "Team Name Here",
			"numberOfPlayersRequired": 11,
			"standbyPlayers": 4,
			"numberOfPlayersJoined": 10,
			"preferredPlayers": 3,
			"createdByPlayinId": "playinId Here",
			"createdByentityType": 1,
			"sportId": 1,
			"preferredLocation": {
				"latitude" : "12.378",
		        "longitude" : "13.78786",
		        "geoCodeAddress" : "agsdhgaksas",
		        "userEnteredAddress": "",
		        "area": "Yelahanka",
		        "city": "Bangalore",
		        "isEventLocation": false
			},
			"isCreatorATeamMember": true,
			"needsCreatorPermissionToJoin": true,
			"imagePath": "Image path comes here"*/

        tvEventTitle.setText("Event From   " + model.getName());

        //tvEventVenueDetail.setText(model.getTitle());
        //tvSportNameDetail.setText(model.getTitle());

        tvHostName.setText(model.getName());

        tvNumberOFPlayersJoinedValue.setText(""+model.getNumberOfPlayersJoined());
        tvNumberOfPlayersRequired.setText(""+model.getNumberOfPlayersRequired());

        if (model.isNeedsCreatorPermissionToJoin())
            tvPermission.setText("YES");
        else
            tvPermission.setText("NO");

        if (model.isCreatorATeamMember())
            tvParticipating.setText("YES");
        else
            tvParticipating.setText("YES");

        if (model.getPreferredPlayers()==1) {
            imgPreferredPlayer.setImageDrawable(getResources().getDrawable(R.mipmap.icon_female));
        } else if (model.getPreferredPlayers()==2) {
            imgPreferredPlayer.setImageDrawable(getResources().getDrawable(R.mipmap.icon_male));
        } else if (model.getPreferredPlayers()==3){
            imgPreferredPlayer.setImageDrawable(getResources().getDrawable(R.mipmap.ic_launcher));
        }
        //tvDescription.setText(model.getDescription());
    }

    @Override
    public void setScreenData(String obj) {

    }

    @Override
    public void setScreenMessage(String obj, byte type) {

    }

    @Override
    public void setCancelMessage(String obj, byte type) {

    }
}
