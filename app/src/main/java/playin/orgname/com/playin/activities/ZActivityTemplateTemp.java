package playin.orgname.com.playin.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.base.BaseActivity;

public class ZActivityTemplateTemp extends BaseActivity implements OnClickListener
{
	public static final String TAG = "ZActivityTemplateTemp";

	private Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		//getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_signup);
		mContext=getApplicationContext();
		initViews();
		initListener();
	}

	private void initViews()
	{

	}

	private void initListener()
	{

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{

		}
	}



}
