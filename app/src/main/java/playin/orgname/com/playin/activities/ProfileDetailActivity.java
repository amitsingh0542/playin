package playin.orgname.com.playin.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.customview.RoundedImageView;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.DataModel;
import playin.orgname.com.playin.model.FacebookUserModel;
import playin.orgname.com.playin.model.LoginModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.model.SignUpModel;

public class ProfileDetailActivity extends Fragment implements OnClickListener, DisplableInterface {
    public static final String TAG = "ProfileActivity";

    private Context mContext;
    private SignUpModel signUpModel = null;
    private LoginModel loginModel = null;
    private FacebookUserModel facebookModel = null;
    private EditText edtFirstName, edtLastName, edDateOfBirth;
   // private ImageView imgPrevious;
    private RoundedImageView imgProfileImage;
   // private TextView tvDone, tvHeader;
    private RadioButton rbGenderMale, rbGenderFemale;
    private String sex = null;
    private final int CAMERA_CODE=1;
    private final int GALLERY_CODE=2;
    private PopupWindow pwindo;
    private String strGender=null;
    private View view = null;
    private static int mHeight;
    private static int mWidth;

   /* protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_detail);
        mContext = getApplicationContext();
        initViews();
        initListener();
        initData();
    }
*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            view = inflater.inflate(R.layout.activity_profile_detail, container, false);
        } catch (InflateException e) {

        }
        mContext = getActivity().getApplicationContext();
        initViews(view);
        initListener();
        initData();
        setDeviceSize();
        return view;
    }
    private void initData() {

    }

    private void initViews(View view) {
        /*imgPrevious = (ImageView) findViewById(R.id.img_previous);
        imgPrevious.setVisibility(View.GONE);*/
        imgProfileImage = (RoundedImageView)view.findViewById(R.id.img_profile_image);
       // tvHeader = (TextView) findViewById(R.id.tv_header_topbar);
        edtFirstName = (EditText)view.findViewById(R.id.edt_first_name_profile);
        edtLastName = (EditText)view.findViewById(R.id.edt_last_name_profile);
        edDateOfBirth = (EditText)view.findViewById(R.id.edt_dob_profile);
      /*  tvDone = (TextView) findViewById(R.id.tv_done_topbar);
        tvDone.setVisibility(View.VISIBLE);
        tvHeader.setText("Profile");*/
        /*rbGenderMale = (RadioButton) findViewById(R.id.rb_gender_male);
        rbGenderFemale = (RadioButton) findViewById(R.id.rb_gender_female);*/

        RadioGroup radioGroupGender = (RadioGroup)view.findViewById(R.id.radio_group_gender);
        int checkedRadioButtonID = radioGroupGender.getCheckedRadioButtonId();
        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.rb_gender_male:
                        strGender = "1";
                        Log.v(TAG, "MALE");
                        break;
                    case R.id.rb_gender_female:
                        strGender = "0";
                        Log.v(TAG, "FEMALE");
                        break;
                    default:
                        break;
                }
            }
        });
    }

    private void initListener() {
        //tvDone.setOnClickListener(this);
        edDateOfBirth.setOnClickListener(this);
        imgProfileImage.setOnClickListener(this);
       /* rbGenderMale.setOnClickListener(this);
        rbGenderFemale.setOnClickListener(this);*/
    }

 /*   @Override
    public void onBackPressed() {
        //super.onBackPressed();
        showLogoutDialog();
    }
*/
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_done_topbar:
                //Utility.ShowToast(mContext, "Profile updated" + "Sex is :" + sex);
                updateProfile();
                break;

            case R.id.edt_dob_profile:
                setDateOfBirth();
                break;

            case R.id.img_profile_image:
                if(pwindo!=null)
                {
                   if(pwindo.isShowing())
                       pwindo.dismiss();
                }
                openPopup(v);
               // selectImage();
                break;

          /*  case R.id.rb_gender_male:

                break;

            case R.id.rb_gender_female:

                break;*/

            case R.id.ll_camera:
                openCamera();
                break;

            case R.id.ll_gallery:
                openGallery();
                break;
        }
    }

    private void showLogoutDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Logout");
        alertDialog.setMessage("Are you sure you want to logout?");
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }


    private void setDateOfBirth() {
        int year, month, day;
        final Calendar myCalendarDOB = Calendar.getInstance();
        year = myCalendarDOB.get(Calendar.YEAR);
        month = myCalendarDOB.get(Calendar.MONTH);
        day = myCalendarDOB.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                myCalendarDOB.set(Calendar.YEAR, selectedyear);
                myCalendarDOB.set(Calendar.MONTH, selectedmonth);
                myCalendarDOB.set(Calendar.DAY_OF_MONTH, selectedday);

                SimpleDateFormat fmtDate = new SimpleDateFormat("dd-MM-yyyy");
                Calendar myCalendarTemp = Calendar.getInstance();
                Calendar tempmyCalendarDOB = myCalendarTemp;
                tempmyCalendarDOB.set(Calendar.YEAR, myCalendarTemp.get(Calendar.YEAR) - 18);

                if (myCalendarDOB.before(tempmyCalendarDOB) || myCalendarDOB.equals(tempmyCalendarDOB)) {
                    edDateOfBirth.setText(fmtDate.format(myCalendarDOB.getTime().getTime()).toString());
                    edDateOfBirth.setTextColor(Color.BLACK);
                } else {
                    edDateOfBirth.setText("");
                    myCalendarDOB.clear();
                    Utility.ShowToast(mContext, "DOB cannot be less than 18 years");
                }
            }
        }, year, month, day);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }



    private void openCamera()
    {
        pwindo.dismiss();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(intent, CAMERA_CODE);
    }
    private void openGallery()
    {
        pwindo.dismiss();
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_CODE);
    }
    private void openPopup(View anchor) {
        try {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.screen_popup, (ViewGroup)view.findViewById(R.id.popup_parent));
            pwindo = new PopupWindow(layout, getWidth(), getHeight(), true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
            pwindo.setOutsideTouchable(true);
            // pwindo.setFocusable(true);
            pwindo.setBackgroundDrawable(new BitmapDrawable(getResources(), Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)));
            layout.setOnClickListener(this);
            pwindo.showAsDropDown(anchor, 0, 0);
            layout.findViewById(R.id.ll_gallery).setOnClickListener(this);
            layout.findViewById(R.id.ll_camera).setOnClickListener(this);

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_CODE) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    Bitmap bitmap;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                            bitmapOptions);

                    imgProfileImage.setImageBitmap(bitmap);

                    String path = Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + "Phoenix" + File.separator + "default";
                    f.delete();
                    OutputStream outFile = null;
                    File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
                    try {
                        outFile = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        outFile.flush();
                        outFile.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == GALLERY_CODE) {

                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                //Log.w("path of image from gallery......******************.........", picturePath + "");
                imgProfileImage.setImageBitmap(thumbnail);
            }
        }
    }*/

    private void updateProfile()
    {
        if (!TextUtils.isEmpty(edtFirstName.getText().toString()))
        {
            if (!TextUtils.isEmpty(edtLastName.getText().toString()))
            {
                profileUpdateRequest(edtFirstName.getText().toString(),edtLastName.getText().toString());

            } else
                Utility.ShowToast(mContext, getResources().getString(R.string.last_name_validation));
        } else
            Utility.ShowToast(mContext, getResources().getString(R.string.first_name_validation));
    }

    private void profileUpdateRequest(String firstName, String lastName) {

       /* location
                playinId
        lastModifiedBy
                isMobileVerified
        isEmailVerified
                googleUserId
        facebookUserId
                tempPassword
        tempExpiryTime
                isTempPassword
        imagePath*/


        ArrayList<DataModel> list = new ArrayList<DataModel>();
        DataModel dmFirstName = new DataModel();
        dmFirstName.setKey("firstName");
        dmFirstName.setValue(firstName);

        DataModel dmLastName = new DataModel();
        dmLastName.setKey("lastName");
        dmLastName.setValue(lastName);

        DataModel dmCountryCode = new DataModel();
        dmCountryCode.setKey("countryCode");
        dmCountryCode.setValue("+91");

        DataModel dmMobileNumber = new DataModel();
        dmMobileNumber.setKey("mobile");
        dmMobileNumber.setValue("9999999999");

        DataModel dmEmail = new DataModel();
        dmEmail.setKey("email");
        dmEmail.setValue("playin@gmail.com");

        DataModel dmTimeStampDOB = new DataModel();
        dmTimeStampDOB.setKey("timestampOfBirth");
        dmTimeStampDOB.setValue(edDateOfBirth.getText().toString());

        DataModel dmGender = new DataModel();
        dmGender.setKey("gender");
        dmGender.setValue(strGender);

        DataModel dmStatusID = new DataModel();
        dmStatusID.setKey("statusId");
        dmStatusID.setValue("1");


        list.add(dmFirstName);
        list.add(dmLastName);
        list.add(dmCountryCode);
        list.add(dmMobileNumber);
        list.add(dmEmail);
        list.add(dmTimeStampDOB);
        list.add(dmGender);
        list.add(dmStatusID);

        MainController controller = new MainController(getActivity(), this, Constants.UPDATE_USER_DETAILS, true);
        controller.RequestService(list, Constants.BASE_URL + Constants.UPDATE_USER_DETAILS_API, Constants.REQUEST_TYPE_POST, Constants.HEADER_TYPE_PLAY_IN_TOKEN);
    }


    @Override
    public void setScreenData(Hashtable obj, byte type, String Responce) {
        switch (type) {
            case Constants.UPDATE_USER_DETAILS:

                Hashtable<String, Object> result = (Hashtable<String, Object>) obj;
                ResponseInfo responseInfo = (ResponseInfo) obj.get("responseInfo");
                if (responseInfo != null && responseInfo.getMessage() != null)
                    Utility.ShowToast(mContext, responseInfo.getMessage());

                Intent intentDashboard = new Intent(mContext, HomeActivity.class);
                startActivity(intentDashboard);
                getActivity().finish();
                break;

            case Constants.FORGOT_PASSWORD:

                break;

        }
    }

    @Override
    public void setScreenData(String obj) {

    }

    @Override
    public void setScreenMessage(String obj, byte type) {

    }

    @Override
    public void setCancelMessage(String obj, byte type) {

    }

    private void setDeviceSize() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        mHeight = displaymetrics.heightPixels;
        mWidth = displaymetrics.widthPixels;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int height) {
        mHeight = height;
    }

    public int getWidth() {
        return mWidth;
    }

    public void setWidth(int width) {
        mWidth = width;
    }
}
