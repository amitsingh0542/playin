package playin.orgname.com.playin.model;

import java.io.Serializable;

/**
 * Created by Ashutosh on 7/3/2016.
 */
public class EventDetailsModel implements Serializable {

    private String eventId = null;
    private String title = null;
    private String description = null;
    private int sportId = 0;
    private String playinId = null;
    private String creatorName =null;
    private int entityType = 0;
    private long eventStartDate = 0;
    private long eventEndDate = 0;
    private String preferredPlayers = null;
    private boolean isTeamEvent = false;
    private int numberOfPlayersRequired = 0;
    private int numberOfPlayersJoined = 0;
    private EventDetailsLocationModel location=null;
    private boolean isCreatorAParticipant = false;
    private String imagePath = null;
    private boolean needsCreatorPermissionToJoinEvent = false;
    private int joinStatus = 0;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSportId() {
        return sportId;
    }

    public void setSportId(int sportId) {
        this.sportId = sportId;
    }

    public String getPlayinId() {
        return playinId;
    }

    public void setPlayinId(String playinId) {
        this.playinId = playinId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public int getEntityType() {
        return entityType;
    }

    public void setEntityType(int entityType) {
        this.entityType = entityType;
    }

    public long getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(long eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public long getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(long eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public String getPreferredPlayers() {
        return preferredPlayers;
    }

    public void setPreferredPlayers(String preferredPlayers) {
        this.preferredPlayers = preferredPlayers;
    }

    public boolean isTeamEvent() {
        return isTeamEvent;
    }

    public void setIsTeamEvent(boolean isTeamEvent) {
        this.isTeamEvent = isTeamEvent;
    }

    public int getNumberOfPlayersRequired() {
        return numberOfPlayersRequired;
    }

    public void setNumberOfPlayersRequired(int numberOfPlayersRequired) {
        this.numberOfPlayersRequired = numberOfPlayersRequired;
    }

    public int getNumberOfPlayersJoined() {
        return numberOfPlayersJoined;
    }

    public void setNumberOfPlayersJoined(int numberOfPlayersJoined) {
        this.numberOfPlayersJoined = numberOfPlayersJoined;
    }

    public EventDetailsLocationModel getLocation() {
        return location;
    }

    public void setLocation(EventDetailsLocationModel location) {
        this.location = location;
    }

    public boolean isCreatorAParticipant() {
        return isCreatorAParticipant;
    }

    public void setIsCreatorAParticipant(boolean isCreatorAParticipant) {
        this.isCreatorAParticipant = isCreatorAParticipant;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public boolean isNeedsCreatorPermissionToJoinEvent() {
        return needsCreatorPermissionToJoinEvent;
    }

    public void setNeedsCreatorPermissionToJoinEvent(boolean needsCreatorPermissionToJoinEvent) {
        this.needsCreatorPermissionToJoinEvent = needsCreatorPermissionToJoinEvent;
    }

    public int getJoinStatus() {
        return joinStatus;
    }

    public void setJoinStatus(int joinStatus) {
        this.joinStatus = joinStatus;
    }
}
