package playin.orgname.com.playin.model;

import java.io.Serializable;

/**
 * Created by Ashutosh on 7/3/2016.
 */
public class EventDetailsLocationModel implements Serializable {


    private int locationId = 0;
    private double latitude = 0.0;
    private double longitude = 0.0;
    private String geoCodeAddress = null;
    private String userEnteredAddress = null;
    private String shortAddress = null;
    private String area = null;
    private String city = null;
    private boolean isEventLocation = false;
    private String locationName = null;

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getGeoCodeAddress() {
        return geoCodeAddress;
    }

    public void setGeoCodeAddress(String geoCodeAddress) {
        this.geoCodeAddress = geoCodeAddress;
    }

    public String getUserEnteredAddress() {
        return userEnteredAddress;
    }

    public void setUserEnteredAddress(String userEnteredAddress) {
        this.userEnteredAddress = userEnteredAddress;
    }

    public String getShortAddress() {
        return shortAddress;
    }

    public void setShortAddress(String shortAddress) {
        this.shortAddress = shortAddress;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isEventLocation() {
        return isEventLocation;
    }

    public void setIsEventLocation(boolean isEventLocation) {
        this.isEventLocation = isEventLocation;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
}
