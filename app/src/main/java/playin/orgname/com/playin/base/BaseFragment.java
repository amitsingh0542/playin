package playin.orgname.com.playin.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;


public class BaseFragment extends Fragment {

    public BaseFragment() {
        super();
    }

    private static final String TAG = BaseFragment.class.getSimpleName();

    public Activity mActivity;
    protected View mView;
    protected FragmentManager mFragmentManager;

    /**
     * Key for the title that will be shown on the action bar when this fragment
     * is displayed
     */
    public static final String FRAGMENT_TITLE = "fragment_title";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
    }
}
