package playin.orgname.com.playin.model;

import java.io.Serializable;

/**
 * Created by Ashutosh on 7/3/2016.
 */
public class MyTeamsModel implements Serializable {
    private int teamId = 0;
    private String name = null;
    private int numberOfPlayersRequired = 0;
    private int numberOfPlayersJoined = 0;
    private String sport = null;
    private String CreatedBy = null;
    private String creatorPlayinId = null;
    private String creatorImage = null;

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfPlayersRequired() {
        return numberOfPlayersRequired;
    }

    public void setNumberOfPlayersRequired(int numberOfPlayersRequired) {
        this.numberOfPlayersRequired = numberOfPlayersRequired;
    }

    public int getNumberOfPlayersJoined() {
        return numberOfPlayersJoined;
    }

    public void setNumberOfPlayersJoined(int numberOfPlayersJoined) {
        this.numberOfPlayersJoined = numberOfPlayersJoined;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatorPlayinId() {
        return creatorPlayinId;
    }

    public void setCreatorPlayinId(String creatorPlayinId) {
        this.creatorPlayinId = creatorPlayinId;
    }

    public String getCreatorImage() {
        return creatorImage;
    }

    public void setCreatorImage(String creatorImage) {
        this.creatorImage = creatorImage;
    }


}
