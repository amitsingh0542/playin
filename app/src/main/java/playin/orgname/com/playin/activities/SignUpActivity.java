package playin.orgname.com.playin.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Hashtable;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.base.BaseActivity;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.DataModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.model.SignUpModel;
import playin.orgname.com.playin.preference.PlayInSharedPrefKeys;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;

public class SignUpActivity extends BaseActivity implements OnClickListener, DisplableInterface
{
	public static final String TAG = "SignUpActivity";
	private Context mContext;
	private Button btnSignUp;
	private EditText edEmail,edPassword, edConfirmPassword;
	private TextView tvHeader;
	private ImageView imgPrevious;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		//getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_signup);
		mContext=getApplicationContext();
		initViews();
		initListener();
	}

	private void initViews() 
	{
		edEmail = (EditText)findViewById(R.id.edt_email);
		edPassword = (EditText)findViewById(R.id.edt_password);
		edConfirmPassword = (EditText)findViewById(R.id.edt_password_confirm);
		btnSignUp=(Button)findViewById(R.id.btn_signup);
		tvHeader= (TextView) findViewById(R.id.tv_header_topbar);
		imgPrevious=(ImageView)findViewById(R.id.img_previous);

		if (PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE).equalsIgnoreCase(Constants.USER_TYPE_INDIVIDUAL)) {
			tvHeader.setText(Constants.USER_TYPE_INDIVIDUAL+ "  "+"SignUp");
		} else if (PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE).equalsIgnoreCase(Constants.USER_TYPE_ORGANIZATION)) {
			tvHeader.setText(Constants.USER_TYPE_ORGANIZATION +"  "+"SignUp");
		}
	}
	
	private void initListener() 
	{
		btnSignUp.setOnClickListener(this);
		imgPrevious.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
			case R.id.btn_signup:
				//registerPhoneNumberThroughOtp();
				getSignUp();
				break;
			case R.id.img_previous:
				finish();
				break;
			default:
				break;
			
		}
	}

	private void getSignUp()
	{
		if(!TextUtils.isEmpty(edEmail.getText().toString())){
			if(Utility.isValidEmail(edEmail.getText().toString())){


						if(!TextUtils.isEmpty(edPassword.getText().toString())){
							if(edPassword.getText().toString().equals(edConfirmPassword.getText().toString()))
							{
								registrationRequest(edEmail.getText().toString(), edPassword.getText().toString());
							}
							else
								Utility.ShowToast(mContext, Constants.PASSWORD_MISMATCH);
						}
						else
							Utility.ShowToast(mContext, Constants.PLEASE_ENTER_PASSWORD);
			}
			else
				Utility.ShowToast(mContext, Constants.INVALID_EMAIL);
		}
		else
			Utility.ShowToast(mContext, Constants.PLEASE_ENTER_EMAIL);
	}

	private void registerPhoneNumberThroughOtp()
	{
		try
		{
			final Dialog dialogRegisterPhoneNumber = new Dialog(SignUpActivity.this);
			dialogRegisterPhoneNumber.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialogRegisterPhoneNumber.setContentView(R.layout.dialog_register_phone_number);
			//dialogForgetPassword.setTitle("Forgot Password");
			Button btnSendTemporaryPassword=(Button)dialogRegisterPhoneNumber.findViewById(R.id.btn_getotp_mobile_number_registration);

			final EditText edtMobileNumberRegistrsation=(EditText)dialogRegisterPhoneNumber.findViewById(R.id.edt_mobile_number_registrsation);

			edtMobileNumberRegistrsation.addTextChangedListener(new TextWatcher() {

				public void afterTextChanged(Editable s) {

				}

				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if (!TextUtils.isEmpty(s)) {

						if (s.toString().equalsIgnoreCase("0")) {
							Utility.ShowToast(mContext, Constants.RULE_VALID_MOBILE_NUMBER);
							edtMobileNumberRegistrsation.setText("");
							edtMobileNumberRegistrsation.setFocusable(true);
						}
					} else {

					}
				}
			});

			btnSendTemporaryPassword.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v)
				{
					if(!TextUtils.isEmpty(edtMobileNumberRegistrsation.getText()))
					{
						if(edtMobileNumberRegistrsation.getText().length()==10)
						{
							Intent intentIndividual = new Intent(mContext, VerifyPhoneNumberThroughOtpActivity.class);
							startActivity(intentIndividual);
							dialogRegisterPhoneNumber.dismiss();
							finish();
						}
						else
						{
							Utility.ShowToast(mContext, Constants.RULE_VALID_MOBILE_NUMBER);
						}
					}
					else {
						Utility.ShowToast(mContext, Constants.PLEASE_ENTER_MOBILE_NUMBER);
					}
				}
			});
			dialogRegisterPhoneNumber.show();
			//Toast.makeText(mContext, "Login with Google is Successfull!!!!", Toast.LENGTH_SHORT).show();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	// Registration request
	public void registrationRequest(String email, String password)
	{

		ArrayList<DataModel> list = new ArrayList<DataModel>();
		DataModel eMailModel = new DataModel();
		eMailModel.setKey("email");
		eMailModel.setValue(email);

		DataModel passwordModel = new DataModel();
		passwordModel.setKey("password");
		passwordModel.setValue(password);

		DataModel deviceIdentifier = new DataModel();
		deviceIdentifier.setKey("deviceIdentifier");
		deviceIdentifier.setValue("Android");


		list.add(eMailModel);
		list.add(passwordModel);
		list.add(deviceIdentifier);

		MainController controller = new MainController(SignUpActivity.this, this, Constants.REGISTER, true);
		if (PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE).equalsIgnoreCase(Constants.USER_TYPE_INDIVIDUAL))
		{
			controller.RequestService(list, Constants.BASE_URL + Constants.REGISTRATION_METHOD, Constants.REQUEST_TYPE_POST, Constants.HEADER_TYPE_NORMAL);
		} else if (PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_USER_TYPE).equalsIgnoreCase(Constants.USER_TYPE_ORGANIZATION)) {
			controller.RequestService(list, Constants.BASE_URL + Constants.REGISTRATION_ORGANIZATION_METHOD, Constants.REQUEST_TYPE_POST, Constants.HEADER_TYPE_NORMAL);
		}


		//controller.PostMethod(Constants.BASE_URL + Constants.REGISTRATION_METHOD, list);

		}



	@Override
	public void setScreenData(Hashtable obj, byte type, String Responce) {
		switch(type){
			case Constants.REGISTER:
				Hashtable<String, Object> result = (Hashtable<String, Object>)obj;
				ArrayList<SignUpModel> list = (ArrayList<SignUpModel>)obj.get("data");
				if(list != null && list.size() > 0) {
					SignUpModel model = list.get(0);
					registerPhoneNumberThroughOtp();
				//	Utility.ShowToast(getApplicationContext(), model.getMessage());

				}

				ResponseInfo responseInfo = (ResponseInfo) obj.get("responseInfo");
				if(responseInfo != null && responseInfo.getMessage() != null)
				Utility.ShowToast(getApplicationContext(), responseInfo.getMessage());

				break;
		}
	}

	@Override
	public void setScreenData(String obj) {

	}

	@Override
	public void setScreenMessage(String obj, byte type) {

	}

	@Override
	public void setCancelMessage(String obj, byte type) {

	}
}
