package playin.orgname.com.playin.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Switch;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.base.BaseActivity;
import playin.orgname.com.playin.bootstrap.PlayInLogManager;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.customview.RoundedImageView;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.DataModel;
import playin.orgname.com.playin.model.LoginModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.model.SportsModel;
import playin.orgname.com.playin.preference.PlayInSharedPrefKeys;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;

public class CreateTeamsActivity extends BaseActivity implements OnClickListener, DisplableInterface {
    public static final String TAG = "CreateTeamsActivity";
    private Context mContext;
    private Button btnCreateEvent;

    private TextView tvEventVenue;
    private EditText edTeamName, edNumberOfPlayers, edNumberOfStandByPlayers;
    private AutoCompleteTextView actvSports;
    private CheckBox cbMale, cbFemale;
    private Switch switchPermission, switchParticipating;
    private final int CAMERA_CODE = 1;
    private final int GALLERY_CODE = 2;
    private PopupWindow pwindo;
    private RoundedImageView imgTeamIcon;
    private TextView tvHeaderTitle;
    private ImageView imgPrevious;



    private int sportsId = -1;
    private String[] arrName;
    int[] arrId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_teams);
        mContext = getApplicationContext();
        initViews();
        initListener();
        initData();
    }

    private void initViews() {


        imgPrevious = (ImageView) findViewById(R.id.img_previous);
        tvHeaderTitle = (TextView) findViewById(R.id.tv_header_topbar);
        tvHeaderTitle.setText("Create Teams");

        imgTeamIcon = (RoundedImageView) findViewById(R.id.img_teams_icon);
        edTeamName = (EditText) findViewById(R.id.edt_teams_name_teams);

        edNumberOfPlayers = (EditText) findViewById(R.id.ed_number_of_players_teams);
        edNumberOfStandByPlayers = (EditText) findViewById(R.id.ed_number_of_standby_players_teams);

        cbMale = (CheckBox) findViewById(R.id.cb_male_teams);
        cbFemale = (CheckBox) findViewById(R.id.cb_female_teams);
        switchPermission = (Switch) findViewById(R.id.switch_permission_teams);
        switchParticipating = (Switch) findViewById(R.id.switch_participating_teams);
        actvSports = (AutoCompleteTextView) findViewById(R.id.actv_choose_sports_teams);
        btnCreateEvent = (Button) findViewById(R.id.btn_create_teams);
    }

    private void initListener() {
        imgPrevious.setOnClickListener(this);
        imgTeamIcon.setOnClickListener(this);

        btnCreateEvent.setOnClickListener(this);
        actvSports.setOnClickListener(this);
        cbMale.setOnClickListener(this);
        cbFemale.setOnClickListener(this);

        switchPermission.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                } else {
                }
            }
        });

        switchParticipating.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                } else {
                }
            }
        });
    }


    private void initData() {
        getAllSports();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.btn_create_teams:
                createTeams();
                break;

            case R.id.cb_male_teams:

                break;
            case R.id.cb_female_teams:

                break;

            case R.id.img_previous:
                finish();

            case R.id.img_teams_icon:
                if (pwindo != null) {
                    if (pwindo.isShowing())
                        pwindo.dismiss();
                }
                openPopup(v);
                break;

            case R.id.popup_parent:
                if (pwindo != null) {
                    if (pwindo.isShowing())
                        pwindo.dismiss();
                }
                break;

            case R.id.ll_camera:
                openCamera();
                break;

            case R.id.ll_gallery:
                openGallery();
                break;

        }
    }


    private void createTeams() {
        if (!TextUtils.isEmpty(edTeamName.getText().toString())) {

            if (!TextUtils.isEmpty(actvSports.getText().toString().trim())) {

                if (!TextUtils.isEmpty(edNumberOfPlayers.getText().toString().trim())) {

                    requestCreateTeams();
                } else
                    Utility.ShowToast(mContext, getResources().getString(R.string.str_event_alert_number_of_players));

            } else
                Utility.ShowToast(mContext, getResources().getString(R.string.str_event_alert_sports_name));

        } else
            Utility.ShowToast(mContext, getResources().getString(R.string.str_event_alert_title));
    }

    private void requestCreateTeams() {

        ArrayList<DataModel> list = new ArrayList<DataModel>();

        DataModel dmTeamName = new DataModel();
        dmTeamName.setKey("name");
        dmTeamName.setValue(edTeamName.getText().toString());

        DataModel dmTeamNumberOfPlayersRequired = new DataModel();
        dmTeamNumberOfPlayersRequired.setKey("numberOfPlayersRequired");
        dmTeamNumberOfPlayersRequired.setValue(String.valueOf(edNumberOfPlayers.getText()));

        DataModel dmPreferredPlayers = new DataModel();
        dmPreferredPlayers.setKey("preferredPlayers");
        if (cbMale.isChecked() && cbFemale.isChecked())
            dmPreferredPlayers.setValue("3");
        if (cbFemale.isChecked())
            dmPreferredPlayers.setValue("1");
        if (cbMale.isChecked())
            dmPreferredPlayers.setValue("2");

        DataModel dmTeamStandByPlayers = new DataModel();
        dmTeamStandByPlayers.setKey("standbyPlayers");
        dmTeamStandByPlayers.setValue(String.valueOf(edNumberOfStandByPlayers.getText()));

        DataModel dmTeamsCreatedByPlayinId = new DataModel();
        dmTeamsCreatedByPlayinId.setKey("createdByPlayinId");
        dmTeamsCreatedByPlayinId.setValue(PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_PLAYIN_ID));

        DataModel dmTeamCreatedByEntityType = new DataModel();
        dmTeamCreatedByEntityType.setKey("createdByentityType");
        dmTeamCreatedByEntityType.setValue("1");

        DataModel dmTeamSportsId = new DataModel();
        dmTeamSportsId.setKey("sportId");
        dmTeamSportsId.setValue("2");//Currently Static

        DataModel dmTeamPreferredLocation = new DataModel();
        dmTeamPreferredLocation.setKey("preferredLocation");
        dmTeamPreferredLocation.setValue(setLocationParameter());//Currently Static

        DataModel dmTeamsNeedsCreatorPermissionToJoinEvent = new DataModel();
        dmTeamsNeedsCreatorPermissionToJoinEvent.setKey("needsCreatorPermissionToJoin");
        dmTeamsNeedsCreatorPermissionToJoinEvent.setValue(String.valueOf(switchPermission.isChecked()));

        DataModel dmTeamsIsCreatorAParticipant = new DataModel();
        dmTeamsIsCreatorAParticipant.setKey("isCreatorATeamMember");
        dmTeamsIsCreatorAParticipant.setValue(String.valueOf(switchParticipating.isChecked()));

        list.add(dmTeamName);
        list.add(dmTeamNumberOfPlayersRequired);
        list.add(dmPreferredPlayers);
        list.add(dmTeamStandByPlayers);
        list.add(dmTeamsCreatedByPlayinId);
        list.add(dmTeamCreatedByEntityType);
        list.add(dmTeamSportsId);
        list.add(dmTeamPreferredLocation);
        list.add(dmTeamsNeedsCreatorPermissionToJoinEvent);
        list.add(dmTeamsIsCreatorAParticipant);


        MainController controller = new MainController(CreateTeamsActivity.this, this, Constants.CREATE_TEAM, true);
        controller.RequestService(list, Constants.BASE_URL + Constants.CREATE_TEAMS_API, Constants.REQUEST_TYPE_POST, Constants.HEADER_TYPE_PLAY_IN_TOKEN);
    }


    private String setLocationParameter() {
        ArrayList<DataModel> listSubLocation = new ArrayList<DataModel>();

        DataModel dmEventSubLocationLatitude = new DataModel();
        dmEventSubLocationLatitude.setKey("latitude");
        dmEventSubLocationLatitude.setValue("12.378");//Currently Static

        DataModel dmEventSubLocationLongitude = new DataModel();
        dmEventSubLocationLongitude.setKey("longitude");
        dmEventSubLocationLongitude.setValue("13.78786");//Currently Static

        DataModel dmEventSubLocationGeoCodeAddress = new DataModel();
        dmEventSubLocationGeoCodeAddress.setKey("geoCodeAddress");
        dmEventSubLocationGeoCodeAddress.setValue("agsdhgaksas");//Currently Static

        DataModel dmEventSubLocationUserEnteredAddress = new DataModel();
        dmEventSubLocationUserEnteredAddress.setKey("userEnteredAddress");
        dmEventSubLocationUserEnteredAddress.setValue("");//Currently Static

        DataModel dmEventSubLocationArea = new DataModel();
        dmEventSubLocationArea.setKey("area");
        dmEventSubLocationArea.setValue("Airoli");//Currently Static

        DataModel dmEventSubLocationGeoCity = new DataModel();
        dmEventSubLocationGeoCity.setKey("city");
        dmEventSubLocationGeoCity.setValue("Mumbai");//Currently Static

        DataModel dmEventSubLocationIsEventLocations = new DataModel();
        dmEventSubLocationIsEventLocations.setKey("isEventLocation");
        dmEventSubLocationIsEventLocations.setValue("true");//Currently Static

        listSubLocation.add(dmEventSubLocationLatitude);
        listSubLocation.add(dmEventSubLocationLongitude);
        listSubLocation.add(dmEventSubLocationGeoCodeAddress);
        listSubLocation.add(dmEventSubLocationUserEnteredAddress);
        listSubLocation.add(dmEventSubLocationArea);
        listSubLocation.add(dmEventSubLocationGeoCity);
        listSubLocation.add(dmEventSubLocationIsEventLocations);

        String requestResult = Utility.getJsonRequest(listSubLocation);
        requestResult = requestResult.replaceAll("'\'", "");
        //requestResult = requestResult.substring(1, requestResult.length() - 1);
        PlayInLogManager.v(TAG, requestResult);

        return requestResult;
    }

    @Override
    public void setScreenData(Hashtable obj, byte type, String Responce) {
        switch (type) {
            case Constants.CREATE_TEAM:
                Hashtable<String, Object> result = (Hashtable<String, Object>) obj;
                ArrayList<LoginModel> list = (ArrayList<LoginModel>) obj.get("data");
                if (list != null && list.size() > 0) {
                    LoginModel model = list.get(0);
                    //	Utility.ShowToast(getApplicationContext(), model.getMessage());

                    PlayInSharedPrefUtils.getInstance(mContext).saveStringPrefernce(PlayInSharedPrefKeys.KEY_PLAYIN_ID, model.getPlayinId());
                    showCreateEventSuccessDialog("Ashutosh");
                    /*Intent intentProfile = new Intent(mContext, ProfileActivity.class);
                    startActivity(intentProfile);
                    finish();*/
                }

                ResponseInfo responseInfo = (ResponseInfo) obj.get("responseInfo");
                if (responseInfo != null && responseInfo.getMessage() != null) {
                    if (!TextUtils.isEmpty(responseInfo.getMessage()))
                        Utility.ShowToast(mContext, responseInfo.getMessage());
                }

                break;

            case Constants.GET_ALL_SPORTS:
                Hashtable<String, Object> resultAllSports = (Hashtable<String, Object>) obj;
                ArrayList<SportsModel> listAllSports = (ArrayList<SportsModel>) obj.get("data");
                if (listAllSports != null && listAllSports.size() > 0) {
                    SportsModel model = listAllSports.get(0);
                    arrName = new String[listAllSports.size()];
                    arrId = new int[listAllSports.size()];
                    for(int i = 0; i<listAllSports.size(); i++){
                        arrName[i] = listAllSports.get(i).getName();
                        arrId[i] = listAllSports.get(i).getSportId();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrName);
                    //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, ProgLanguages);

                    //Getting the instance of AutoCompleteTextView
                    actvSports.setThreshold(1);//will start working from first character
                    actvSports.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                }

                ResponseInfo responseInfoAllSports = (ResponseInfo) obj.get("responseInfo");
                if (responseInfoAllSports != null && responseInfoAllSports.getMessage() != null) {
                    if (!TextUtils.isEmpty(responseInfoAllSports.getMessage()))
                        Utility.ShowToast(mContext, responseInfoAllSports.getMessage());
                }

                break;
        }
    }

    @Override
    public void setScreenData(String obj) {

    }

    @Override
    public void setScreenMessage(String obj, byte type) {

    }

    @Override
    public void setCancelMessage(String obj, byte type) {

    }


    private void showCreateEventSuccessDialog(String createName) {
        try {
            //tvHeader.setText("Forgot Password");
            final Dialog dialog = new Dialog(CreateTeamsActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_server_response);
            Button btnOkay = (Button) dialog.findViewById(R.id.btn_ok_response_dialog);
            TextView tvResponse = (TextView) dialog.findViewById(R.id.tv_response_details);
            TextView tvClose = (TextView) dialog.findViewById(R.id.tv_close);

            btnOkay.setText("Invite");
            tvResponse.setText("You have successfully created your event Event from  " + createName + " Start Inviting the players to your Event");
            btnOkay.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            tvClose.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
            //Toast.makeText(mContext, "Login with Google is Successfull!!!!", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_CODE) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    Bitmap bitmap;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                            bitmapOptions);

                    imgTeamIcon.setImageBitmap(bitmap);

                    String path = Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + "Phoenix" + File.separator + "default";
                    f.delete();
                    OutputStream outFile = null;
                    File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
                    try {
                        outFile = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        outFile.flush();
                        outFile.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == GALLERY_CODE) {

                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                //Log.w("path of image from gallery......******************.........", picturePath + "");
                imgTeamIcon.setImageBitmap(thumbnail);
            }
        }
    }

    public void getAllSports() {
        MainController controller = new MainController(CreateTeamsActivity.this, this, Constants.GET_ALL_SPORTS, true);
        String finalUrl = Constants.BASE_URL + Constants.GET_ALL_SPORTS_API;
        controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN_TOKEN);
    }

    private void openCamera() {
        pwindo.dismiss();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(intent, CAMERA_CODE);
    }

    private void openGallery() {
        pwindo.dismiss();
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_CODE);
    }

    private void openPopup(View anchor) {
        try {
            LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.screen_popup, (ViewGroup) findViewById(R.id.popup_parent));
            pwindo = new PopupWindow(layout, getWidth(), getHeight(), true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
            pwindo.setOutsideTouchable(true);
            // pwindo.setFocusable(true);
            pwindo.setBackgroundDrawable(new BitmapDrawable(getResources(), Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)));
            layout.setOnClickListener(this);
            pwindo.showAsDropDown(anchor, 0, 0);
            layout.findViewById(R.id.ll_gallery).setOnClickListener(this);
            layout.findViewById(R.id.ll_camera).setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
