package playin.orgname.com.playin.adapters;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.model.EventForMeModel;


public class EventForMeAdapter extends BaseListAdapter {

	protected static final String TAG = "EventForMeAdapter";
	private LayoutInflater mInflater;
    private ArrayList<EventForMeModel> listEventForMe;
    private String[] sss;
    public EventForMeAdapter(Context context, ArrayList<EventForMeModel> eventForMe)
	{
		 mInflater = LayoutInflater.from(context);
	     this.listEventForMe = eventForMe;
	}
	@Override
	public int getCount() 
	{
		if (listEventForMe != null)
			return listEventForMe.size();
		else
			return 0;
	}

	@Override
	public Object getItem(int position) 
	{
		return listEventForMe.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		Holder viewHolder = new Holder();
		if(convertView == null)
		{
    		convertView = mInflater.inflate(R.layout.list_item_event_for_me, null);

			viewHolder.tvEventDate = (TextView) convertView.findViewById(R.id.tv_tournament_date_myevent);
			viewHolder.tvEventMonth=(TextView)convertView.findViewById(R.id.tv_tournament_month_myevent);
			viewHolder.tvEventYear=(TextView)convertView.findViewById(R.id.tv_tournament_year_myevent);

    		viewHolder.tvTournamentName = (TextView) convertView.findViewById(R.id.tv_tournament_name_myevent);
			viewHolder.tvTournamentTime=(TextView)convertView.findViewById(R.id.tv_tournament_time_myevent);
			viewHolder.imgSportsName=(TextView)convertView.findViewById(R.id.tv_sports_name_myevent);
			viewHolder.tvTournamentLocation = (TextView) convertView.findViewById(R.id.tv_location_myevent);
    		convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder = (Holder) convertView.getTag();
    	}

		/* {
      "eventId": 182,
      "title": "Estuate fun event",
      "eventStartDate": 1467977220,
      "sportId": 5,
      "latitude": 13.1048145,
      "longitude": 77.58082,
      "area": "Yelahanka New Town",
      "city": "Bengaluru"
    }*/

		//String longV = "1343805819061";
		String longV =listEventForMe.get(position).getEventStartDate();
		System.out.println("Date Long Value:>>>"+longV);
		long millisecond = Long.parseLong(longV);
		String dateString= DateFormat.format("MM/dd/yyyy", new Date(millisecond)).toString();
		System.out.println("Date String:>>>"+dateString);
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		try {
			Date date = format.parse(dateString);
			System.out.println("Date:>>>"+date);
			String day = (String) DateFormat.format("dd", date);
			String intMonth = (String) DateFormat.format("MM", date); //06
			String year = (String) DateFormat.format("yyyy", date); //2013
			viewHolder.tvEventDate.setText(day);
			viewHolder.tvEventMonth.setText(intMonth);
			viewHolder.tvEventYear.setText(year);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		viewHolder.tvTournamentName.setText(listEventForMe.get(position).getTitle());
		viewHolder.tvTournamentTime.setText("9:30 PM");

		viewHolder.imgSportsName.setText("Carrom");
		viewHolder.tvTournamentLocation.setText(listEventForMe.get(position).getCity());
		return convertView;
	}
	
	private class Holder 
	{
		TextView tvEventDate;
		TextView tvEventMonth;
		TextView tvEventYear;

		TextView tvTournamentName;
		TextView tvTournamentTime;
		TextView imgSportsName;
		TextView tvTournamentLocation;
    }
}
