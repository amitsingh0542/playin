package playin.orgname.com.playin.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Switch;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;

import playin.orgname.com.playin.R;
import playin.orgname.com.playin.Utility.Utility;
import playin.orgname.com.playin.base.BaseActivity;
import playin.orgname.com.playin.bootstrap.PlayInLogManager;
import playin.orgname.com.playin.constants.Constants;
import playin.orgname.com.playin.controller.MainController;
import playin.orgname.com.playin.customview.RoundedImageView;
import playin.orgname.com.playin.interfaces.DisplableInterface;
import playin.orgname.com.playin.model.DataModel;
import playin.orgname.com.playin.model.EventDetailsModel;
import playin.orgname.com.playin.model.LoginModel;
import playin.orgname.com.playin.model.ResponseInfo;
import playin.orgname.com.playin.model.SportsModel;
import playin.orgname.com.playin.preference.PlayInSharedPrefKeys;
import playin.orgname.com.playin.preference.PlayInSharedPrefUtils;

public class EditEventActivity extends BaseActivity implements OnClickListener, DisplableInterface {
    public static final String TAG = "EditEventActivity";
    private Context mContext;
    private Button btnUpdateEvent;

    private TextView tvEventVenue;
    private EditText edTitle,edEventStartDate,edEventEndDate,edEventDescription,edNumberOfPlayers;
    private Switch switchPermission, switchParticipating;
    private AutoCompleteTextView actvSports;
    private CheckBox cbMale, cbFemale;


    private final int CAMERA_CODE=1;
    private final int GALLERY_CODE=2;
    private PopupWindow pwindo;
    private RoundedImageView imgEventEditIcon;
    private TextView tvHeaderTitle;
    private ImageView imgPrevious;
    //private EventDetailsModel myEventModel = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_event);
        mContext = getApplicationContext();
        initViews();
        initListener();
        initData();
    }

    private void initViews() {

        imgPrevious = (ImageView) findViewById(R.id.img_previous);
        tvHeaderTitle = (TextView) findViewById(R.id.tv_header_topbar);
        tvHeaderTitle.setText("Edit Event");
        imgEventEditIcon = (RoundedImageView) findViewById(R.id.img_event_edit_icon);
        edTitle = (EditText) findViewById(R.id.edt_title_event_edit);
        edEventStartDate = (EditText) findViewById(R.id.edt_start_date_event_edit);
        edEventEndDate = (EditText) findViewById(R.id.edt_end_date_event_edit);
        edEventDescription = (EditText) findViewById(R.id.ed_event_description_edit);
        edNumberOfPlayers = (EditText) findViewById(R.id.ed_number_of_players_edit);
        switchPermission = (Switch) findViewById(R.id.switch_permission_event_edit);
        switchParticipating = (Switch) findViewById(R.id.switch_participating_event_edit);
        actvSports = (AutoCompleteTextView) findViewById(R.id.actv_choose_sports_event_edit);

        cbMale = (CheckBox) findViewById(R.id.cb_male_edit);
        cbFemale = (CheckBox) findViewById(R.id.cb_female_edit);

        btnUpdateEvent = (Button) findViewById(R.id.btn_update_event_edit);


    }

    private void initListener() {
        imgPrevious.setOnClickListener(this);
        edEventStartDate.setOnClickListener(this);
        edEventEndDate.setOnClickListener(this);
        actvSports.setOnClickListener(this);
        btnUpdateEvent.setOnClickListener(this);
        imgEventEditIcon.setOnClickListener(this);


        cbMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
              {

                  @Override
                  public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                      if (isChecked) {
                      } else {
                      }
                  }
              }
        );
        cbFemale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                    } else {
                    }
                }
            }
        );

        switchPermission.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                } else {
                }
            }
        });

        switchParticipating.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                } else {
                }
            }
        });
    }

    private void initData() {
       setEventData();
    }

    private void setEventData() {
        Intent intentMyEvent = getIntent();
        EventDetailsModel myEventModel = (EventDetailsModel) intentMyEvent.getSerializableExtra("MY_EVENT");
        if (myEventModel != null) {

            //tvEventVenue.setText(myEventModel);
            if (!TextUtils.isEmpty(myEventModel.getTitle()))
                edTitle.setText(myEventModel.getTitle());

           /* if(!TextUtils.isEmpty(String.valueOf(myEventModel.getSportId())))
                edSports.setText(myEventModel.getSportId());*/

            edEventStartDate.setText(Utility.getDateFromLong(myEventModel.getEventStartDate()));
            edEventEndDate.setText(Utility.getDateFromLong(myEventModel.getEventEndDate()));

            if (!TextUtils.isEmpty(myEventModel.getDescription()))
                edEventDescription.setText(myEventModel.getDescription());

            if(myEventModel.getNumberOfPlayersRequired()>0) {
                edNumberOfPlayers.setText(""+myEventModel.getNumberOfPlayersRequired());
            }
            else {
                edNumberOfPlayers.setText("0");
            }
            //actvSports

            if (myEventModel.getPreferredPlayers().equalsIgnoreCase("1"))//Female
            {
                cbFemale.setChecked(true);
                cbMale.setChecked(false);
            } else if (myEventModel.getPreferredPlayers().equalsIgnoreCase("2"))//male
            {
                cbMale.setChecked(true);
                cbFemale.setChecked(false);
            } else if (myEventModel.getPreferredPlayers().equalsIgnoreCase("3"))//both
            {
                cbMale.setChecked(true);
                cbFemale.setChecked(true);
            } else {
                cbMale.setChecked(false);
                cbFemale.setChecked(false);
            }
            if (myEventModel.isNeedsCreatorPermissionToJoinEvent())
                switchPermission.setChecked(true);
            if (myEventModel.isCreatorAParticipant())
                switchParticipating.setChecked(true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edt_start_date_event_edit:
                setEventDate(true);
                break;

            case R.id.edt_end_date_event_edit:
                setEventDate(false);
                break;

            case R.id.btn_update_event_edit:
                Utility.ShowToast(mContext,Constants.UNDER_DEVELOPMENT);
                //updateEvent();
                break;

            case R.id.img_previous:
                finish();
            case R.id.img_event_edit_icon:
                if(pwindo!=null)
                {
                    if(pwindo.isShowing())
                        pwindo.dismiss();
                }
                openPopup(v);
                break;

            case R.id.popup_parent:
                if(pwindo!=null)
                {
                    if(pwindo.isShowing())
                        pwindo.dismiss();
                }
                break;

            case R.id.ll_camera:
                openCamera();
                break;

            case R.id.ll_gallery:
                openGallery();
                break;

        }
    }

    private void updateEvent() {
        if (!TextUtils.isEmpty(edTitle.getText().toString())) {

            if (!TextUtils.isEmpty(actvSports.getText().toString().trim())) {
                if (!TextUtils.isEmpty(edEventStartDate.getText().toString().trim())) {

                    if (!TextUtils.isEmpty(edEventEndDate.getText().toString().trim())) {

                        if (!TextUtils.isEmpty(edNumberOfPlayers.getText().toString().trim())) {

                            requestUpateEvent();
                        } else
                            Utility.ShowToast(mContext, getResources().getString(R.string.str_event_alert_number_of_players));
                    } else
                        Utility.ShowToast(mContext, getResources().getString(R.string.str_event_alert_end_date));
                } else
                    Utility.ShowToast(mContext, getResources().getString(R.string.str_event_alert_start_date));
            } else
                Utility.ShowToast(mContext, getResources().getString(R.string.str_event_alert_sports_name));

        } else
            Utility.ShowToast(mContext, getResources().getString(R.string.str_event_alert_title));
    }

    private void requestUpateEvent() {
        ArrayList<DataModel> list = new ArrayList<DataModel>();

        DataModel dmEventTitle = new DataModel();
        dmEventTitle.setKey("title");
        dmEventTitle.setValue(edTitle.getText().toString());

        DataModel dmEventDescription = new DataModel();
        dmEventDescription.setKey("description");
        dmEventDescription.setValue(edEventDescription.getText().toString());

        DataModel dmEventSportsId = new DataModel();
        dmEventSportsId.setKey("sportId");
        dmEventSportsId.setValue("1");//Currently Static

        DataModel dmEventStartDate = new DataModel();
        dmEventStartDate.setKey("eventStartDate");
        dmEventStartDate.setValue("78008787.79879");//Currently Static

        DataModel dmEventEndDate = new DataModel();
        dmEventEndDate.setKey("eventEndDate");
        dmEventEndDate.setValue("88989798.76558");//Currently Static

        DataModel dmEventEntityType = new DataModel();
        dmEventEntityType.setKey("entityType");
        dmEventEntityType.setValue("1");//Currently Static

        DataModel dmEventLocation = new DataModel();
        dmEventLocation.setKey("location");
        dmEventLocation.setValue(setLocationParameter());//Currently Static


        DataModel dmEventNumberOfPlayersRequired = new DataModel();
        dmEventNumberOfPlayersRequired.setKey("numberOfPlayersRequired");
        dmEventNumberOfPlayersRequired.setValue("10");//Currently Static

        DataModel dmEventNeedsCreatorPermissionToJoinEvent = new DataModel();
        dmEventNeedsCreatorPermissionToJoinEvent.setKey("needsCreatorPermissionToJoinEvent");
        dmEventNeedsCreatorPermissionToJoinEvent.setValue("false");//Currently Static

        DataModel dmEventIsCreatorAParticipant = new DataModel();
        dmEventIsCreatorAParticipant.setKey("isCreatorAParticipant");
        dmEventIsCreatorAParticipant.setValue("true");//Currently Static

        list.add(dmEventTitle);
        list.add(dmEventDescription);
        list.add(dmEventSportsId);
        list.add(dmEventStartDate);
        list.add(dmEventEndDate);
        list.add(dmEventEntityType);
        list.add(dmEventLocation);
        list.add(dmEventNumberOfPlayersRequired);
        list.add(dmEventNeedsCreatorPermissionToJoinEvent);
        list.add(dmEventIsCreatorAParticipant);

        MainController controller = new MainController(EditEventActivity.this, this, Constants.UPDATE_EVENT, true);
        String finalUrl = Constants.BASE_URL + Constants.UPDATE_EVENT_API + PlayInSharedPrefUtils.getInstance(mContext).fetchStringPrefernce(PlayInSharedPrefKeys.KEY_EVENT_ID);
        controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_PUT, Constants.HEADER_TYPE_PLAY_IN_TOKEN);

    }


    private String setLocationParameter() {
        ArrayList<DataModel> listSubLocation = new ArrayList<DataModel>();

        DataModel dmEventSubLocationLatitude = new DataModel();
        dmEventSubLocationLatitude.setKey("latitude");
        dmEventSubLocationLatitude.setValue("12.378");//Currently Static

        DataModel dmEventSubLocationLongitude = new DataModel();
        dmEventSubLocationLongitude.setKey("longitude");
        dmEventSubLocationLongitude.setValue("13.78786");//Currently Static

        DataModel dmEventSubLocationGeoCodeAddress = new DataModel();
        dmEventSubLocationGeoCodeAddress.setKey("geoCodeAddress");
        dmEventSubLocationGeoCodeAddress.setValue("agsdhgaksas");//Currently Static

        DataModel dmEventSubLocationUserEnteredAddress = new DataModel();
        dmEventSubLocationUserEnteredAddress.setKey("userEnteredAddress");
        dmEventSubLocationUserEnteredAddress.setValue("");//Currently Static

        DataModel dmEventSubLocationArea = new DataModel();
        dmEventSubLocationArea.setKey("area");
        dmEventSubLocationArea.setValue("Airoli");//Currently Static

        DataModel dmEventSubLocationGeoCity = new DataModel();
        dmEventSubLocationGeoCity.setKey("city");
        dmEventSubLocationGeoCity.setValue("Mumbai");//Currently Static

        DataModel dmEventSubLocationIsEventLocations = new DataModel();
        dmEventSubLocationIsEventLocations.setKey("isEventLocation");
        dmEventSubLocationIsEventLocations.setValue("true");//Currently Static

        listSubLocation.add(dmEventSubLocationLatitude);
        listSubLocation.add(dmEventSubLocationLongitude);
        listSubLocation.add(dmEventSubLocationGeoCodeAddress);
        listSubLocation.add(dmEventSubLocationUserEnteredAddress);
        listSubLocation.add(dmEventSubLocationArea);
        listSubLocation.add(dmEventSubLocationGeoCity);
        listSubLocation.add(dmEventSubLocationIsEventLocations);

        String requestResult = Utility.getJsonRequest(listSubLocation);
        requestResult = requestResult.replaceAll("'\'", "");
        //requestResult = requestResult.substring(1, requestResult.length() - 1);
        PlayInLogManager.v(TAG, requestResult);

        return requestResult;
    }

    @Override
    public void setScreenData(Hashtable obj, byte type, String Responce) {
        switch (type) {
            case Constants.CREATE_EVENT:
                Hashtable<String, Object> result = (Hashtable<String, Object>) obj;
                ArrayList<LoginModel> list = (ArrayList<LoginModel>) obj.get("data");
                if (list != null && list.size() > 0) {
                    LoginModel model = list.get(0);
                    //	Utility.ShowToast(getApplicationContext(), model.getMessage());

                    PlayInSharedPrefUtils.getInstance(mContext).saveStringPrefernce(PlayInSharedPrefKeys.KEY_PLAYIN_ID, model.getPlayinId());
                    showCreateEventSuccessDialog("Ashutosh");
                    /*Intent intentProfile = new Intent(mContext, ProfileActivity.class);
                    startActivity(intentProfile);
                    finish();*/
                }

                ResponseInfo responseInfo = (ResponseInfo) obj.get("responseInfo");
                if (responseInfo != null && responseInfo.getMessage() != null)
                    Utility.ShowToast(mContext, responseInfo.getMessage());

                break;

            case Constants.GET_ALL_SPORTS:
                Hashtable<String, Object> resultAllSports = (Hashtable<String, Object>) obj;
                ArrayList<SportsModel> listAllSports = (ArrayList<SportsModel>) obj.get("data");
                if (listAllSports != null && listAllSports.size() > 0) {
                    SportsModel model = listAllSports.get(0);
                    ArrayAdapter<SportsModel> adapter = new ArrayAdapter<SportsModel>(this, android.R.layout.simple_list_item_1, listAllSports);
                    //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, ProgLanguages);

                    //Getting the instance of AutoCompleteTextView
                    actvSports.setThreshold(1);//will start working from first character
                    actvSports.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                }

                ResponseInfo responseInfoAllSports = (ResponseInfo) obj.get("responseInfo");
                if (responseInfoAllSports != null && responseInfoAllSports.getMessage() != null)
                    Utility.ShowToast(mContext, responseInfoAllSports.getMessage());

                break;
        }
    }

    @Override
    public void setScreenData(String obj) {

    }

    @Override
    public void setScreenMessage(String obj, byte type) {

    }

    @Override
    public void setCancelMessage(String obj, byte type) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_CODE) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    Bitmap bitmap;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                            bitmapOptions);

                    imgEventEditIcon.setImageBitmap(bitmap);

                    String path = Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + "Phoenix" + File.separator + "default";
                    f.delete();
                    OutputStream outFile = null;
                    File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
                    try {
                        outFile = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        outFile.flush();
                        outFile.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == GALLERY_CODE) {

                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                //Log.w("path of image from gallery......******************.........", picturePath + "");
                imgEventEditIcon.setImageBitmap(thumbnail);
            }
        }
    }

    private void showCreateEventSuccessDialog(String createName) {
        try {
            //tvHeader.setText("Forgot Password");
            final Dialog dialog = new Dialog(EditEventActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_server_response);
            Button btnOkay = (Button) dialog.findViewById(R.id.btn_ok_response_dialog);
            TextView tvResponse = (TextView) dialog.findViewById(R.id.tv_response_details);
            TextView tvClose = (TextView) dialog.findViewById(R.id.tv_close);

            btnOkay.setText("Invite");
            tvResponse.setText("You have successfully created your event Event from  " + createName + " Start Inviting the players to your Event");
            btnOkay.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            tvClose.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
            //Toast.makeText(mContext, "Login with Google is Successfull!!!!", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setEventDate(final boolean isStartDate) {
        int year, month, day;
        final Calendar myCalendarDOB = Calendar.getInstance();
        year = myCalendarDOB.get(Calendar.YEAR);
        month = myCalendarDOB.get(Calendar.MONTH);
        day = myCalendarDOB.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker = new DatePickerDialog(EditEventActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                myCalendarDOB.set(Calendar.YEAR, selectedyear);
                myCalendarDOB.set(Calendar.MONTH, selectedmonth);
                myCalendarDOB.set(Calendar.DAY_OF_MONTH, selectedday);

                SimpleDateFormat fmtDate = new SimpleDateFormat("dd-MM-yyyy");
                Calendar myCalendarTemp = Calendar.getInstance();
                Calendar tempmyCalendarDOB = myCalendarTemp;
                tempmyCalendarDOB.set(Calendar.YEAR, myCalendarTemp.get(Calendar.YEAR));

                if (isStartDate) {
                    if (myCalendarDOB.before(tempmyCalendarDOB) || myCalendarDOB.equals(tempmyCalendarDOB)) {
                        edEventStartDate.setText(fmtDate.format(myCalendarDOB.getTime().getTime()).toString());
                        edEventStartDate.setTextColor(Color.BLACK);
                    } else {
                        edEventStartDate.setText("");
                        myCalendarDOB.clear();
                        Utility.ShowToast(mContext, "Start date can't be less than current date");
                    }
                } else {
                    if (myCalendarDOB.before(tempmyCalendarDOB) || myCalendarDOB.equals(tempmyCalendarDOB)) {
                        edEventEndDate.setText(fmtDate.format(myCalendarDOB.getTime().getTime()).toString());
                        edEventEndDate.setTextColor(Color.BLACK);
                    } else {
                        edEventEndDate.setText("");
                        myCalendarDOB.clear();
                        Utility.ShowToast(mContext, "End date can't be less than event start date");
                    }
                }

            }
        }, year, month, day);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }


    public void getAllSports() {
        MainController controller = new MainController(EditEventActivity.this, this, Constants.GET_ALL_SPORTS, true);
        String finalUrl = Constants.BASE_URL + Constants.GET_ALL_SPORTS_API;
        controller.RequestServiceGet(finalUrl.trim(), Constants.REQUEST_TYPE_GET, Constants.HEADER_TYPE_PLAY_IN_TOKEN);
    }

    private void openCamera()
    {
        pwindo.dismiss();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(intent, CAMERA_CODE);
    }
    private void openGallery()
    {
        pwindo.dismiss();
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_CODE);
    }
    private void openPopup(View anchor) {
        try {
            LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.screen_popup, (ViewGroup) findViewById(R.id.popup_parent));
            pwindo = new PopupWindow(layout, getWidth(), getHeight(), true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
            pwindo.setOutsideTouchable(true);
            // pwindo.setFocusable(true);
            pwindo.setBackgroundDrawable(new BitmapDrawable(getResources(), Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)));
            layout.setOnClickListener(this);
            pwindo.showAsDropDown(anchor, 0, 0);
            layout.findViewById(R.id.ll_gallery).setOnClickListener(this);
            layout.findViewById(R.id.ll_camera).setOnClickListener(this);

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
