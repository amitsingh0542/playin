package playin.orgname.com.playin.model;

/**
 * Created by Shashvat on 6/21/2016.
 */

public class DataModel {

    private String Key;
    private String Value;

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
