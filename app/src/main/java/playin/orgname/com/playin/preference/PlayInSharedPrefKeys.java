package playin.orgname.com.playin.preference;

/**
 * @author Ashutosh.Srivastava
 */
public class PlayInSharedPrefKeys {
	// Response Credentials


	public static final String KEY_USERNAME_LOGIN = "username";
	public static final String KEY_PASSWORD_LOGIN = "password";
	public static final String KEY_USER_TYPE="key_user_type";
	public static final String PREF_KEY_SPLASH_LAUNCH_COUNT = "splashLaunchCount";
	public static final String KEY_PLAYIN_ID = "playinId";
	public static final String KEY_EVENT_ID = "eventId";
	public static final String KEY_TEAM_ID = "teamId";


	public static final String KEY_TOKEN="token";
	public static final String KEY_TOKEN_EXPIRE_ON="tokenExpiresOn" ;

}
